package com.android.bodegas.domain.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
