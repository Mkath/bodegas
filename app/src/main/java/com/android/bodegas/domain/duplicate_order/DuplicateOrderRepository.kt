package com.android.bodegas.domain.duplicate_order

import com.android.bodegas.domain.util.Resource

interface DuplicateOrderRepository {

    suspend fun getDuplicateOrder(establishmentId: Int, orderId: Int): Resource<DuplicateOrder>

}