package com.android.bodegas.domain.user.login.model

data class UserLogin(val token: String, val userName: String)

