package com.android.bodegas.domain.user.login

import com.android.bodegas.domain.user.UserRepository
import com.android.bodegas.domain.user.login.model.UserLoginToken
import com.android.bodegas.domain.util.Resource

class LoginTokenUser constructor(private val userRepository: UserRepository) {

    suspend fun loginUserToken(email: String, password: String): Resource<UserLoginToken> {
        return userRepository.loginUserToken(email, password)
    }
}