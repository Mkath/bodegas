package com.android.bodegas.domain.generate_order

import com.android.bodegas.domain.util.Resource


interface GenerateOrderRepository {

    suspend fun generateOrder(
        deliveryType: String,
        total: String,
        startTime: String,
        endTime: String,
        paymentMethodId:Int,
        deliveryCharge:Double,
        customerAmount: Double
    ): Resource<String>

}