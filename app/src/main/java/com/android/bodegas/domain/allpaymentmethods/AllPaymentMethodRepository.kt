package com.android.bodegas.domain.allpaymentmethods

import com.android.bodegas.domain.util.Resource
import com.android.bodegasadmin.domain.allpaymentmethods.AllPaymentMethod

interface AllPaymentMethodRepository {

    suspend fun getAllPaymentMethods(): Resource<List<AllPaymentMethod>>
}