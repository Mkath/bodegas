package com.android.bodegas.domain.allpaymentmethods

import com.android.bodegas.domain.util.Resource
import com.android.bodegasadmin.domain.allpaymentmethods.AllPaymentMethod

class GetAllPaymentMethods constructor(private val allPaymentMethodsRepository: AllPaymentMethodRepository) {

    suspend fun getAllPaymentMethods(): Resource<List<AllPaymentMethod>> {
        return allPaymentMethodsRepository.getAllPaymentMethods()
    }
}