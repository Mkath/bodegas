package com.android.bodegas.domain.subcategory

data class Subcategory (
    val id: Int,
    val description: String,
    val name: String,
    val pathImage: String ? = ""
)