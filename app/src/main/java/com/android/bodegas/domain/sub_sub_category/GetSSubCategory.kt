package com.android.bodegas.domain.sub_sub_category

import com.android.bodegas.domain.util.Resource
import java.util.*


class GetSSubCategory(private val sSubCategoryRepository: SSubCategoryRepository) {

    suspend fun getSSubCategory(
        establishmentId: Int,
        subCategoryId: Int
    ): Resource<Group> {
        return sSubCategoryRepository.getSSubCategory(establishmentId, subCategoryId)
    }
}
