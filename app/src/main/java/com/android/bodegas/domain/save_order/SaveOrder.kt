package com.android.bodegas.domain.save_order

data class SaveOrder(
    var id: String,
    val storeProductId: Int,
    var price: Double,
    val quantity: Double,
    var subtotal: Double,
    val unitMeasure: String,
    val observation: String,
    val name: String,
    val imageProduct:String
)