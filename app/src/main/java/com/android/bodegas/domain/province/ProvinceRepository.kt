package com.android.bodegas.domain.province

import com.android.bodegas.domain.util.Resource

interface ProvinceRepository {
    suspend fun getProvinces(
        id: String,
        name: String,
        department_id: String
    ): Resource<List<Province>>
}
