package com.android.bodegas.domain.province

import com.android.bodegas.domain.util.Resource

class GetProvinces (private val provinceRepository: ProvinceRepository){
    suspend fun  getProvince(id: String,
                             name: String,
                             department_id: String): Resource<List<Province>> {

        return provinceRepository.getProvinces(id,name,department_id )
    }
}
