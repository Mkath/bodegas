package com.android.bodegas.domain.subcategory

import com.android.bodegas.domain.util.Resource

interface SubcategoryRepository {
    suspend fun getSubcategories(
        establishmentId:Int,
        category_id:Int,
        page: String,
        size: String,
        sortBy: String
    ) : Resource<List<Subcategory>>
}