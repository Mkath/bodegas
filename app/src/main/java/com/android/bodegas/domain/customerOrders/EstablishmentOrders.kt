package com.android.bodegas.domain.customerOrders

import com.android.bodegas.domain.supplier.PaymentMethod
import com.android.bodegas.domain.supplier.Schedule

class EstablishmentOrders (
    val establishmentId: Int,
    val establishmentEmail: String,
    val establishmentName: String,
    val delivery: String,
    val shippingSchedule: List<Schedule>,
    val operationSchedule: List<Schedule>,
    val paymentMethods: List<PaymentMethod>,
    val deliveryCharge: Boolean,
    val establishmentAddress: String
)