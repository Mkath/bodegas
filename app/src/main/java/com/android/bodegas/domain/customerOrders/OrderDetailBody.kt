package com.android.bodegas.domain.customerOrders

class OrderDetailBody (
    val orderDetailId: Int,
    val state: String
)