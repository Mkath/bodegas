package com.android.bodegas.domain.sub_sub_category

import com.android.bodegas.data.repository.produtcs.model.ProductsEntity
import com.android.bodegas.domain.products.Products


data class Group(
    val total: Int,
    val storeProducts: List<Products>,
    val subSubCategories: List<SSubCategory>
)
