package com.android.bodegas.domain.sub_sub_category

data class SSubCategory(
    val description: String,
    val name: String,
    val id: Int,
    val pathImage: String ? = ""

)
