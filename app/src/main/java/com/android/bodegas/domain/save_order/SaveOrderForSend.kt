package com.android.bodegas.domain.save_order

import com.android.bodegas.domain.util.Resource
import java.util.*


class SaveOrderForSend(private val saveOrderRepository: SaveOrderRepository) {

    suspend fun saveOrder(saveOrder: SaveOrder)  {
        return saveOrderRepository.saveOrder(saveOrder)
    }

    suspend fun updateOrder(saveOrder: SaveOrder)  {
        return saveOrderRepository.updateOrder(saveOrder)
    }
}
