package com.android.bodegas.domain.user.create.model

data class User(
    val customerId: Int,
    val creationDate: String,
    val creationUser: String,
    val departament: String,
    val district: String,
    val dni: String,
    val email: String,
    val address: String,
    val lastNameMaternal: String,
    val lastNamePaternal: String,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val phoneNumber: String,
    val province: String,
    val ruc: String,
    val status: String,
    val urbanization: String,
    val pathImage:String? =""
)
