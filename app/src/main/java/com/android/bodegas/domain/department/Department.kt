package com.android.bodegas.domain.department

data class Department (
    val id: String,
    val name: String
)