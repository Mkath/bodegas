package com.android.bodegas.domain.user.update

import com.android.bodegas.data.repository.user.model.UserEntity
import com.android.bodegas.domain.user.UserRepository
import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.presentation.profile.UpdateCustomerBody

class UpdateDataUser constructor(private val userRepository: UserRepository) {

    suspend fun updateDataUser(customerId: Int, body: UpdateCustomerBody): Resource<User> {
        return userRepository.updateUser(customerId, body)
    }
}