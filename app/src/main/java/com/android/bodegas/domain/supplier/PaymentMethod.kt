package com.android.bodegas.domain.supplier

data class PaymentMethod(
    val paymentMethodId: Int,
    val requestedAmount: Boolean,
    val description: String,
    val customerMessage: String
)