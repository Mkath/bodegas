package com.android.bodegas.domain.duplicate_order

data class DuplicateOrder (

    val establishmentId: Int,
    val flagAllAvailableProducts: Boolean,
    val orderDetailRequest: List<DuplicateOrderItem>

)