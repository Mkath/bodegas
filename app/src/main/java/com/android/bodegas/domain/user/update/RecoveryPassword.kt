package com.android.bodegas.domain.user.update

import com.android.bodegas.domain.user.UserRepository
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.presentation.login.ForgetPassDTO

class RecoveryPassword constructor(private val userRepository: UserRepository) {
    suspend fun recoveryPassword(body: ForgetPassDTO): Resource<Boolean> {
        return userRepository.recoveryPassword(body)
    }
}