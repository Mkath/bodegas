package com.android.bodegas.domain.products

import com.android.bodegas.domain.util.Resource
import java.util.*


class GetProductsBySubCategory(private val productsRepository: ProductsRepository) {

    suspend fun getProductsBySubCategory(
        establishmentId: Int,
        supplierDocument: String,
        subCategoryId: Int
    ): Resource<List<Products>> {
        return productsRepository.getProducts(establishmentId, supplierDocument, subCategoryId)
    }
}
