package com.android.bodegas.domain.user.logout

import com.android.bodegas.domain.user.UserRepository


class LogoutUser(private val userRepository: UserRepository) {

    suspend fun logOut() {
        userRepository.logoutUser()
    }
}