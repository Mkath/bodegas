package com.android.bodegas.domain.clases

import com.android.bodegas.domain.util.Resource

interface ClassRepository {
    suspend fun getClassesBySupplierId(
        establishmentId: Int,
        page: String,
        size: String,
        sortBy: String
    ): Resource<List<Class>>
}