package com.android.bodegas.domain.customerOrders

import com.android.bodegas.domain.util.Resource

interface CustomerOrderRepository {
    suspend fun getCustomerOrdersByCustomerId(
        customerId: Int
    ): Resource<List<CustomerOrders>>

    suspend fun updateStateOrder(
        orderId:Int,
        state:String,
        list: List<OrderDetailBody>
    ): Resource<Boolean>

    suspend fun getFileByOrder(
        orderId:Int
    ):Resource<String>
}