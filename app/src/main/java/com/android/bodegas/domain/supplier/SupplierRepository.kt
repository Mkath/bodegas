package com.android.bodegas.domain.supplier

import com.android.bodegas.domain.util.Resource


interface SupplierRepository {
    suspend fun getSupplierByType(
        type: Int,
        latitude: String,
        longitude: String,
        radius: Int
    ): Resource<List<Supplier>>
}
