package com.android.bodegas.domain.generate_order

import com.android.bodegas.domain.user.UserRepository
import com.android.bodegas.domain.util.Resource

class GenerateOrder constructor(private val generateOrderRepository: GenerateOrderRepository) {

    suspend fun generateOrder(
        deliveryType: String,
        total: String,
        startTime: String,
        endTime: String,
        paymentMethodId:Int,
        deliveryCharge:Double,
        customerAmount: Double
    ): Resource<String> {
        return generateOrderRepository.generateOrder(deliveryType, total, startTime, endTime, paymentMethodId, deliveryCharge, customerAmount)
    }
}
