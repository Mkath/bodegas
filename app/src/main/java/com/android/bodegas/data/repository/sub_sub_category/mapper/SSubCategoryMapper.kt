package com.android.bodegas.data.repository.sub_sub_category.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.produtcs.model.ProductsEntity
import com.android.bodegas.data.repository.sub_sub_category.model.SSubCategoryEntity
import com.android.bodegas.data.repository.supplier.model.SupplierEntity
import com.android.bodegas.domain.products.Products
import com.android.bodegas.domain.sub_sub_category.SSubCategory
import com.android.bodegas.domain.supplier.Supplier


class SSubCategoryMapper : Mapper<SSubCategoryEntity, SSubCategory> {

    override fun mapFromEntity(type: SSubCategoryEntity): SSubCategory {
        return SSubCategory(
            type.description,
            type.name,
            type.id,
            type.pathImage
        )
    }

    override fun mapToEntity(type: SSubCategory): SSubCategoryEntity {
        return SSubCategoryEntity(
            type.description,
            type.name,
            type.id,
            type.pathImage
        )
    }

}