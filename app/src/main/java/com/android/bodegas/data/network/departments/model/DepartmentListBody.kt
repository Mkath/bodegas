package com.android.bodegas.data.network.departments.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class DepartmentListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var departmentListBody: DepartmentItem? = null
}

class DepartmentItem (
    @SerializedName("departamentosTotal")
    val departamentosTotal: Int,
    @SerializedName("departments")
    val departments: List<DepartmentResponse>
)