package com.android.bodegas.data.repository.category.store.remote

import com.android.bodegas.data.repository.category.model.CategoryEntity
import com.android.bodegas.domain.util.Resource

interface CategoryRemote {
    suspend fun getCategories(
        establishmentId: Int,
        class_id: Int,
        page: String,
        size: String,
        sortBy: String
    ) : Resource<List<CategoryEntity>>
}
