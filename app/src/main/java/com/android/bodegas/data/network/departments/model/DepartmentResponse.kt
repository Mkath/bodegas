package com.android.bodegas.data.network.departments.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DepartmentResponse (
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("name")
    @Expose
    val name: String
)