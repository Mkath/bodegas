package com.android.bodegas.data.repository.sub_sub_category.model

import com.android.bodegas.data.repository.produtcs.model.ProductsEntity


data class GroupEntity(
    val total: Int,
    val storeProducts: List<ProductsEntity>,
    val subSubCategories: List<SSubCategoryEntity>
)
