package com.android.bodegas.data.network.departments

import android.content.Context
import com.android.bodegas.data.network.departments.mapper.DepartmentEntityMapper
import com.android.bodegas.data.network.departments.model.DepartmentItem
import com.android.bodegas.data.network.departments.model.DepartmentListBody
import com.android.bodegas.data.repository.departments.model.DepartmentEntity
import com.android.bodegas.data.repository.departments.store.remote.DepartmentRemote
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.InputStream
import java.io.InputStreamReader
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.coroutines.CoroutineContext

class DepartmentRemoteImpl constructor(private val departmentServices: DepartmentService,
                                       private val deparmentEntityMapper: DepartmentEntityMapper,
                                       private val context: Context
) : DepartmentRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getDepartments(
    ): Resource<List<DepartmentEntity>> {
        return coroutineScope {
            try{
             /*   val result = departmentServices.getDeparments()

                if (result.isSuccessful) {
                    result.body()?.let {
                        val departmentEntityList = mapList(result.body()!!.departmentListBody)
                        departmentEntityList?.let {
                            return@coroutineScope Resource(Status.SUCCESS, departmentEntityList, "")
                        }
                    }
                }
                return@coroutineScope Resource(Status.ERROR, listOf<DepartmentEntity>(), "")*/
                return@coroutineScope getMockedDeparments()
            }
            catch(e: UnknownHostException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<DepartmentEntity>(),
                    "Not Connection"
                )
            }
            catch(e: Throwable){
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<DepartmentEntity>(),"")
            }
        }
    }

    private fun mapList(departmentListBody: DepartmentItem?): List<DepartmentEntity>? {
        val departmentList = ArrayList<DepartmentEntity>()
        departmentListBody?.departments?.forEach{
            departmentList.add(deparmentEntityMapper.mapFromRemote(it))
        }
        return departmentList
    }

    private suspend fun getMockedDeparments(): Resource<List<DepartmentEntity>>{
        return coroutineScope {
            delay(1000)
            try{
                val gson = Gson()
                val inputStream: InputStream = context.assets.open("peru_departamentos")
                val inputStreamReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
                val departmentList: DepartmentListBody = gson.fromJson(inputStreamReader, DepartmentListBody::class.java)
                val departmentEntityList = mutableListOf<DepartmentEntity>()
                departmentList.departmentListBody?.departments?.forEach { it -> departmentEntityList.add(deparmentEntityMapper.mapFromRemote(it))  }

                return@coroutineScope Resource<List<DepartmentEntity>>(
                    Status.SUCCESS,
                    departmentEntityList,
                    "Mocked classes list"
                )
            }
            catch (e: Throwable){
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<DepartmentEntity>(), "")
            }
        }
    }
}

