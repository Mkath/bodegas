package com.android.bodegas.data.network.category.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class CategoryListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var categoryListBody: CategoryItem? = null
}

class CategoryItem (
    @SerializedName("categoriesTotal")
    val categoriesTotal: Int,
    @SerializedName("categories")
    val categoriesList: List<CategoryResponse>
)