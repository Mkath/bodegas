package com.android.bodegas.data.repository.category.store.remote

import com.android.bodegas.data.repository.category.model.CategoryEntity
import com.android.bodegas.data.repository.category.store.CategoryDataStore
import com.android.bodegas.domain.util.Resource

class CategoryRemoteDataStorage constructor(private val category_Remote: CategoryRemote) : CategoryDataStore {
    override suspend fun getCategories(
        establishmentId: Int,
        class_id: Int,
        page: String,
        size: String,
        sortBy: String
    ): Resource<List<CategoryEntity>> {
        return category_Remote.getCategories(establishmentId, class_id, page, size, sortBy)
    }

}