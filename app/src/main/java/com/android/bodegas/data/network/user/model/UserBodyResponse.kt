package com.android.bodegas.data.network.user.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserBodyResponse : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var registryNewUserItem: UserItem? = null
}

class UserItem(
    @SerializedName("customer")
    @Expose
    var customer: UserResponse
)


class UserResponse(
    @SerializedName("customerId")
    @Expose
    val customerId: Int,
    @SerializedName("creationDate")
    @Expose
    var creationDate: String,
    @SerializedName("creationUser")
    @Expose
    var creationUser: String,
    @SerializedName("departament")
    @Expose
    var departament: String,
    @SerializedName("district")
    @Expose
    var district: String,
    @SerializedName("dni")
    @Expose
    var dni: String,
    @SerializedName("email")
    @Expose
    var email: String,
    @SerializedName("lastNameMaternal")
    @Expose
    var lastNameMaternal: String,
    @SerializedName("lastNamePaternal")
    @Expose
    var lastNamePaternal: String,
    @SerializedName("latitude")
    @Expose
    var latitude: Double,
    @SerializedName("longitude")
    @Expose
    var longitude: Double,
    @SerializedName("name")
    @Expose
    var name: String,
    @SerializedName("phoneNumber")
    @Expose
    var phoneNumber: String,
    @SerializedName("province")
    @Expose
    var province: String,
    @SerializedName("ruc")
    @Expose
    var ruc: String,
    @SerializedName("status")
    @Expose
    var status: String,
    @SerializedName("urbanization")
    @Expose
    var urbanization: String,

    @SerializedName("pathImage")
    @Expose
    var pathImage: String? = "",
    @SerializedName("address")
    @Expose
    var address: String
)






