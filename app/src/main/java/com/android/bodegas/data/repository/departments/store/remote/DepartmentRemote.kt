package com.android.bodegas.data.repository.departments.store.remote

import com.android.bodegas.data.repository.departments.model.DepartmentEntity
import com.android.bodegas.domain.util.Resource


interface DepartmentRemote {
    suspend fun getDepartments (
    ): Resource<List<DepartmentEntity>>
}