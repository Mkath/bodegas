package com.android.bodegas.data.db.save_order.model

import androidx.room.*
import com.android.bodegas.data.db.save_order.dao.SaveOrderConstants

@Entity(
    tableName = SaveOrderConstants.TABLE_NAME
)
data class SaveOrderDb(
    @PrimaryKey
    var id: String,
    val storeProductId: Int,
    val price: Double,
    val quantity: Double,
    val subtotal: Double,
    val unitMeasure: String,
    val observation: String,
    val name: String,
    val imageProduct: String
)
