package com.android.bodegas.data.repository.subcategory.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.subcategory.model.SubCategoryEntity
import com.android.bodegas.domain.subcategory.Subcategory

class SubCategoryMapper : Mapper<SubCategoryEntity, Subcategory> {
    override fun mapToEntity(type: Subcategory): SubCategoryEntity {
        return SubCategoryEntity(type.id, type.description, type.name, type.pathImage)
    }

    override fun mapFromEntity(type: SubCategoryEntity): Subcategory {
        return Subcategory(type.id, type.description, type.name, type.pathImage)
    }
}