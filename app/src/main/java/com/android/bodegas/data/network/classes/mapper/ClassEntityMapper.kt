package com.android.bodegas.data.network.classes.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.classes.model.ClassResponse
import com.android.bodegas.data.repository.clases.model.ClassEntity

class ClassEntityMapper : Mapper<ClassResponse, ClassEntity> {
    override fun mapFromRemote(type: ClassResponse): ClassEntity {
        return ClassEntity(
            type.id,
            type.description,
            type.name,
            type.pathImage
        )
    }
}