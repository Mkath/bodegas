package com.android.bodegas.data.network.category

import android.content.Context
import android.util.Log
import com.android.bodegas.data.network.category.mapper.CategoryEntityMapper
import com.android.bodegas.data.network.category.model.CategoryItem

import com.android.bodegas.data.network.category.model.CategoryListBody
import com.android.bodegas.data.repository.category.model.CategoryEntity
import com.android.bodegas.data.repository.category.store.remote.CategoryRemote

import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.InputStream
import java.io.InputStreamReader
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.coroutines.CoroutineContext

class CategoryRemoteImpl constructor(
    private val categoryServices: CategoryServices,
    private val categoryEntityMapper: CategoryEntityMapper,
    private val context: Context
) : CategoryRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getCategories(
        establishmentId: Int,
        class_id: Int,
        page: String,
        size: String,
        sortBy: String
    ): Resource<List<CategoryEntity>> {
        return coroutineScope {
            try {
                val result = categoryServices.getCategoriesBySupplierId(
                    establishmentId,
                    class_id
                )
                if (result.isSuccessful) {
                    if(result.code() == 200){
                    result.body()?.let {
                        val categoryEntityList = mapList(result.body()!!.categoryListBody)
                        categoryEntityList?.let {
                            return@coroutineScope Resource(Status.SUCCESS, categoryEntityList, "")
                        }
                    }
                    }else if (result.code() == 204){
                        return@coroutineScope Resource(Status.SUCCESS, listOf<CategoryEntity>(), "")
                    }
                }
                return@coroutineScope Resource(Status.ERROR, listOf<CategoryEntity>(), "")
            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<CategoryEntity>(),
                    "Not Connection"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<CategoryEntity>(), "")
            }
        }
    }

    private fun mapList(categoryListBody: CategoryItem?): List<CategoryEntity>? {
        val categoryList = ArrayList<CategoryEntity>()
        categoryListBody?.categoriesList?.forEach {
            categoryList.add(categoryEntityMapper.mapFromRemote(it))
        }
        return categoryList
    }

    private suspend fun getMockedCategories(): Resource<List<CategoryEntity>> {
        return coroutineScope {
            delay(100)
            try {
                val gson = Gson()
                val inputStream: InputStream = context.assets.open("mocked_categories_list")
                val inputStreamReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
                val classList: CategoryListBody =
                    gson.fromJson(inputStreamReader, CategoryListBody::class.java)
                val classEntityList = mutableListOf<CategoryEntity>()
                classList.categoryListBody?.categoriesList?.forEach { it ->
                    classEntityList.add(
                        categoryEntityMapper.mapFromRemote(it)
                    )
                }

                return@coroutineScope Resource<List<CategoryEntity>>(
                    Status.SUCCESS,
                    classEntityList,
                    "Mocked categories list"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<CategoryEntity>(), "")
            }
        }
    }
}