package com.android.bodegas.data.network.products.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ProductsResponse(
    @SerializedName("price")
    @Expose
    val price: Double,
    @SerializedName("status")
    @Expose
    val status: String,
    @SerializedName("storeProductId")
    @Expose
    val storeProductId: Int,
    @SerializedName("stock")
    @Expose
    val stock: String,
    @SerializedName("productTemplate")
    @Expose
    val productDetails: ProductsDetailsResponse
)