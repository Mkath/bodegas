package com.android.bodegas.data.repository.allPaymentMethods.store.remote

import com.android.bodegas.domain.util.Resource
import com.android.bodegasadmin.data.repository.allPaymentMethods.model.AllPaymentMethodsEntity
import com.android.bodegasadmin.data.repository.allPaymentMethods.store.AllPaymentMethodsDataStore

class AllPaymentMethodsRemoteDataStore constructor(
    private val allPaymentMethodsRemote: AllPaymentMethodsRemote
) : AllPaymentMethodsDataStore {

    override suspend fun getAllPaymentMethods(): Resource<List<AllPaymentMethodsEntity>> {
        return  allPaymentMethodsRemote.getAllPaymentMethods()
    }


}