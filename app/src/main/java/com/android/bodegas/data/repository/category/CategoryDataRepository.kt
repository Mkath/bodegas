package com.android.bodegas.data.repository.category

import android.util.Log
import com.android.bodegas.data.repository.category.mapper.CategoryMapper
import com.android.bodegas.data.repository.category.model.CategoryEntity
import com.android.bodegas.data.repository.category.store.remote.CategoryRemoteDataStorage
import com.android.bodegas.domain.category.Category
import com.android.bodegas.domain.category.CategoryRepository
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class CategoryDataRepository (
    private val categoryRemoteDataStore: CategoryRemoteDataStorage,
    private val categoryMapper: CategoryMapper
): CategoryRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getCategories(
        establishmentId: Int,
        class_id: Int,
        page: String,
        size: String,
        sortBy: String
    ): Resource<List<Category>> { //devolvemos un objeto de la capa domain
        return coroutineScope {
            val classList =
                categoryRemoteDataStore.getCategories( establishmentId, class_id,
                    page,
                    size,
                    sortBy)
            var resource: Resource<List<Category>> =
                Resource(classList.status, mutableListOf(), classList.message)

            if (classList.status == Status.SUCCESS) {
                val mutableListclasses = mapCategoryList(classList.data)
                resource = Resource(Status.SUCCESS, mutableListclasses, classList.message)
            } else {
                if (classList.status == Status.ERROR) {
                    resource = Resource(classList.status, mutableListOf(), classList.message)
                    Log.e("Debug", "Error al obtener data")
                }
            }

            return@coroutineScope resource
        }
    }

    private fun mapCategoryList(categoryEntityList: List<CategoryEntity>): List<Category> {
        val categoryList = mutableListOf<Category>()
        categoryEntityList.forEach {
            categoryList.add( categoryMapper.mapFromEntity(it))
        }
        return categoryList
    }
}
