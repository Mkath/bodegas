package com.android.bodegas.data.network.user.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserLoginItem(
    @SerializedName("accessToken")
    val token: String,
    @SerializedName("tokenType")
    val tokenType: String)


class UserLoginResponse : ResponseBodySuccess() {

    @SerializedName("body")
    @Expose
    var body: UserLoginItem? = null

}