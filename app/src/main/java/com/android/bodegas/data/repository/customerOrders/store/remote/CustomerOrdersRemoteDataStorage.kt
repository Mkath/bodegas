package com.android.bodegas.data.repository.customerOrders.store.remote

import com.android.bodegas.data.repository.customerOrders.model.CustomerOrdersEntity
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailBodyEntity
import com.android.bodegas.data.repository.customerOrders.store.CustomerOrdersDataStore
import com.android.bodegas.domain.customerOrders.OrderDetailBody
import com.android.bodegas.domain.util.Resource

class CustomerOrdersRemoteDataStorage constructor(private val customerOrderRemote: CustomerOrdersRemote)
    : CustomerOrdersDataStore {
    override suspend fun getCustomerOrdersByCustomerId(customerId: Int): Resource<List<CustomerOrdersEntity>> {
        return customerOrderRemote.getCustomerOrdersByCustomerId(customerId)
    }

    override suspend fun updateStateOrder(orderId: Int, state:String, list: List<OrderDetailBodyEntity>): Resource<Boolean> {
        return customerOrderRemote.updateStateOrder(orderId, state, list)
    }

    override suspend fun getFileByOrder(orderId: Int): Resource<String> {
        return customerOrderRemote.getFileByOrder(orderId)
    }

}
