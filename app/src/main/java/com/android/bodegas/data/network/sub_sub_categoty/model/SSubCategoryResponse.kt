package com.android.bodegas.data.network.sub_sub_categoty.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SSubCategoryResponse(
    @SerializedName("description")
    @Expose
    val description: String,
    @SerializedName("subSubCategoryId")
    @Expose
    val id: Int,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("pathImage")
    @Expose
    val pathImage: String ? = ""
)