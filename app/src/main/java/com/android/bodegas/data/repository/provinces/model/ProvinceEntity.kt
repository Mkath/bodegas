package com.android.bodegas.data.repository.provinces.model

class ProvinceEntity (
    val id: String,
    val name: String,
    val department_id: String
)