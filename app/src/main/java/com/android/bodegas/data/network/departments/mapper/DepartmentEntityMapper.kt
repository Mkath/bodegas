package com.android.bodegas.data.network.departments.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.departments.model.DepartmentResponse
import com.android.bodegas.data.repository.departments.model.DepartmentEntity

class DepartmentEntityMapper : Mapper<DepartmentResponse, DepartmentEntity> {
    override fun mapFromRemote(type: DepartmentResponse): DepartmentEntity {
        return DepartmentEntity(
            type.id,
            type.name
        )
    }
}