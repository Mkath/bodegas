package com.android.bodegas.data.repository.districts.model

class ProvinceEntity (
    val id: String,
    val name: String,
    val province_id: String,
    val department_id: String
)