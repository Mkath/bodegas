package com.android.bodegas.data.repository.sub_sub_category.store.remote

import com.android.bodegas.data.repository.sub_sub_category.model.GroupEntity
import com.android.bodegas.domain.util.Resource


interface SSubCategoryRemote {
    suspend fun getSSubCategory(
        establishmentId:Int,
        subCategoryId: Int
    ): Resource<GroupEntity>

}