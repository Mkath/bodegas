package com.android.bodegas.data.repository.supplier

import com.android.bodegas.data.repository.supplier.mapper.SupplierMapper
import com.android.bodegas.data.repository.supplier.model.SupplierEntity
import com.android.bodegas.data.repository.supplier.store.remote.SupplierRemoteDataStore
import com.android.bodegas.domain.supplier.Supplier
import com.android.bodegas.domain.supplier.SupplierRepository
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class SupplierDataRepository(
    private val supplierRemoteDataStore: SupplierRemoteDataStore,
    private val supplierMapper: SupplierMapper
) : SupplierRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getSupplierByType(
        type: Int,
        latitude: String,
        longitude: String,
        radius: Int
    ): Resource<List<Supplier>> {
        return coroutineScope {
            val supplierList =
                supplierRemoteDataStore.getSupplierByType(type, latitude, longitude, radius)

            var resource: Resource<List<Supplier>> =
                Resource(supplierList.status, mutableListOf(), supplierList.message)

            if (supplierList.status == Status.SUCCESS) {
                val mutableListFarms = mapSupplierList(supplierList.data)
                resource = Resource(Status.SUCCESS, mutableListFarms, supplierList.message)
            } else {
                if (supplierList.status == Status.ERROR) {
                    resource = Resource(supplierList.status, mutableListOf(), supplierList.message)
                }
            }

            return@coroutineScope resource
        }
    }

    private fun mapSupplierList(supplierEntityList: List<SupplierEntity>): List<Supplier> {
        val supplierList = mutableListOf<Supplier>()
        supplierEntityList.forEach {
            supplierList.add(supplierMapper.mapFromEntity(it))
        }
        return supplierList
    }
}