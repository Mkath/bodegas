package com.android.bodegas.data.repository.sub_sub_category.model

data class SSubCategoryEntity(
    val description: String,
    val name: String,
    val id: Int,
    val pathImage:String ? = ""
)
