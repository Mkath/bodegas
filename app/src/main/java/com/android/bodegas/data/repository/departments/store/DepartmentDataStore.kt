package com.android.bodegas.data.repository.departments.store

import com.android.bodegas.data.repository.departments.model.DepartmentEntity
import com.android.bodegas.domain.util.Resource

interface DepartmentDataStore {
    suspend fun getDepartments(
    ): Resource<List<DepartmentEntity>>
}