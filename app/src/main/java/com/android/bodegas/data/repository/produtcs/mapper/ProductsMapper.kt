package com.android.bodegas.data.repository.produtcs.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.produtcs.model.ProductsEntity
import com.android.bodegas.data.repository.supplier.model.SupplierEntity
import com.android.bodegas.domain.products.Products
import com.android.bodegas.domain.supplier.Supplier


class ProductsMapper : Mapper<ProductsEntity, Products> {

    override fun mapFromEntity(type: ProductsEntity): Products {
        return Products(
            type.storeProductId,
            type.price,
            type.status,
            type.code,
            type.name,
            type.description,
            type.unitMeasure,
            type.stock,
            type.imageProduct
        )
    }

    override fun mapToEntity(type: Products): ProductsEntity {
        return ProductsEntity(
            type.storeProductId,
            type.price,
            type.status,
            type.code,
            type.name,
            type.description,
            type.unitMeasure,
            type.stock,
            type.imageProduct
        )
    }

}