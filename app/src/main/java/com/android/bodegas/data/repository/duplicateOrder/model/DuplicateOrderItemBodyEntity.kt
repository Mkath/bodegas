package com.android.bodegas.data.repository.duplicateOrder.model

data class DuplicateOrderItemBodyEntity (

    val description: String,
    val price: Double,
    val storeProductId: Int,
    val subTotal: Double

)