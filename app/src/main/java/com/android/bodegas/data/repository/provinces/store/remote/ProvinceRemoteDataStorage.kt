package com.android.bodegas.data.repository.provinces.store.remote

import com.android.bodegas.data.repository.provinces.model.ProvinceEntity
import com.android.bodegas.data.repository.provinces.store.ProvinceDataStore
import com.android.bodegas.domain.util.Resource

class ProvinceRemoteDataStorage constructor(private val provinceRemote: ProvinceRemote)  :
    ProvinceDataStore {
    override suspend fun getProvinces(
        id: String,
        name: String,
        department_id: String

    ): Resource<List<ProvinceEntity>> {
        return provinceRemote.getProvinces(id, name, department_id)
    }
}

