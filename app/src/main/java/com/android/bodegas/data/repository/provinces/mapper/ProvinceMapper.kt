package com.android.bodegas.data.repository.provinces.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.provinces.model.ProvinceEntity
import com.android.bodegas.domain.province.Province

class ProvinceMapper: Mapper<ProvinceEntity, Province> {
    override fun mapFromEntity(type: ProvinceEntity): Province {
        return Province(type.id, type.name, type.department_id)
    }

    override fun mapToEntity(type: Province): ProvinceEntity {
        return ProvinceEntity(type.id, type.name, type.department_id)
    }
}