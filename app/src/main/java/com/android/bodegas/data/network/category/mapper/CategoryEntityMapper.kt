package com.android.bodegas.data.network.category.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.category.model.CategoryResponse
import com.android.bodegas.data.repository.category.model.CategoryEntity

class CategoryEntityMapper : Mapper<CategoryResponse, CategoryEntity> {
    override fun mapFromRemote(type: CategoryResponse): CategoryEntity {
        return CategoryEntity(
            type.id,
            type.description,
            type.name,
            type.pathImage
        )
    }
}