package com.android.bodegas.data.network.user.model

import com.google.gson.annotations.SerializedName

class RegisterUserBodyRemote(
    @SerializedName("address")
    val address: String,
    @SerializedName("businessName")
    val businessName: String,
    @SerializedName("cardNumber")
    val cardNumber: String,
    @SerializedName("cardOperator")
    val cardOperator: String,
    @SerializedName("country")
    val country:String = "PE",
    @SerializedName("creationUser")
    val creationUser: String,
    @SerializedName("departament")
    val department: String,
    @SerializedName("district")
    val district: String,
    @SerializedName("dni")
    val dni: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("lastNameMaternal")
    val lastNameMaternal: String,
    @SerializedName("lastNamePaternal")
    val lastNamePaternal: String,
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("longitude")
    val longitude: Double,
    @SerializedName("name")
    val name: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("phoneNumber")
    val phoneNumber: String,
    @SerializedName("province")
    val province: String,
    @SerializedName("ruc")
    val ruc: String,
    @SerializedName("urbanization")
    val urbanization: String
)