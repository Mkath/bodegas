package com.android.bodegas.data.network.subcategory

import android.content.Context
import android.util.Log
import com.android.bodegas.data.network.subcategory.mapper.SubcategoryEntityMapper
import com.android.bodegas.data.network.subcategory.model.SubCategoryItem
import com.android.bodegas.data.network.subcategory.model.SubCategoryListBody
import com.android.bodegas.data.repository.subcategory.model.SubCategoryEntity
import com.android.bodegas.data.repository.subcategory.store.remote.SubcategoryRemote
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.InputStream
import java.io.InputStreamReader
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.coroutines.CoroutineContext

class SubcategoryRemoteImpl constructor(
    private val subCategoryServices: SubCategoryServices,
    private val categoryEntityMapper: SubcategoryEntityMapper,
    private val context: Context
) : SubcategoryRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getSubcategories(
        establishmentId: Int,
        class_id: Int,
        page: String,
        size: String,
        sortBy: String
    ): Resource<List<SubCategoryEntity>> {
        return coroutineScope {
            try{
                val result = subCategoryServices.getSubCategories(
                    establishmentId,
                    class_id
                )
                if (result.isSuccessful) {
                    if(result.code() == 200){
                        result.body()?.let {
                            val subCategoryEntityList = mapList(result.body()!!.subcategoryListBody)
                            subCategoryEntityList?.let {
                                return@coroutineScope Resource(Status.SUCCESS, subCategoryEntityList, "")
                            }
                        }
                    }else if (result.code() == 204){
                        return@coroutineScope Resource(Status.SUCCESS, listOf<SubCategoryEntity>(), "")
                    }
                }
                return@coroutineScope Resource(Status.ERROR, listOf<SubCategoryEntity>(), "")
            }
            catch(e: UnknownHostException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<SubCategoryEntity>(),
                    "Not Connection"
                )
            }

            catch(e: Throwable){
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<SubCategoryEntity>(),"")
            }
        }
    }

    private fun mapList(subCategoryListBody: SubCategoryItem?): List<SubCategoryEntity>? {
        val categoryList = ArrayList<SubCategoryEntity>()
        subCategoryListBody?.subCategoriesList?.forEach {
            categoryList.add(categoryEntityMapper.mapFromRemote(it))
        }
        return categoryList
    }

    private suspend fun getMockedSubCategories(): Resource<List<SubCategoryEntity>> {
        return coroutineScope {
            delay(100)
            try{
                val gson = Gson()
                val inputStream: InputStream = context.assets.open("mocked_subcategories_list")
                val inputStreamReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
                val classList: SubCategoryListBody = gson.fromJson(inputStreamReader, SubCategoryListBody::class.java)
                val classEntityList = mutableListOf<SubCategoryEntity>()
                classList.subcategoryListBody?.subCategoriesList?.forEach{ it -> classEntityList.add(categoryEntityMapper.mapFromRemote(it))}

                return@coroutineScope Resource<List<SubCategoryEntity>>(
                    Status.SUCCESS,
                    classEntityList,
                    "Mocked categories list"
                )
            }
            catch (e: Throwable){
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<SubCategoryEntity>(), "")
            }
        }
    }

}