package com.android.bodegas.data.repository.user.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.user.model.LoginEntity
import com.android.bodegas.data.repository.user.model.UserEntity
import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.domain.user.login.model.UserLogin

class UserLoginMapper : Mapper<LoginEntity, UserLogin> {

    override fun mapFromEntity(type: LoginEntity): UserLogin {
        return UserLogin(type.token, type.userName)
    }

    override fun mapToEntity(type: UserLogin): LoginEntity {
        return LoginEntity(type.token, type.userName)
    }

}
