package com.android.bodegas.data.repository.departments.store.remote

import com.android.bodegas.data.repository.departments.model.DepartmentEntity
import com.android.bodegas.data.repository.departments.store.DepartmentDataStore
import com.android.bodegas.domain.util.Resource

class DepartmentRemoteDataStorage constructor(private val departmentRemote: DepartmentRemote)  : DepartmentDataStore {
    override suspend fun getDepartments(
    ): Resource<List<DepartmentEntity>> {
        return departmentRemote.getDepartments()
    }
}

