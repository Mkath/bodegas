package com.android.bodegas.data.network.supplier.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.supplier.model.ScheduleResponse
import com.android.bodegas.data.network.supplier.model.SupplierResponse
import com.android.bodegas.data.repository.supplier.model.ScheduleEntity
import com.android.bodegas.data.repository.supplier.model.SupplierEntity

class ScheduleEntityMapper : Mapper<ScheduleResponse, ScheduleEntity> {

    override fun mapFromRemote(type: ScheduleResponse): ScheduleEntity {
        return ScheduleEntity(
            type.range,
            type.startTime,
            type.endTime
        )
    }

}