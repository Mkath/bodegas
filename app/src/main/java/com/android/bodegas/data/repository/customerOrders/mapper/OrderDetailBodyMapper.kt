package com.android.bodegas.data.repository.customerOrders.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailBodyEntity
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailItemEntity
import com.android.bodegas.domain.customerOrders.OrderDetailBody
import com.android.bodegas.domain.customerOrders.OrderDetails

class OrderDetailBodyMapper : Mapper<OrderDetailBodyEntity, OrderDetailBody> {

    override fun mapFromEntity(type: OrderDetailBodyEntity): OrderDetailBody {
        return OrderDetailBody(
            type.orderDetailId,
            type.state
        )
    }

    override fun mapToEntity(type: OrderDetailBody): OrderDetailBodyEntity {
        return OrderDetailBodyEntity(
            type.orderDetailId,
            type.state
        )
    }

}