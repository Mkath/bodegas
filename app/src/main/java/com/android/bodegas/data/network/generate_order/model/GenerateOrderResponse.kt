package com.android.bodegas.data.network.generate_order.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GenerateOrderResponse : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var orderResponse: OrderItem? = null
}

class OrderItem(
    @SerializedName("creationDate")
    val creationDate: String,
    @SerializedName("orderId")
    val orderId: Int
)
