package com.android.bodegas.data.repository.duplicateOrder.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderItemBodyEntity
import com.android.bodegas.domain.duplicate_order.DuplicateOrderItem

class ListDuplicateOrderItemMapper : Mapper<DuplicateOrderItemBodyEntity, DuplicateOrderItem> {

    override fun mapFromEntity(type: DuplicateOrderItemBodyEntity): DuplicateOrderItem {
        return DuplicateOrderItem(
            type.description,
            type.price,
            type.storeProductId,
            type.subTotal
        )
    }

    override fun mapToEntity(type: DuplicateOrderItem): DuplicateOrderItemBodyEntity {
        return DuplicateOrderItemBodyEntity(
            type.description,
            type.price,
            type.storeProductId,
            type.subTotal
        )
    }

}