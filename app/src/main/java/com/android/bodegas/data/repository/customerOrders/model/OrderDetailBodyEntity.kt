package com.android.bodegas.data.repository.customerOrders.model

class OrderDetailBodyEntity (
    val orderDetailId: Int,
    val state: String
)