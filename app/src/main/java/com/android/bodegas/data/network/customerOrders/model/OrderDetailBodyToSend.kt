package com.android.bodegas.data.network.customerOrders.model

import com.android.bodegas.data.network.supplier.model.ScheduleResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class OrderDetailBodyToSend (
    @SerializedName("orderDetailId")
    val orderDetailId: Int,
    @SerializedName("state")
    val state: String
)