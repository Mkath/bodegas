package com.android.bodegas.data.repository.clases.store.remote

import com.android.bodegas.data.repository.clases.model.ClassEntity
import com.android.bodegas.domain.util.Resource

interface ClassRemote {
    suspend fun getClassesBySupplierId(
        establishmentId: Int,
        page: String,
        size: String,
        sortBy: String
    ) : Resource<List<ClassEntity>>
}
