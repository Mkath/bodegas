package com.android.bodegas.data.repository.departments

import com.android.bodegas.data.repository.departments.mapper.DepartmentMapper
import com.android.bodegas.data.repository.departments.model.DepartmentEntity
import com.android.bodegas.data.repository.departments.store.remote.DepartmentRemoteDataStorage
import com.android.bodegas.domain.department.Department
import com.android.bodegas.domain.department.DepartmentRepository
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class DepartmentDataRepository (
    private val departmentRemoteDataStore: DepartmentRemoteDataStorage,
    private val departmentMapper: DepartmentMapper
) : DepartmentRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getDepartments(id: String, name: String): Resource<List<Department>> {
        return coroutineScope {
            val departmentList = departmentRemoteDataStore.getDepartments()
            var resource : Resource<List<Department>> = Resource(departmentList.status, mutableListOf(), departmentList.message)
            if(departmentList.status == Status.SUCCESS)  {
                val mutableListdeparments = mapClassList(departmentList.data)
                resource = Resource(Status.SUCCESS, mutableListdeparments, departmentList.message)
            }
            else {
                if(departmentList.status == Status.ERROR) {
                    resource = Resource(departmentList.status, mutableListOf(), departmentList.message)
                }
            }
            return@coroutineScope resource
        }
    }

    private fun mapClassList(departmentEntityList: List<DepartmentEntity>) : List<Department> {
        val departmentList = mutableListOf<Department>()
        departmentEntityList.forEach{
            departmentList.add(departmentMapper.mapFromEntity(it))
        }
        return departmentList
    }
}