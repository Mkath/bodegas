package com.android.bodegas.data.repository.user.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.user.model.UserLoginTokenEntity
import com.android.bodegas.domain.user.login.model.UserLoginToken

class UserLoginTokenMapper: Mapper<UserLoginTokenEntity, UserLoginToken> {

    override fun mapFromEntity(type: UserLoginTokenEntity): UserLoginToken {
        return UserLoginToken(
            type.idEntity,
            type.token
        )
    }

    override fun mapToEntity(type: UserLoginToken): UserLoginTokenEntity {
        return UserLoginTokenEntity(
            type.idEntity,
            type.token
        )
    }
}