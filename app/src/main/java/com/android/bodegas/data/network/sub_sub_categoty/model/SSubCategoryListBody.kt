package com.android.bodegas.data.network.sub_sub_categoty.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.android.bodegas.data.network.products.model.ProductsResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SSubCategoryListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    lateinit var sSubCategoryListBody: SSubCategoryItem
}

class SSubCategoryItem(
    @SerializedName("total")
    val total: Int,
    @SerializedName("storeProducts")
    val storeProducts: List<ProductsResponse>,
    @SerializedName("subSubCategories")
    val subSubCategories: List<SSubCategoryResponse>
)
