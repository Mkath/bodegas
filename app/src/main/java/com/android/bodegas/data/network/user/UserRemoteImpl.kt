package com.android.bodegas.data.network.user

import android.util.Log
import com.android.bodegas.data.network.ResponseBodyError
import com.android.bodegas.data.network.user.mapper.RegisterUserEntityMapper
import com.android.bodegas.data.network.user.mapper.UserEntityMapper
import com.android.bodegas.data.network.user.mapper.UserLoginTokenEntityMapper
import com.android.bodegas.data.network.user.model.UserBody
import com.android.bodegas.data.repository.user.model.RegisterUserBodyEntity
import com.android.bodegas.data.repository.user.model.UserEntity
import com.android.bodegas.data.repository.user.model.UserLoginTokenEntity
import com.android.bodegas.data.repository.user.store.remote.UserRemote
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.login.ForgetPassDTO
import com.android.bodegas.presentation.profile.ChangePasswordDtoBody
import com.android.bodegas.presentation.profile.UpdateCustomerBody
import com.google.gson.GsonBuilder
import kotlinx.coroutines.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.IOException
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext


class UserRemoteImpl constructor(
    private val userServices: UserServices,
    private val userEntityMapper: UserEntityMapper,
    private val registerUserEntityMapper: RegisterUserEntityMapper,
    private val registerUserTokenLoginMapper: UserLoginTokenEntityMapper
) : UserRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun loginUserToken(
        email: String,
        password: String
    ): Resource<UserLoginTokenEntity> {
        return coroutineScope {
            val userBodySend = UserBody(email, password)
            try {
                val response = userServices.loginUserToken(userBodySend)
                if (response.isSuccessful) {
                    if (response.body()!!.success) {
                        val userResponse = response.body()!!.data!!.login
                        val userEntity =
                            registerUserTokenLoginMapper.mapFromRemote(userResponse)
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            userEntity,
                            response.message()
                        )
                    } else {
                        val emptyUser = UserLoginTokenEntity(
                            "",
                            ""
                        )
                        val message = response.body()!!.message
                        return@coroutineScope Resource(
                            Status.ERROR,
                            emptyUser,
                            message
                        )
                    }
                } else {
                    val emptyEstablishment = UserLoginTokenEntity(
                        "",
                        ""
                    )
                    return@coroutineScope Resource(
                        Status.ERROR,
                        emptyEstablishment,
                        response.message()
                    )
                }
            } catch
                (e: java.net.UnknownHostException) {
                val emptyUser = UserLoginTokenEntity(
                    "",
                    ""
                )
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyUser,
                    "500"
                )
            } catch (e: kotlin.Throwable) {
                val emptyUser =
                    UserLoginTokenEntity(
                        "",
                        ""
                    )
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyUser,
                    e.message
                )
            }
        }
    }

    override suspend fun loginUser(
        establishmentId: Int
    ): Resource<UserEntity> {
        return coroutineScope {
            //val userBody = UserBody(email, password)
            val emptyUserEntity = UserEntity(
                0, "", "", "", "", "", "",
                "", "", "", 0.0, 0.0, "", "", "",
                "", "", "", ""
            )
            try {
                val response = userServices.loginUser(establishmentId)
                if (response.isSuccessful) {
                    if (response.code() == 201) {
                        if (response.body()!!.success) {
                            val body = response.body()?.registryNewUserItem!!.customer
                            val userEntity = userEntityMapper.mapFromRemote(body)
                            return@coroutineScope Resource(
                                Status.SUCCESS,
                                userEntity,
                                ""
                            )
                        } else {
                            return@coroutineScope Resource(
                                Status.SUCCESS,
                                emptyUserEntity,
                                response.body()!!.message
                            )
                        }
                    } else {
                        return@coroutineScope Resource(
                            Status.ERROR,
                            emptyUserEntity,
                            response.message()
                        )
                    }

                } else {
                    return@coroutineScope Resource(
                        Status.ERROR,
                        emptyUserEntity,
                        response.message()
                    )
                }
            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyUserEntity,
                    "500"
                )
            } catch (e: Throwable) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyUserEntity,
                    e.message
                )
            }
        }
    }

    override suspend fun registerUser(
        registerUserBodyEntity: RegisterUserBodyEntity
    ): Resource<UserLoginTokenEntity> {
        return coroutineScope {
            val createUserBody = registerUserEntityMapper.mapFromRemote(registerUserBodyEntity)
            /*val emptyUser = UserEntity(
                0, "", "", "", "", "", "",
                "", "", "", 0.0, 0.0, "", "", "",
                "", "", "", ""
            )*/
            try {
                val response = userServices.registerUser(createUserBody)
                if (response.isSuccessful) {
                    if(response.body()!!.success){
                        val body = response.body()!!.data!!.login
                        val createUserEntity = registerUserTokenLoginMapper.mapFromRemote(body)
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            createUserEntity,
                            response.message()
                        )
                    }else{
                        val emptyUser = UserLoginTokenEntity(
                            "",
                            ""
                        )
                        return@coroutineScope Resource(
                            Status.ERROR,
                            emptyUser,
                            response.body()!!.message
                        )
                    }

                } else {
                    val gson = GsonBuilder().create()
                    var mError = ResponseBodyError()
                    try {
                        mError = gson.fromJson(
                            response.errorBody()!!.string(),
                            ResponseBodyError::class.java
                        )
                        val emptyUser = UserLoginTokenEntity(
                            "",
                            ""
                        )
                        return@coroutineScope Resource(
                            Status.ERROR,
                            emptyUser,
                            mError.fields[0].fieldName + " - " + mError.fields[0].message
                        )
                    } catch (e: IOException) {
                        val emptyUser = UserLoginTokenEntity(
                            "",
                            ""
                        )
                        return@coroutineScope Resource(
                            Status.ERROR,
                            emptyUser,
                            "Ha ocurrido un error, intente nuevamente por favor"
                        )
                    }
                }
            } catch (e: UnknownHostException) {
                val emptyUser = UserLoginTokenEntity(
                    "",
                    ""
                )
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyUser,
                    "Error de conexión"
                )
            } catch (e: Throwable) {
                val emptyUser = UserLoginTokenEntity(
                    "",
                    ""
                )
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyUser,
                    "Ha ocurrido un error, intente nuevamente por favor"
                )
            }
        }
    }

    override suspend fun updatePhotoUser(customerId: Int, filePath: String): Resource<Boolean> {
        return coroutineScope {
            val body = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("file", filePath, RequestBody.create(MediaType.parse("application/octet-stream"),
            File(filePath)))
                .addFormDataPart("table", "Customer").build() as RequestBody
            try {
                val response = userServices.updatePhotoUser(customerId, body)
                if(response.isSuccessful){
                    if(response.body()!!.success){
                        val body = response.body()!!.fileResponse!!.file.fileDownloadUri
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            true,
                            body
                        )
                    }
                   else{
                        return@coroutineScope Resource(
                            Status.ERROR,
                            false,
                            "Ha ocurrido un error al subir su imagen, intente nuevamente por favor"
                        )
                    }
                }else{
                    return@coroutineScope Resource(
                        Status.ERROR,
                        false,
                        "Ha ocurrido un error al subir su imagen, intente nuevamente por favor"
                    )
                }
            }catch (e:IOException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    false,
                    "Ha ocurrido un error, intente nuevamente por favor"
                )
            }
        }
    }

    override suspend fun updatePassword(body: ChangePasswordDtoBody): Resource<Boolean> {
        return coroutineScope {
            try {
                val response = userServices.updatePassword(body)
                if(response.isSuccessful){
                    if(response.body()!!.success){
                        val body = response.body()!!
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            true,
                            body.message
                        )
                    }
                    else{
                        return@coroutineScope Resource(
                            Status.ERROR,
                            false,
                            "Ha ocurrido un error al actualizar la contraseña, intente nuevamente por favor"
                        )
                    }
                }else{
                    return@coroutineScope Resource(
                        Status.ERROR,
                        false,
                        "Ha ocurrido un error al actualizar la contraseña, intente nuevamente por favor"
                    )
                }
            }catch (e:IOException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    false,
                    "Ha ocurrido un error, intente nuevamente por favor"
                )
            }
        }
    }

    override suspend fun recoveryPassword(body: ForgetPassDTO): Resource<Boolean> {
        return coroutineScope {
            try {
                val response = userServices.recoveryPassword(body)
                if(response.isSuccessful){
                    if(response.body()!!.success){
                        val body = response.body()!!
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            true,
                            body.message
                        )
                    }
                    else{
                        return@coroutineScope Resource(
                            Status.ERROR,
                            false,
                            "Ha ocurrido un error al enviar mail, intente nuevamente por favor"
                        )
                    }
                }else{
                    return@coroutineScope Resource(
                        Status.ERROR,
                        false,
                        "Ha ocurrido un error al enviar mail, intente nuevamente por favor"
                    )
                }
            }catch (e:IOException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    false,
                    "Ha ocurrido un error, intente nuevamente por favor"
                )
            }
        }
    }

    override suspend fun updateUser(customerId: Int, body_: UpdateCustomerBody): Resource<UserEntity> {
        return coroutineScope {
            var emptyEstablishment = UserEntity(
                0,
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                0.0,
                0.0,
                "",
                "",
                "",
                "",
                "",
                "",
                ""
            )
            try {
                val response = userServices.updateUsers(customerId, body_)
                if (response.isSuccessful) {
                    if (response.body()!!.success) {
                        val body = response.body()?.registryNewUserItem!!.customer
                        val userEntity = userEntityMapper.mapFromRemote(body)
                        //val createUserEntity = establishmentEntityMapper.mapFromRemote(body.establishmentItem.establishment)
                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            userEntity,
                            response.message()
                        )
                    } else {
                        return@coroutineScope Resource(
                            Status.ERROR,
                            emptyEstablishment,
                            "Ha ocurrido un error al actualizar los datos, intente nuevamente por favor"
                        )
                    }
                } else {
                    return@coroutineScope Resource(
                        Status.ERROR,
                        emptyEstablishment,
                        "Ha ocurrido un error al actualizar los datos, intente nuevamente por favor"
                    )
                }
            } catch (e: java.io.IOException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyEstablishment,
                    "Ha ocurrido un error, intente nuevamente por favor"
                )
            }
        }
    }

}