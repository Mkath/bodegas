package com.android.bodegas.data.repository.save_order.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.save_order.model.SaveOrderEntity
import com.android.bodegas.domain.save_order.SaveOrder

class SaveOrderMapper : Mapper<SaveOrderEntity, SaveOrder> {

    override fun mapFromEntity(type: SaveOrderEntity): SaveOrder {
        return SaveOrder(
            type.id,
            type.storeProductId,
            type.price,
            type.quantity,
            type.subtotal,
            type.unitMeasure,
            type.observation,
            type.name,
            type.imageProduct
        )
    }

    override fun mapToEntity(type: SaveOrder): SaveOrderEntity {
        return SaveOrderEntity(type.id,
            type.storeProductId,
            type.price,
            type.quantity,
            type.subtotal,
            type.unitMeasure,
            type.observation,
            type.name,
            type.imageProduct
        )
    }

}