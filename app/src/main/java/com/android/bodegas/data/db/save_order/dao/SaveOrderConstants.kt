package com.android.bodegas.data.db.save_order.dao

object SaveOrderConstants {

    const val TABLE_NAME = "save_order"

    const val QUERY_ORDER = "SELECT * FROM" + " " + TABLE_NAME

    const val QUERY_ORDER_BY_ID = "SELECT * FROM $TABLE_NAME WHERE id=:orderId"

    const val DELETE_ORDER= "DELETE FROM" + " " + TABLE_NAME

    const val DELETE_ORDER_BY_ID = "DELETE FROM $TABLE_NAME WHERE id=:orderId"


}