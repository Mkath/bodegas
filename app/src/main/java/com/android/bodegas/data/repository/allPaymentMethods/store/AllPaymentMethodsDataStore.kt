package com.android.bodegasadmin.data.repository.allPaymentMethods.store

import com.android.bodegas.domain.util.Resource
import com.android.bodegasadmin.data.repository.allPaymentMethods.model.AllPaymentMethodsEntity

interface AllPaymentMethodsDataStore {

    suspend fun getAllPaymentMethods(): Resource<List<AllPaymentMethodsEntity>>

}