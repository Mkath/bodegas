package com.android.bodegas.data.network.customerOrders.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.customerOrders.model.CustomerListOrdersBodyResponse
import com.android.bodegas.data.network.customerOrders.model.OrderDetailItem
import com.android.bodegas.data.repository.customerOrders.model.CustomerOrdersEntity
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailItemEntity

class CustomerOrdersEntityMapper(
    private val establishmentOrdersEntityMapper: EstablishmentOrdersEntityMapper,
    private val orderDetailItemsEntityMapper: OrderDetailItemEntityMapper
) :
    Mapper<CustomerListOrdersBodyResponse, CustomerOrdersEntity> {

    override fun mapFromRemote(type: CustomerListOrdersBodyResponse): CustomerOrdersEntity {
        val delivery = if (type.deliveryCharge.isNullOrEmpty()) {
            "0.00"
        } else {
            type.deliveryCharge

        }
        val ammount = if (type.customerAmount.isNullOrEmpty()) {
            ""
        } else {
            type.customerAmount
        }

        val message = if (type.paymentMethod.customerMessage.isNullOrEmpty()) {
            ""
        } else {
            type.paymentMethod.customerMessage
        }
        return CustomerOrdersEntity(
            type.orderId.toString(),
            type.deliveryType,
            type.creationDate.orEmpty(),
            type.shippingDateFrom.orEmpty(),
            type.shippingDateUntil.orEmpty(),
            type.status,
            establishmentOrdersEntityMapper.mapFromRemote(type.establishment),
            type.total,
            getOrdersDetail(type.orderDetail),
            type.updateDate,
            delivery,
            ammount,
            message,
            type.paymentMethod.description.orEmpty(),
            type.paymentMethod.establishmentMessage.orEmpty(),
            type.paymentMethod.paymentMethodId,
            type.paymentMethod.requestedAmount
        )
    }

    private fun getOrdersDetail(list: List<OrderDetailItem>): List<OrderDetailItemEntity> {
        val listOrdersDetail = mutableListOf<OrderDetailItemEntity>()
        list.forEach {
            listOrdersDetail.add(orderDetailItemsEntityMapper.mapFromRemote(it))
        }
        return listOrdersDetail
    }
}