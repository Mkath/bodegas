package com.android.bodegas.data.network.duplicateorder.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.duplicateorder.model.DuplicateOrderItem
import com.android.bodegas.data.network.duplicateorder.model.DuplicateOrderItemBody
import com.android.bodegas.data.network.duplicateorder.model.DuplicateOrderResponse
import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderEntity
import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderItemBodyEntity

class DuplicateOrderEntityMapperNetwork(
    private val puplicateOrderItemEntityMapper: DuplicateOrderItemEntityMapper
): Mapper<DuplicateOrderResponse, DuplicateOrderEntity> {
    override fun mapFromRemote(type: DuplicateOrderResponse): DuplicateOrderEntity {
        return DuplicateOrderEntity (
            type.establishmentId,
            type.flagAllAvailableProducts,
            getListDuplicateOrderItem(type.orderDetailRequest)
        )
    }

    private fun getListDuplicateOrderItem(list: List<DuplicateOrderItemBody>): List<DuplicateOrderItemBodyEntity>{
        val listItems = mutableListOf<DuplicateOrderItemBodyEntity>()
        if(list.isNotEmpty()){
            list.forEach {
                listItems.add(puplicateOrderItemEntityMapper.mapFromRemote(it))
            }
        }
        return listItems
    }
}