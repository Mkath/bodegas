package com.android.bodegas.data.network.user.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.user.model.RegisterUserBodyRemote
import com.android.bodegas.data.network.user.model.UserResponse
import com.android.bodegas.data.repository.user.model.RegisterUserBodyEntity
import com.android.bodegas.data.repository.user.model.UserEntity

class RegisterUserEntityMapper : Mapper<RegisterUserBodyEntity, RegisterUserBodyRemote> {

    override fun mapFromRemote(type: RegisterUserBodyEntity): RegisterUserBodyRemote {
        return RegisterUserBodyRemote(
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.country,
            type.creationUser,
            type.department,
            type.district,
            type.dni,
            type.email,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.password,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.urbanization
        )
    }

}
