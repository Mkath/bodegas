package com.android.bodegas.data.network.sub_sub_categoty.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.products.mapper.ProductsEntityMapper
import com.android.bodegas.data.network.products.model.ProductsResponse
import com.android.bodegas.data.network.sub_sub_categoty.model.SSubCategoryItem
import com.android.bodegas.data.network.sub_sub_categoty.model.SSubCategoryResponse
import com.android.bodegas.data.repository.produtcs.model.ProductsEntity
import com.android.bodegas.data.repository.sub_sub_category.model.GroupEntity
import com.android.bodegas.data.repository.sub_sub_category.model.SSubCategoryEntity

class GroupEntityMapper(
    private val productsEntityMapper: ProductsEntityMapper,
    private val sSubcategoryEntityMapper: SSubcategoryEntityMapper
) : Mapper<SSubCategoryItem, GroupEntity> {

    override fun mapFromRemote(type: SSubCategoryItem): GroupEntity {
        return GroupEntity(
            type.total,
            getProductsEntityList(type.storeProducts),
            getSSEntityList(type.subSubCategories)
        )
    }

    private fun getProductsEntityList(list: List<ProductsResponse>): List<ProductsEntity> {
        val productsEntityList = ArrayList<ProductsEntity>()
        if (!list.isNullOrEmpty()) {
            list.forEach {
                productsEntityList.add(productsEntityMapper.mapFromRemote(it))
            }
        }
        return productsEntityList
    }

    private fun getSSEntityList(list: List<SSubCategoryResponse>): List<SSubCategoryEntity> {
        val ssEntityList = ArrayList<SSubCategoryEntity>()
        if (!list.isNullOrEmpty()) {
            list.forEach {
                ssEntityList.add(sSubcategoryEntityMapper.mapFromRemote(it))
            }
        }
        return ssEntityList
    }

}