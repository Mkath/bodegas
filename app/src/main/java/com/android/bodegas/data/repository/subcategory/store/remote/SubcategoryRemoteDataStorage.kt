package com.android.bodegas.data.repository.subcategory.store.remote

import com.android.bodegas.data.repository.subcategory.model.SubCategoryEntity
import com.android.bodegas.data.repository.subcategory.store.SubcategoryDataStore
import com.android.bodegas.domain.util.Resource

class SubcategoryRemoteDataStorage constructor(private val subcategory_Remote: SubcategoryRemote) : SubcategoryDataStore {
    override suspend fun getSubcategories(
        establishmentId: Int,
        category_id: Int,
        page: String,
        size: String,
        sortBy: String
    ): Resource<List<SubCategoryEntity>> {
        return subcategory_Remote.getSubcategories(establishmentId, category_id, page, size, sortBy)
    }

}
