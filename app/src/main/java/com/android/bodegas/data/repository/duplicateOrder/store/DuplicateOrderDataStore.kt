package com.android.bodegas.data.repository.duplicateOrder.store

import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderEntity
import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderItemBodyEntity
import com.android.bodegas.domain.util.Resource

interface DuplicateOrderDataStore {

    suspend fun getDuplicateOrder(establishmentId: Int, orderId: Int): Resource<DuplicateOrderEntity>

}