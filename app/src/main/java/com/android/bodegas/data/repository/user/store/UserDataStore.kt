package com.android.bodegas.data.repository.user.store

import com.android.bodegas.data.repository.user.model.LoginEntity
import com.android.bodegas.data.repository.user.model.RegisterUserBodyEntity
import com.android.bodegas.data.repository.user.model.UserEntity
import com.android.bodegas.data.repository.user.model.UserLoginTokenEntity
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.presentation.login.ForgetPassDTO
import com.android.bodegas.presentation.profile.ChangePasswordDtoBody
import com.android.bodegas.presentation.profile.UpdateCustomerBody

interface UserDataStore {

    suspend fun loginUserToken(email: String, password: String): Resource<UserLoginTokenEntity>

    suspend fun loginUser(establishmentId: Int): Resource<UserEntity>

    suspend fun saveUser(userEntity: UserEntity)

    suspend fun clearUser()

    suspend fun registerUser(registerUserBodyEntity: RegisterUserBodyEntity) : Resource<UserLoginTokenEntity>

    suspend fun updatePhotoUser(customerId: Int, filePath: String): Resource<Boolean>

    suspend fun updatePassword( body: ChangePasswordDtoBody): Resource<Boolean>

    suspend fun recoveryPassword( body: ForgetPassDTO): Resource<Boolean>

    suspend fun updateUser(customerId: Int, body: UpdateCustomerBody): Resource<UserEntity>
}