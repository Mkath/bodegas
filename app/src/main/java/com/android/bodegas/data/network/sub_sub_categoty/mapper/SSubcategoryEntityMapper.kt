package com.android.bodegas.data.network.sub_sub_categoty.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.products.model.ProductsResponse
import com.android.bodegas.data.network.sub_sub_categoty.model.SSubCategoryResponse
import com.android.bodegas.data.network.supplier.model.SupplierResponse
import com.android.bodegas.data.repository.produtcs.model.ProductsEntity
import com.android.bodegas.data.repository.sub_sub_category.model.SSubCategoryEntity
import com.android.bodegas.data.repository.supplier.model.SupplierEntity

class SSubcategoryEntityMapper : Mapper<SSubCategoryResponse, SSubCategoryEntity> {

    override fun mapFromRemote(type: SSubCategoryResponse): SSubCategoryEntity {
        return SSubCategoryEntity(
            type.description,
            type.name,
            type.id,
            type.pathImage
        )
    }

}