package com.android.bodegas.data.network.supplier.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ScheduleResponse(
    @SerializedName("range")
    @Expose
    val range: String,
    @SerializedName("startTime")
    @Expose
    val startTime: String ? = "",
    @SerializedName("endTime")
    @Expose
    val endTime: String ? = ""
)