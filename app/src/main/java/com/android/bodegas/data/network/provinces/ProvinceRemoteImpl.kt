package com.android.bodegas.data.network.provinces

import android.content.Context
import com.android.bodegas.data.network.provinces.mapper.ProvinceEntityMapper
import com.android.bodegas.data.network.provinces.model.ProvinceItem
import com.android.bodegas.data.network.provinces.model.ProvinceListBody
import com.android.bodegas.data.repository.provinces.model.ProvinceEntity
import com.android.bodegas.data.repository.provinces.store.remote.ProvinceRemote
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.InputStream
import java.io.InputStreamReader
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.coroutines.CoroutineContext

class ProvinceRemoteImpl constructor(private val provinceServices: ProvinceService,
                                     private val provinceEntityMapper: ProvinceEntityMapper,
                                     private val context: Context
) : ProvinceRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getProvinces(
        id: String,
        name: String,
        department_id: String
    ): Resource<List<ProvinceEntity>> {
        return coroutineScope {
            try{
                /*val result = provinceServices.getProvinces()

                if (result.isSuccessful) {
                    result.body()?.let {
                        val provincesEntityList = mapList(result.body()!!.provinceListBody)
                        provincesEntityList?.let {
                            return@coroutineScope Resource(Status.SUCCESS, provincesEntityList, "")
                        }
                    }
                }
                return@coroutineScope Resource(Status.ERROR, listOf<ProvinceEntity>(), "")*/
                return@coroutineScope getMockedProvinces()
            }
            catch(e: UnknownHostException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<ProvinceEntity>(),
                    "Not Connection"
                )
            }
            catch(e: Throwable){
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<ProvinceEntity>(),"")
            }
        }
    }

    private fun mapList(provinceListBody: ProvinceItem?): List<ProvinceEntity>? {
        val provinceList = ArrayList<ProvinceEntity>()
        provinceListBody?.provinces?.forEach{
            provinceList.add(provinceEntityMapper.mapFromRemote(it))
        }
        return provinceList
    }

    private suspend fun getMockedProvinces(): Resource<List<ProvinceEntity>> {
        return coroutineScope {
            delay(1000)
            try{
                val gson = Gson()
                val inputStream: InputStream = context.assets.open("peru_provincias")
                val inputStreamReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
                val provincesList: ProvinceListBody = gson.fromJson(inputStreamReader, ProvinceListBody::class.java)
                val provinceEntityList = mutableListOf<ProvinceEntity>()
                provincesList.provinceListBody?.provinces?.forEach{ it -> provinceEntityList.add(provinceEntityMapper.mapFromRemote(it))}

                return@coroutineScope Resource<List<ProvinceEntity>>(
                    Status.SUCCESS,
                    provinceEntityList,
                    "Mocked classes list"
                )
            }
            catch (e: Throwable){
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<ProvinceEntity>(), "")
            }
        }
    }
}
