package com.android.bodegas.data.network.duplicateorder.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.duplicateorder.model.DuplicateOrderItemBody
import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderItemBodyEntity

class DuplicateOrderItemEntityMapper : Mapper<DuplicateOrderItemBody, DuplicateOrderItemBodyEntity> {

    override fun mapFromRemote(type: DuplicateOrderItemBody): DuplicateOrderItemBodyEntity {
        return DuplicateOrderItemBodyEntity(
            type.description,
            type.price,
            type.storeProductId,
            type.subTotal
        )
    }

}