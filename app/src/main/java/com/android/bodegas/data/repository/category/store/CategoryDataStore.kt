package com.android.bodegas.data.repository.category.store

import com.android.bodegas.data.repository.category.model.CategoryEntity

import com.android.bodegas.domain.util.Resource

interface CategoryDataStore {
    suspend fun getCategories(
        establishmentId: Int,
        class_id: Int,
        page: String,
        size: String,
        sortBy: String
    ) : Resource<List<CategoryEntity>>
}