package com.android.bodegas.data.network.district.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DistrictListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var districtListBody: DistrictItem? = null
}

class DistrictItem (
    @SerializedName("districtsTotal")
    val districtsTotal: Int,
    @SerializedName("districts")
    val districts: List<DistrictResponse>
)