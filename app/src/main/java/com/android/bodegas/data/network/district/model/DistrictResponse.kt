package com.android.bodegas.data.network.district.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DistrictResponse (
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("province_id")
    @Expose
    val province_id: String,
    @SerializedName("department_id")
    @Expose
    val department_id: String
)