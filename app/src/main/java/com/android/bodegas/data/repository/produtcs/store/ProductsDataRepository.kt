package com.android.bodegas.data.repository.produtcs.store

import com.android.bodegas.data.repository.produtcs.mapper.ProductsMapper
import com.android.bodegas.data.repository.produtcs.model.ProductsEntity
import com.android.bodegas.data.repository.produtcs.store.remote.ProductsRemoteDataStore
import com.android.bodegas.domain.products.Products
import com.android.bodegas.domain.products.ProductsRepository
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class ProductsDataRepository(
    private val productsRemoteDataStore: ProductsRemoteDataStore,
    private val productsMapper: ProductsMapper
) : ProductsRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getProducts(
        establishmentId: Int,
        supplierDocument: String,
        subCategoryId: Int
    ): Resource<List<Products>> {
        return coroutineScope {
            val productsList =
                productsRemoteDataStore.getProducts(establishmentId, supplierDocument, subCategoryId)

            var resource: Resource<List<Products>> =
                Resource(productsList.status, mutableListOf(), productsList.message)

            if (productsList.status == Status.SUCCESS) {
                val mutableListFarms = mapSupplierList(productsList.data)
                resource = Resource(Status.SUCCESS, mutableListFarms, productsList.message)
            } else {
                if (productsList.status == Status.ERROR) {
                    resource = Resource(productsList.status, mutableListOf(), productsList.message)
                }
            }

            return@coroutineScope resource
        }
    }

    override suspend fun searchProducts(searchText: String, storeId:Int): Resource<List<Products>> {
        return coroutineScope {
            val productsList =
                productsRemoteDataStore.searchProducts(searchText, storeId)

            var resource: Resource<List<Products>> =
                Resource(productsList.status, mutableListOf(), productsList.message)

            if (productsList.status == Status.SUCCESS) {
                val mutableListFarms = mapSupplierList(productsList.data)
                resource = Resource(Status.SUCCESS, mutableListFarms, productsList.message)
            } else {
                if (productsList.status == Status.ERROR) {
                    resource = Resource(productsList.status, mutableListOf(), productsList.message)
                }
            }

            return@coroutineScope resource
        }
    }

    private fun mapSupplierList(productsEntityList: List<ProductsEntity>): List<Products> {
        val productsList = mutableListOf<Products>()
        productsEntityList.forEach {
            productsList.add(productsMapper.mapFromEntity(it))
        }
        return productsList
    }
}