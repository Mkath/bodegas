package com.android.bodegas.data.repository.user.store.remote

import com.android.bodegas.data.repository.user.model.RegisterUserBodyEntity
import com.android.bodegas.data.repository.user.model.UserEntity
import com.android.bodegas.data.repository.user.model.UserLoginTokenEntity
import com.android.bodegas.data.repository.user.store.UserDataStore
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.presentation.login.ForgetPassDTO
import com.android.bodegas.presentation.profile.ChangePasswordDtoBody
import com.android.bodegas.presentation.profile.UpdateCustomerBody


class UserRemoteDataStore constructor(private val userRemote: UserRemote) :
    UserDataStore {

    override suspend fun loginUserToken(email: String, password: String): Resource<UserLoginTokenEntity> {
        return userRemote.loginUserToken(email, password)
    }

    override suspend fun loginUser(establishmentId: Int): Resource<UserEntity> {
        return userRemote.loginUser(establishmentId)
    }

    override suspend fun saveUser(userEntity: UserEntity) {
        throw UnsupportedOperationException()
    }

    override suspend fun clearUser() {
        throw UnsupportedOperationException()
    }

    override suspend fun registerUser(
       registerUserBodyEntity: RegisterUserBodyEntity
    ): Resource<UserLoginTokenEntity> {
        return userRemote.registerUser(registerUserBodyEntity)
    }

    override suspend fun updatePhotoUser(customerId: Int, filePath: String): Resource<Boolean> {
        return userRemote.updatePhotoUser(customerId, filePath)
    }

    override suspend fun updatePassword( body: ChangePasswordDtoBody): Resource<Boolean> {
        return userRemote.updatePassword(body)
    }

    override suspend fun recoveryPassword( body: ForgetPassDTO): Resource<Boolean> {
        return userRemote.recoveryPassword(body)
    }

    override suspend fun updateUser(customerId: Int, body: UpdateCustomerBody): Resource<UserEntity> {
        return userRemote.updateUser(customerId, body)
    }

}