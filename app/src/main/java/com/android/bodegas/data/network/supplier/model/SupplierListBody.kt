package com.android.bodegas.data.network.supplier.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SupplierListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var supplierListBody: SupplierItem? = null
}

class SupplierItem(
    @SerializedName("establishmentsTotal")
    val suppliersTotal: Int,
    @SerializedName("establishments")
    val supplierList: List<SupplierResponse>
)
