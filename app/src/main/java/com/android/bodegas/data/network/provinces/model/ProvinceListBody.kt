package com.android.bodegas.data.network.provinces.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProvinceListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var provinceListBody: ProvinceItem? = null
}

class ProvinceItem (
    @SerializedName("provincesTotal")
    val provinceTotal: Int,
    @SerializedName("provinces")
    val provinces: List<ProvinceResponse>
)