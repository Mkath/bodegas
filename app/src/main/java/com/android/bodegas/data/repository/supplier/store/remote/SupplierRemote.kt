package com.android.bodegas.data.repository.supplier.store.remote

import com.android.bodegas.data.repository.supplier.model.SupplierEntity
import com.android.bodegas.domain.util.Resource


interface SupplierRemote {

    suspend fun getSupplierByType(
        type: Int,
        latitude: String,
        longitude: String,
        radius: Int
    ): Resource<List<SupplierEntity>>

}