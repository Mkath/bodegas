package com.android.bodegas.data.repository.category.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.category.model.CategoryEntity
import com.android.bodegas.domain.category.Category

class CategoryMapper : Mapper<CategoryEntity, Category> {
    override fun mapFromEntity(type: CategoryEntity): Category {
        return Category(
            type.id,
            type.description,
            type.name,
            type.pathImage
        )
    }

    override fun mapToEntity(type: Category): CategoryEntity {
        return CategoryEntity(
            type.id,
            type.description,
            type.name,
            type.pathImage
        )
    }

}