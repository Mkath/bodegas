package com.android.bodegasadmin.data.repository.allPaymentMethods.model

data class AllPaymentMethodsEntity(
    val paymentMethodId: Int,
    val requestedAmount: Boolean,
    val description: String,
    val customerMessage: String
)