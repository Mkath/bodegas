package com.android.bodegas.data.repository.clases.model

class ClassEntity(
    val id: Int,
    val description: String,
    val name: String,
    val pathImage: String ? = ""
)