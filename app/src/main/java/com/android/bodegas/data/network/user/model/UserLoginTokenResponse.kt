package com.android.bodegas.data.network.user.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserLoginTokenResponse (
    @SerializedName("idEntity")
    @Expose
    val id_entity: String,
    @SerializedName("token")
    @Expose
    val token: String
)