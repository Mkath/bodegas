package com.android.bodegas.data.repository.user.store.db

import com.android.bodegas.data.repository.user.model.LoginEntity
import com.android.bodegas.data.repository.user.model.UserEntity
import com.android.bodegas.domain.util.Resource

interface UserDb {

    suspend fun getUser(): Resource<UserEntity>

    suspend fun saveUser(userEntity: UserEntity)

    suspend fun clearAllTables()
}