package com.android.bodegas.data.network.user.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.user.model.UserResponse
import com.android.bodegas.data.repository.user.model.UserEntity

class UserEntityMapper : Mapper<UserResponse, UserEntity> {

    override fun mapFromRemote(type: UserResponse): UserEntity {
        val ruc = if(type.ruc.isNullOrEmpty()){
            ""
        }else{
            type.ruc
        }
        return UserEntity(
            type.customerId,
            type.creationDate,
            type.creationUser,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.address,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            ruc,
            type.status,
            type.urbanization,
            type.pathImage
        )
    }

}
