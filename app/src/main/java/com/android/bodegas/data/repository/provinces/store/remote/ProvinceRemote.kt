package com.android.bodegas.data.repository.provinces.store.remote

import com.android.bodegas.data.repository.provinces.model.ProvinceEntity
import com.android.bodegas.domain.util.Resource

interface ProvinceRemote {
    suspend fun getProvinces (
        id: String,
        name: String,
        department_id: String
    ): Resource<List<ProvinceEntity>>
}