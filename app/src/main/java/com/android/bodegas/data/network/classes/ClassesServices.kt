package com.android.bodegas.data.network.classes

import com.android.bodegas.data.network.classes.model.ClassListBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ClassesServices {
    @GET("/establishments/{establishment-id}/classes")
    suspend fun getClassesBySupplierId(
        @Path("establishment-id") establishment_id: Int
    ) : Response<ClassListBody>
}