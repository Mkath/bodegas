package com.android.bodegas.data.repository.duplicateOrder.store.remote

import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderEntity
import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderItemBodyEntity
import com.android.bodegas.domain.util.Resource

interface DuplicateOrderRemote {

    suspend fun getDuplicateOrder(establishmentId: Int, orderId: Int): Resource<DuplicateOrderEntity>
}