package com.android.bodegas.data.repository.districts.store.remote

import com.android.bodegas.data.repository.districts.model.ProvinceEntity
import com.android.bodegas.data.repository.districts.store.DistrictDataStore
import com.android.bodegas.domain.util.Resource

class DistrictRemoteDataStorage constructor(private val districtRemote: DistrictRemote)  : DistrictDataStore {
    override suspend fun getDistricts(
        id: String,
        name: String,
        province_id: String,
        department_id: String

    ): Resource<List<ProvinceEntity>> {
        return districtRemote.getDistricts( id, name, province_id, department_id)
    }
}

