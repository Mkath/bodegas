package com.android.bodegas.data.network.allPaymentMethods.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AllPaymentMethodsResponse(
    @SerializedName("paymentMethodId")
    @Expose
    val paymentMethodId: Int,
    @SerializedName("requestedAmount")
    @Expose
    val requestedAmount: Boolean,
    @SerializedName("description")
    @Expose
    val description: String,
    @SerializedName("customerMessage")
    @Expose
    val customerMessage: String
)