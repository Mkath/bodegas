package com.android.bodegas.data.db.user.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.bodegas.data.db.user.model.UserDb

@Dao
abstract class UserDao {

    @Query(UserConstants.QUERY_USER)
    abstract suspend fun getUser(): UserDb

    @Query(UserConstants.DELETE_USER)
    abstract suspend fun clearUser()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertUser(userDb: UserDb)

}