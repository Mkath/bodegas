package com.android.bodegas.data.repository.user

import com.android.bodegas.data.repository.user.mapper.RegisterUserMapper
import com.android.bodegas.data.repository.user.mapper.UserLoginTokenMapper
import com.android.bodegas.data.repository.user.mapper.UserMapper
import com.android.bodegas.data.repository.user.store.db.UserDbDataStore
import com.android.bodegas.data.repository.user.store.remote.UserRemoteDataStore
import com.android.bodegas.domain.user.UserRepository
import com.android.bodegas.domain.user.create.model.RegisterUserBody
import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.domain.user.login.model.UserLoginToken
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.login.ForgetPassDTO
import com.android.bodegas.presentation.profile.ChangePasswordDtoBody
import com.android.bodegas.presentation.profile.UpdateCustomerBody
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class UserDataRepository constructor(
    private val userDbDataStore: UserDbDataStore,
    private val userRemoteDataStore: UserRemoteDataStore,
    private val userMapper: UserMapper,
    private val registerUserMapper: RegisterUserMapper,
    private val userLoginMapper: UserLoginTokenMapper
) : UserRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun loginUserToken(
        email: String,
        password: String
    ): Resource<UserLoginToken> {
        return coroutineScope {
            val loginUserResponse =
                userRemoteDataStore. loginUserToken(email, password)
            val user = userLoginMapper.mapFromEntity(loginUserResponse.data)
            // establishmentDbDataStore.saveEstablishment(establishmentMapper.mapToEntity(establishment))
            return@coroutineScope Resource(
                loginUserResponse.status,
                user,
                loginUserResponse.message
            )
        }
    }

    override suspend fun loginUser(
        establishmentId: Int
    ): Resource<User> {
        return coroutineScope {
            val loginUserResponse = userRemoteDataStore.loginUser(establishmentId)
            val user = userMapper.mapFromEntity(loginUserResponse.data)
            if (loginUserResponse.status == Status.SUCCESS) {
                userDbDataStore.saveUser(userMapper.mapToEntity(user))
            }
            return@coroutineScope Resource(
                loginUserResponse.status,
                user,
                loginUserResponse.message
            )
        }
    }

    override suspend fun logoutUser() {
        return coroutineScope {
            userDbDataStore.clearUser()
        }
    }

    override suspend fun registerUser(
        registerUserBody: RegisterUserBody
    ): Resource<UserLoginToken> {
        return coroutineScope {
            val createUserResponse = userRemoteDataStore.registerUser(
                registerUserMapper.mapToEntity(registerUserBody)
            )
            val user = userLoginMapper.mapFromEntity(createUserResponse.data)
            return@coroutineScope Resource(
                createUserResponse.status,
                user,
                createUserResponse.message
            )
        }
    }

    override suspend fun updatePhotoUser(customerId: Int, filePath: String): Resource<Boolean> {
        return coroutineScope {
            val updateUserResponse = userRemoteDataStore.updatePhotoUser(customerId, filePath)
            return@coroutineScope Resource(
                updateUserResponse.status,
                updateUserResponse.data,
                updateUserResponse.message
            )
        }

    }


    override suspend fun updatePassword(body: ChangePasswordDtoBody): Resource<Boolean> {
        return coroutineScope {
            val updateUserResponse = userRemoteDataStore.updatePassword(body)
            return@coroutineScope Resource(
                updateUserResponse.status,
                updateUserResponse.data,
                updateUserResponse.message
            )
        }
    }

    override suspend fun recoveryPassword(body: ForgetPassDTO): Resource<Boolean> {
        return coroutineScope {
            val updateUserResponse = userRemoteDataStore.recoveryPassword(body)
            return@coroutineScope Resource(
                updateUserResponse.status,
                updateUserResponse.data,
                updateUserResponse.message
            )
        }
    }

    override suspend fun updateUser(customerId: Int, body: UpdateCustomerBody): Resource<User> {
        return coroutineScope {
            val updateUserResponse = userRemoteDataStore.updateUser(customerId, body)
            val user = userMapper.mapFromEntity(updateUserResponse.data)
            return@coroutineScope Resource(
                updateUserResponse.status,
                user,
                updateUserResponse.message
            )
        }
    }

}