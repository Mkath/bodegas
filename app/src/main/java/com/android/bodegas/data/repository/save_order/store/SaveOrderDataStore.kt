package com.android.bodegas.data.repository.save_order.store

import com.android.bodegas.data.repository.save_order.model.SaveOrderEntity
import com.android.bodegas.domain.util.Resource

interface SaveOrderDataStore {

    suspend fun getOrder(): Resource<List<SaveOrderEntity>>

    suspend fun getOrderById(orderId:String): Resource<SaveOrderEntity>

    suspend fun saveOrder(saveOrderEntity: SaveOrderEntity)

    suspend fun updateOrder(saveOrderEntity: SaveOrderEntity)

    suspend fun deleteOrder()

    suspend fun deleteOrderByID(orderId: String)

}