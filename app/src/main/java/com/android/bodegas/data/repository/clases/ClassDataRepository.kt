package com.android.bodegas.data.repository.clases

import com.android.bodegas.data.repository.clases.mapper.ClassMapper
import com.android.bodegas.data.repository.clases.model.ClassEntity
import com.android.bodegas.data.repository.clases.store.remote.ClassRemoteDataStorage
import com.android.bodegas.domain.clases.ClassRepository
import com.android.bodegas.domain.util.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext
import com.android.bodegas.domain.clases.Class
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.coroutineScope

class ClassDataRepository(
    private val classRemoteDataStore: ClassRemoteDataStorage,
    private val classMapper: ClassMapper
) : ClassRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getClassesBySupplierId(
        establishmentId: Int,
        page: String,
        size: String,
        sortBy: String
    ): Resource<List<Class>> {
        return coroutineScope {
            val classList =
                classRemoteDataStore.getClassesBySupplierId(
                    establishmentId,
                    page,
                    size,
                    sortBy
                )
            var resource: Resource<List<Class>> =
                Resource(classList.status, mutableListOf(), classList.message)

            if (classList.status == Status.SUCCESS) {
                val mutableListclasses = mapClassList(classList.data)
                resource = Resource(Status.SUCCESS, mutableListclasses, classList.message)
            } else {
                if (classList.status == Status.ERROR) {
                    resource = Resource(classList.status, mutableListOf(), classList.message)
                }
            }

            return@coroutineScope resource
        }
    }

    private fun mapClassList(classEntityList: List<ClassEntity>): List<Class> {
        val classList = mutableListOf<Class>()
        classEntityList.forEach {
            classList.add(classMapper.mapFromEntity(it))
        }
        return classList
    }
}