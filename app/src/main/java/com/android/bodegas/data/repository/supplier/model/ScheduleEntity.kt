package com.android.bodegas.data.repository.supplier.model

data class ScheduleEntity(
    val range: String,
    val startTime: String ? = "",
    val endTime: String? = ""
)