package com.android.bodegas.data.network.products

import com.android.bodegas.data.network.products.model.ProductsListBody
import retrofit2.Response
import retrofit2.http.*

interface ProductsServices {

    @GET("/establishments/{establishment-id}/subsubcategory/{subsubcategory-id}/products")
    suspend fun getProducts(
        @Path("establishment-id") establishmentId: Int,
        @Path("subsubcategory-id") subsubcategoryId: Int
    ): Response<ProductsListBody>

    @GET("/establishments/{establishment-id}/products")
    suspend fun searchProducts(
        @Path("establishment-id") establishmentId: Int,
        @Query("text") searchText: String,
        @Query("sortBy") sortBy: String,
        @Query("page") page: Int,
        @Query("size") size: Int
    ): Response<ProductsListBody>
}

