package com.android.bodegas.data.network.sub_sub_categoty

import com.android.bodegas.data.network.sub_sub_categoty.model.SSubCategoryListBody
import retrofit2.Response
import retrofit2.http.*

interface SSubCategoryServices {

    @GET("/establishments/{establishment-id}/subcategory/{subcategory-id}/subsubcategories-or-products")
    suspend fun getSSubCategory(
        @Path("establishment-id") establishmentId: Int,
        @Path("subcategory-id") subCategoryId: Int
    ): Response<SSubCategoryListBody>
}