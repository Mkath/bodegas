package com.android.bodegas.data.repository.supplier.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.supplier.model.ScheduleEntity
import com.android.bodegas.data.repository.supplier.model.SupplierEntity
import com.android.bodegas.domain.supplier.Schedule
import com.android.bodegas.domain.supplier.Supplier


class ScheduleMapper : Mapper<ScheduleEntity, Schedule> {

    override fun mapFromEntity(type: ScheduleEntity): Schedule {
        return Schedule(
            type.range,
            type.startTime,
            type.endTime
        )
    }

    override fun mapToEntity(type: Schedule): ScheduleEntity {
        return ScheduleEntity(
            type.range,
            type.startTime,
            type.endTime
        )
    }

}