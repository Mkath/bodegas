package com.android.bodegas.data.network.user.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.user.model.UserLoginItem
import com.android.bodegas.data.repository.user.model.LoginEntity

class UserLoginEntityMapper : Mapper<UserLoginItem, LoginEntity> {

    override fun mapFromRemote(type: UserLoginItem): LoginEntity {
        return LoginEntity(type.token, type.tokenType)
    }
}