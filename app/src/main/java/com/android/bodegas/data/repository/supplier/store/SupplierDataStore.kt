package com.android.bodegas.data.repository.supplier.store

import com.android.bodegas.data.repository.supplier.model.SupplierEntity
import com.android.bodegas.domain.util.Resource

interface SupplierDataStore {
    suspend fun getSupplierByType(
        type: Int,
        latitude: String,
        longitud: String,
        radius: Int
    ): Resource<List<SupplierEntity>>
}