package com.android.bodegas.data.network.products.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ProductsDetailsResponse(
    @SerializedName("code")
    @Expose
    val code: String,
    @SerializedName("description")
    @Expose
    val description: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("unitMeasure")
    @Expose
    val unitMeasure: String,
    @SerializedName("pathImage")
    @Expose
    val pathImage: String ? = ""
)