package com.android.bodegas.data.repository.save_order.store.db

import com.android.bodegas.data.repository.save_order.model.SaveOrderEntity
import com.android.bodegas.data.repository.save_order.store.SaveOrderDataStore
import com.android.bodegas.domain.util.Resource

class SaveOrderDbDataStore constructor(private val saveOrderDb: SaveOrderDb) :
    SaveOrderDataStore {

    override suspend fun getOrder(): Resource<List<SaveOrderEntity>> {
        return saveOrderDb.getOrder()
    }

    override suspend fun getOrderById(orderId: String): Resource<SaveOrderEntity> {
        return saveOrderDb.getOrderById(orderId)
    }

    override suspend fun saveOrder(saveOrderEntity: SaveOrderEntity) {
        return saveOrderDb.saveOrder(saveOrderEntity)
    }

    override suspend fun updateOrder(saveOrderEntity: SaveOrderEntity) {
        return saveOrderDb.updateOrder(saveOrderEntity)
    }

    override suspend fun deleteOrder() {
        return saveOrderDb.deleteOrder()
    }

    override suspend fun deleteOrderByID(orderId: String) {
        return saveOrderDb.deleteOrderById(orderId)
    }

}