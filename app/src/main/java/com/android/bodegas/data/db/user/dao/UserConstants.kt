package com.android.bodegas.data.db.user.dao

object UserConstants {

    const val TABLE_NAME = "user"

    const val QUERY_USER = "SELECT * FROM" + " " + TABLE_NAME

    const val DELETE_USER = "DELETE FROM" + " " + TABLE_NAME
}