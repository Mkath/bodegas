package com.android.bodegas.data.repository.customerOrders.model

class CustomerOrdersEntity(
    val orderId: String,
    val deliveryType: String,
    val orderDate:String,
    val shippingDateFrom: String,
    val shippingDateUntil: String,
    val status: String,
    val establishment:EstablishmentOrdersEntity,
    val total: String,
    val orderDetails: List<OrderDetailItemEntity>,
    val updateDate: String,
    val deliveryCharge: String,
    val customerAmount: String,
    val paymentMethodCustomerMessage: String,
    val paymentMethodDescription: String,
    val paymentMethodEstablishmentMessage: String,
    val paymentMethodPaymentMethodId: Int,
    val paymentMethodRequestAmount: Boolean
)