package com.android.bodegas.data.repository.customerOrders.model

import com.android.bodegas.data.repository.supplier.model.PaymentMethodEntity
import com.android.bodegas.data.repository.supplier.model.ScheduleEntity

class EstablishmentOrdersEntity(
    val establishmentId: Int,
    val establishmentEmail: String,
    val establishmentName: String,
    val delivery: String,
    val shippingSchedule: List<ScheduleEntity>,
    val operationSchedule: List<ScheduleEntity>,
    val paymentMethods: List<PaymentMethodEntity>,
    val deliveryCharge: Boolean,
    val establishmentAddress: String
)