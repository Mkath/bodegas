package com.android.bodegas.data.network.user.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.user.model.UserLoginTokenResponse
import com.android.bodegas.data.repository.user.model.UserLoginTokenEntity

class UserLoginTokenEntityMapper: Mapper<UserLoginTokenResponse, UserLoginTokenEntity> {

    override fun mapFromRemote(type: UserLoginTokenResponse): UserLoginTokenEntity {
        return UserLoginTokenEntity(
            type.id_entity,
            type.token
        )
    }
}