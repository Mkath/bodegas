package com.android.bodegas.data.repository.duplicateOrder.model

data class DuplicateOrderEntity (

    val establishmentId: Int,
    val flagAllAvailableProducts: Boolean,
    val orderDetailRequest: List<DuplicateOrderItemBodyEntity>

)