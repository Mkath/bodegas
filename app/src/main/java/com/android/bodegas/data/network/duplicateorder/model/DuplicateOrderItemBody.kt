package com.android.bodegas.data.network.duplicateorder.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DuplicateOrderItemBody(

    @SerializedName("description")
    @Expose
    val description: String,
    @SerializedName("price")
    @Expose
    val price: Double,
    @SerializedName("quantity")
    @Expose
    val quantity: Double,
    @SerializedName("storeProductId")
    @Expose
    val storeProductId: Int,
    @SerializedName("subTotal")
    @Expose
    val subTotal: Double

)