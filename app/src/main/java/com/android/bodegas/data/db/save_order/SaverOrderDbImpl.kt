package com.android.bodegas.data.db.save_order

import com.android.bodegas.data.db.AppDatabase
import com.android.bodegas.data.db.save_order.mapper.SaveOrderDbMapper
import com.android.bodegas.data.repository.save_order.model.SaveOrderEntity
import com.android.bodegas.data.repository.save_order.store.db.SaveOrderDb
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class SaverOrderDbImpl constructor(
    private val database: AppDatabase,
    private val saveOrderDbMapper: SaveOrderDbMapper
) : SaveOrderDb, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getOrder(): Resource<List<SaveOrderEntity>> {
        return coroutineScope {
            val saveOrderDbList = database.saveOrderDao().getOrder()
            val orderList = mutableListOf<SaveOrderEntity>()

            if (saveOrderDbList.isNotEmpty()) {
                saveOrderDbList.forEach {
                    orderList.add(saveOrderDbMapper.mapFromDb(it))
                }
                return@coroutineScope Resource(
                    Status.SUCCESS,
                    orderList,
                    ""
                )
            } else {
                return@coroutineScope Resource(
                    Status.SUCCESS,
                    orderList,
                    ""
                )
            }

        }
    }

    override suspend fun getOrderById(orderId: String): Resource<SaveOrderEntity> {
        return coroutineScope {
            try {
                val saveOrderDb = database.saveOrderDao().getOrderById(orderId)
                val saveOrderEntity = saveOrderDbMapper.mapFromDb(saveOrderDb)
                return@coroutineScope Resource(
                    Status.SUCCESS,
                    saveOrderEntity,
                    ""
                )
            } catch (e: Exception) {
                e.printStackTrace()
                return@coroutineScope Resource(
                    Status.ERROR,
                    SaveOrderEntity(
                        "",
                        0,
                        0.0,
                        0.0,
                        0.0,
                        "",
                        "",
                        "",
                        ""
                    ),
                    ""
                )
            }
        }

    }


    override suspend fun saveOrder(saveOrderEntity: SaveOrderEntity) {
        coroutineScope {
            database.saveOrderDao().insertOrder(saveOrderDbMapper.mapToDb(saveOrderEntity))
        }
    }

    override suspend fun updateOrder(saveOrderEntity: SaveOrderEntity) {
        coroutineScope {
            database.saveOrderDao().updateOrder(saveOrderDbMapper.mapToDb(saveOrderEntity))

        }
    }

    override suspend fun deleteOrder() {
        coroutineScope {
            database.saveOrderDao().deleteOrder()
        }
    }

    override suspend fun deleteOrderById(orderId: String) {
        coroutineScope {
            database.saveOrderDao().deleteOrderById(orderId)
        }
    }
}