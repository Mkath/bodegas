package com.android.bodegas.data.network.generate_order

import com.android.bodegas.data.network.generate_order.model.GenerateOrderBody
import com.android.bodegas.data.network.generate_order.model.GenerateOrderResponse
import retrofit2.Response
import retrofit2.http.*

interface GenerateOrderServices {

    @POST("/customers/{customer-id}/order")
    suspend fun generateOrder(
        @Path("customer-id") customerId: Int,
        @Body generateOrderBody: GenerateOrderBody
    ): Response<GenerateOrderResponse>

}
