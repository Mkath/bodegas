package com.android.bodegas

import android.app.Application

import com.android.bodegas.di.*
import com.android.bodegas.di.appModule
import com.android.bodegas.di.supplierModule
import com.android.bodegas.di.userModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class BodegasApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@BodegasApplication)
            modules(
                listOf(
                    appModule,
                    userModule,
                    supplierModule,
                    classModule,
                    categoryModule,
                    subCategoryModule,
                    subsubcategoryModule,
                    productsModule,
                    duplicateOrderModule,
                    saveOrderModule,
                    generateOrderModule,
                    departmentModule,
                    provinceModule,
                    districtModule,
                    customerOrders,
                    allPaymentMethodsModule

                )
            )
        }
    }
}

