package com.android.bodegas.presentation.favorites

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.supplier.SupplierViewModel
import com.android.bodegas.presentation.supplier.adapter.StoreAdapter
import com.android.bodegas.presentation.supplier.adapter.SupplierAdapter
import com.android.bodegas.presentation.supplier.adapter.SupplierViewHolder
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_market_list.*
import kotlinx.android.synthetic.main.fragment_suppliers.*
import kotlinx.android.synthetic.main.fragment_suppliers.rvStores
import org.koin.androidx.viewmodel.ext.android.viewModel

const val TYPE_STORE = "typeStore"

class FavoritesFragment : Fragment(), SupplierViewHolder.ViewHolderListener {

    companion object {
        const val TAG = "FavoritesFragment"

        @JvmStatic
        fun newInstance() =
            FavoritesFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

    private val storesViewModel: SupplierViewModel by viewModel()
    private var storesViewList: MutableList<SupplierView> = mutableListOf()
    private var listener: OnFragmentInteractionListener? = null
    private var adapter = StoreAdapter(this)


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onResume() {
        super.onResume()
        observeListSuppliers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setSwipeRefreshLayout()
    }

    private fun observeListSuppliers() {
        storesViewModel.listSuppliers.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener?.hideLoading()
                    swipe.isRefreshing = false
                    storesViewList = it.data.toMutableList()
                    setRecyclerView()
                }
                Status.ERROR -> {
                    listener?.hideLoading()
                    Snackbar.make(
                        supplierView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                    listener?.showLoading()
                }
            }
        })
        storesViewModel.getSuppliersByType(1, "1", "1", 1)
    }

    private fun setRecyclerView() {
        val linearLayoutManagerStores = LinearLayoutManager(context)
        rvStores.layoutManager = linearLayoutManagerStores
        rvStores.adapter = adapter
        adapter.setStoreList(storesViewList)
    }

    override fun onClick(position: Int, establishmentType:String) {
        val storeView = storesViewList[position]
        listener?.replaceClassFragment(storeView)
        PreferencesManager.getInstance().setEstablishmentSelected(storeView)
    }

    private fun setSwipeRefreshLayout() {
        swipe.setOnRefreshListener {
            storesViewModel.getSuppliersByType(1, "1", "1", 1)
        }
    }

    interface OnFragmentInteractionListener {
        fun replaceClassFragment(establishment: SupplierView)
        fun showLoading()
        fun hideLoading()
    }
}