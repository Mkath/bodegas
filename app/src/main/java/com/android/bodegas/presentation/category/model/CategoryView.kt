package com.android.bodegas.presentation.category.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CategoryView(
    val id: Int,
    val description: String,
    val name: String,
    val pathImage: String ? = ""
) : Parcelable