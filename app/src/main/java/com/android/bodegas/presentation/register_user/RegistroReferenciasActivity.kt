package com.android.bodegas.presentation.register_user

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.BaseActivity
import com.android.bodegas.presentation.login.LoginViewModel
import com.android.bodegas.presentation.register_user.model.DepartmentView
import com.android.bodegas.presentation.register_user.model.DistrictView
import com.android.bodegas.presentation.register_user.model.ProvinceView
import com.android.bodegas.presentation.register_user.model.RegisterUserBodyView
import com.android.bodegas.presentation.main.MainActivity
import com.android.bodegas.presentation.utils.animation.LoadingAnimationImageView
import com.android.bodegas.presentation.utils.animation.LoadingDots
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_registro_user_referencias.*
import kotlinx.android.synthetic.main.layout_loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel

const val PAIS = "PAIS"
const val DEPARTAMENTO = "DEPARTAMENTO"
const val PROVINCIA = "PROVINCIA"
const val DISTRITO = "DISTRITO"
const val URBANIZACION = "URBANIZACION"
const val DIRECCION = "DIRECCION"
const val LATITUD = "LATITUD"
const val LONGITUD = "LONGITUD"


class RegistroReferenciasActivity : BaseActivity() {
    private var pais: String = "PE"

    private val loginViewModel: LoginViewModel by viewModel()
    private val registerViewModel: RegistroViewModel by viewModel()

    private var departmentViewList: MutableList<DepartmentView> = mutableListOf()
    private var stringDeparmentList = ArrayList<CharSequence>()

    private var provinceViewList: MutableList<ProvinceView> = mutableListOf()
    private var stringProvinceList = ArrayList<CharSequence>()

    private var districtViewList: MutableList<DistrictView> = mutableListOf()
    private var stringDistrictList = ArrayList<CharSequence>()

    var selectedDepartmentId: String = ""
    var selectedProvinceId: String = ""

    private var loadingAnimationImageView: LoadingAnimationImageView? = null
    private var loadingDots: LoadingDots? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro_user_referencias)
        botonSetDepartamento()
        botonSetProvincia()
        botonSetDistrito()
        botonRegistrarUsuarioFinal()
        obtenerDepartamentos()
        observerRegister()
        observeViewModelGetData()
    }

    private fun observerRegister() {
        registerViewModel.registerNewUser.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    loginViewModel.getDataUser(
                        PreferencesManager.getInstance().getCustomerLoginTokenSelected().idEntity.toInt()
                    )
                }
                Status.ERROR -> {
                    Snackbar.make(
                        registerView,
                        it.message!!,
                        Snackbar.LENGTH_LONG
                    ).show()
                    hideLoading()
                }
                Status.LOADING -> {
                    showLoading()
                }
            }
        })
    }

    private fun observeViewModelGetData() {
        loginViewModel.loginCode.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    registerSuccessfully()
                }
                Status.ERROR -> {
                    layoutProgressBar.visibility = View.GONE
                    if (it.message == "500") {
                        Snackbar.make(
                            registerView,
                            it.message.let { getString(R.string.login_not_connection_message)},
                            Snackbar.LENGTH_LONG
                        ).show()
                    } else {
                        Snackbar.make(
                            registerView,
                            it.message.let { getString(R.string.login_not_connection_message)},
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                }
                Status.LOADING -> {
                    showLoading()
                }
            }
        })
    }

    private fun obtenerDepartamentos() {
        registerViewModel.departments.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    departmentViewList = it.data.toMutableList()
                    getDepartmentStringsArray()
                    hideLoading()
                }
                Status.ERROR -> {
                    hideLoading()
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar los departamentos",
                        Toast.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                    showLoading()
                }
            }
        })
        registerViewModel.getDepartments()
    }

    private fun getDepartmentStringsArray() {
        stringDeparmentList.clear()
        departmentViewList.forEach {
            stringDeparmentList.add(it.name)
        }
    }

    private fun showDialogDepartments() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> =
            stringDeparmentList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        var selectedValue: CharSequence = ""
        builder.setTitle("Elige un departamento.")
        builder.setSingleChoiceItems(array, -1)
        { _, which ->
            selectedValue = array[which]
            departamentoinput.setText(selectedValue)
            dialog.dismiss()
            boton_set_provincia.isEnabled = true
            provinciainput.setText("Seleccionar")
            distritoinput.setText("Seleccionar")

            for (i in departmentViewList) {
                if (i.name == selectedValue) {
                    selectedDepartmentId = i.id
                }
            }

            obtenerProvincias()
        }
        dialog = builder.create()
        dialog.show()

    }


    private fun botonSetDepartamento() {
        boton_set_departamento.setOnClickListener {
            showDialogDepartments()
        }
    }

    private fun botonSetProvincia() {
        boton_set_provincia.setOnClickListener {
            showDialogProvincia()
        }
    }

    private fun obtenerProvincias() {
        registerViewModel.provinces.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    provinceViewList = it.data.toMutableList()
                    getProvinceStringsArray()
                    hideLoading()
                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar las provincias",
                        Toast.LENGTH_LONG
                    ).show()
                    hideLoading()
                }
                Status.LOADING -> {
                    showLoading()
                }
            }
        })
        registerViewModel.getProvinces()
    }

    private fun getProvinceStringsArray() {
        stringProvinceList.clear()
        provinceViewList.forEach {
            if (it.department_id == selectedDepartmentId) {
                stringProvinceList.add(it.name)
            }
        }
    }

    private fun showDialogProvincia() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> =
            stringProvinceList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Elige una provincia.")
        var selectedValue: CharSequence = ""
        builder.setSingleChoiceItems(array, -1) { _, which ->
            selectedValue = array[which]
            provinciainput.setText(selectedValue)
            dialog.dismiss()

            boton_set_distrito.isEnabled = true
            distritoinput.setText("Seleccionar")

            for (i in provinceViewList) {
                if (i.name == selectedValue) {
                    selectedProvinceId = i.id
                }
            }
            obtenerDistritos()
        }
        dialog = builder.create()
        dialog.show()
    }

    private fun botonSetDistrito() {
        boton_set_distrito.setOnClickListener {
            showDialogDistrito()
        }
    }

    private fun obtenerDistritos() {
        registerViewModel.districts.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    districtViewList = it.data.toMutableList()
                    getDistrictsStringsArray()
                    hideLoading()

                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar los distritos",
                        Toast.LENGTH_LONG
                    ).show()
                    hideLoading()
                }
                Status.LOADING -> {
                    showLoading()
                }
            }
        })
        registerViewModel.getDistricts()
    }

    private fun getDistrictsStringsArray() {
        stringDistrictList.clear()
        districtViewList.forEach {
            if (it.department_id == selectedDepartmentId && it.province_id == selectedProvinceId) {
                stringDistrictList.add(it.name)
            }
        }
    }

    private fun botonRegistrarUsuarioFinal() {
        boton_finalizar_registro_user.setOnClickListener {
            registrarUsuarioFinal()
        }
    }

    private fun showDialogDistrito() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> =
            stringDistrictList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Elige un distrito.")
        var selectedValue: CharSequence = ""
        builder.setSingleChoiceItems(array, -1) { _, which ->
            selectedValue = array[which]
            distritoinput.setText(selectedValue)
            dialog.dismiss()
        }
        dialog = builder.create()
        dialog.show()
    }

    private fun registrarUsuarioFinal() {
        if (validarAlerts(departamentoinput, departamentobox) &&
            validarAlerts(provinciainput, provinciabox) &&
            validarAlerts(distritoinput, distritobox) &&
            validarExtensionCampo(inputreferencia, boxreferencia, 5)
        ) {
            saveDatafromUser()
           // registerSuccessfully()
        }
    }

    private fun registerSuccessfully() {
        val bundle = Bundle()
        nextActivity(bundle, MainActivity::class.java, true)
    }

    private fun validarAlerts(
        campo_input: TextInputEditText,
        title_input: TextInputLayout
    ): Boolean {
        val campo = campo_input.text.toString()
        title_input.error = null
        if (campo == "Seleccionar") {
            title_input.error = "Selecciona un item de la lista."
            return false
        }
        return true
    }

    private fun validarExtensionCampo(
        campo_input: TextInputEditText,
        title_input: TextInputLayout,
        minima_longitud: Int
    ): Boolean {
        val campo = campo_input.text.toString()
        title_input.error = null
        if ((campo.isEmpty()) || campo.length < minima_longitud) {
            title_input.error = "Introduce una referencia más extensa."
            return false
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        comeBack()
    }

    private fun comeBack() {
        val bundle = Bundle()
        nextActivity(bundle, SearchPlacesActivity::class.java, true)
    }

    fun saveDatafromUser() {
        PreferencesManager.getInstance().saveData(PAIS, pais)
        PreferencesManager.getInstance().saveData(DEPARTAMENTO, departamentoinput.text.toString())
        PreferencesManager.getInstance().saveData(PROVINCIA, provinciainput.text.toString())
        PreferencesManager.getInstance().saveData(DISTRITO, distritoinput.text.toString())
        PreferencesManager.getInstance().saveData(URBANIZACION, inputreferencia.text.toString())

        val address: String = PreferencesManager.getInstance().getData(DIRECCION)
        val businessName: String = ""
        val cardNumber: String = "1234567890123456"
        val cardOperator = "Visa"
        val country: String = pais
        val creationUser = PreferencesManager.getInstance().getData(NUMERO_DOC)
        val departament: String = PreferencesManager.getInstance().getData(DEPARTAMENTO)
        val district: String = PreferencesManager.getInstance().getData(DISTRITO)
        val dni: String = PreferencesManager.getInstance().getData(NUMERO_DOC)
        val email: String = PreferencesManager.getInstance().getData(CORREO)
        val lastNameMaternal: String = PreferencesManager.getInstance().getData(APE_MATERNO)
        val lastNamePaternal: String = PreferencesManager.getInstance().getData(APE_PATERNO)
        val latitude: Double = PreferencesManager.getInstance().getData(LATITUD).toDouble()
        val longitude: Double = PreferencesManager.getInstance().getData(LONGITUD).toDouble()
        val name: String = PreferencesManager.getInstance().getData(NOMBRE)
        val owner: String = ""
        val password: String = PreferencesManager.getInstance().getData(CONTRASENIA)
        val phoneNumber: String = PreferencesManager.getInstance().getData(CELULAR)
        val province: String = PreferencesManager.getInstance().getData(PROVINCIA)
        val ruc = "12345678765"
        val urbanization: String = PreferencesManager.getInstance().getData(URBANIZACION)

        val sendUserBodyView = RegisterUserBodyView(address, businessName, cardNumber, cardOperator, country,creationUser, departament, district, dni,
        email, lastNameMaternal, lastNamePaternal, latitude, longitude,name, password, phoneNumber, province, ruc, urbanization)
        registerViewModel.registerUser(sendUserBodyView)
    }

    override fun onResume() {
        super.onResume()
        loadDataFromLocalStorage()
    }

    private fun loadDataFromLocalStorage() {

    }

    private fun showLoading() {
        layoutLoading.visibility = View.VISIBLE
        animateLoadingText()
        animateLoadingImage()
    }

    private fun hideLoading() {
        layoutLoading.visibility = View.GONE

    }

    private fun animateLoadingText() {
        if (loadingDots == null) {
            loadingDots =
                LoadingDots(dots)
            loadingDots?.animateLoadingText()
        }
    }

    private fun animateLoadingImage() {
        if (loadingAnimationImageView == null) {
            loadingAnimationImageView =
                LoadingAnimationImageView(
                    ivShrimp
                )
            loadingAnimationImageView?.startAnimation()
        }
    }

}
