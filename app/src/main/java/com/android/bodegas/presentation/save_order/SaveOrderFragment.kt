package com.android.bodegas.presentation.save_order

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.save_order.adapter.SaveOrderAdapter
import com.android.bodegas.presentation.save_order.adapter.SaveOrderViewHolder
import com.android.bodegas.presentation.save_order.adapter.SwipeToDeleteCallback
import com.android.bodegas.presentation.save_order.model.SaveOrderView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_save_order.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

const val PRODUCTS_LIST = "productsList"

class SaveOrderFragment : Fragment(), SaveOrderViewHolder.ViewHolderListener {

    companion object {
        const val TAG = "SaveOrderFragment"

        @JvmStatic
        fun newInstance() =
            SaveOrderFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

    private val saveOrderViewModel: SaveOrderViewModel by sharedViewModel()
    private var listener: OnFragmentInteractionListener? = null
    private var adapter = SaveOrderAdapter(this)
    private var orderValueList = ArrayList<SaveOrderView>()
    private var totalValue = ""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_save_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClickListener()
    }

    override fun onResume() {
        super.onResume()
        observeListOrder()
        observeValueTotal()
    }


    private fun observeListOrder() {
        saveOrderViewModel.listProducts.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener?.hideLoading()
                    if (it.data.isNotEmpty()) {
                        tvStoreName.text = PreferencesManager.getInstance().getEstablishmentSelected().storeName
                        containerEmptyState.visibility = View.GONE
                        rvOrder.visibility = View.VISIBLE
                        tvStoreName.visibility = View.VISIBLE
                        containerTotal.visibility = View.VISIBLE
                        orderValueList = it.data.toCollection(ArrayList())
                        setRecyclerView()
                    } else {
                        containerEmptyState.visibility = View.VISIBLE
                        rvOrder.visibility = View.GONE
                        tvStoreName.visibility = View.GONE
                        containerTotal.visibility = View.GONE
                    }
                }

                Status.ERROR -> {
                    listener?.hideLoading()
                    Snackbar.make(
                        sendOrderView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                    listener?.showLoading()
                }
            }
        })
        saveOrderViewModel.getOrdersByUser()
    }

    private fun observeValueTotal() {
        saveOrderViewModel.totalValue.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    tvTotalCompra.visibility = View.VISIBLE
                    tvTotalCompra.text = String.format("%s %.2f","s/",  it.data)
                    totalValue = it.data.toString()
                  //  saveOrderViewModel.getOrdersByUser()
                }

                Status.ERROR -> {
                    tvTotalCompra.visibility = View.GONE
                }

                Status.LOADING -> {
                    tvTotalCompra.visibility = View.GONE
                }
            }
        })
        saveOrderViewModel.getTotalValue()
    }



    private fun setOnClickListener() {
        btnSendOrder.setOnClickListener {
            listener!!.replacePurchaseDetailsFragment()
        }
    }

    private fun setRecyclerView() {
        saveOrderViewModel.getTotalValue()
        val linearLayoutManager = LinearLayoutManager(requireContext())
        rvOrder.layoutManager = linearLayoutManager
        rvOrder.adapter = adapter
        rvOrder.isNestedScrollingEnabled = true

        val swipeHandler = object : SwipeToDeleteCallback(requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val saveOrderView = adapter.ordersList[position]
                deleteSaveOrder(saveOrderView)
                adapter.removeAt(position)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(rvOrder)

        adapter.setProductsList(orderValueList)
    }

    fun deleteSaveOrder(saveOrderView: SaveOrderView){
        saveOrderViewModel.deleteOrderById(saveOrderView.id)
    }

    override fun onClickProducts(position: Int) {
        //   val productView = productList[position]
        // listener?.onProductClicked(productView.name)
    }

    override fun onClickChangeProduct(
        position: Int,
        quantity: Double
    ) {
        saveOrderViewModel.updateProductsForOrder(orderValueList[position], quantity)
    }


    fun refreshList() {
        saveOrderViewModel.getOrdersByUser()
    }


    interface OnFragmentInteractionListener {
        fun showLoading()
        fun hideLoading()
        fun replacePurchaseDetailsFragment()
    }
}