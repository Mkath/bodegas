package com.android.bodegas.presentation.login.model

import com.android.bodegas.domain.user.login.model.UserLogin
import com.android.bodegas.presentation.Mapper

open class UserLoginViewMapper :
    Mapper<UserLoginView, UserLogin> {

    override fun mapToView(type: UserLogin): UserLoginView {
        return UserLoginView(
            type.token,
            type.userName
        )
    }
}