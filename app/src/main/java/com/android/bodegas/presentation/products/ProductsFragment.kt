package com.android.bodegas.presentation.products

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegas.R
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.login.LoginActivity
import com.android.bodegas.presentation.products.adapter.ProductsAdapter
import com.android.bodegas.presentation.products.adapter.ProductsViewHolder
import com.android.bodegas.presentation.products.model.ProductsView
import com.android.bodegas.presentation.save_order.SaveOrderViewModel
import com.android.bodegas.presentation.save_order.model.SaveOrderView
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_products.*
import kotlinx.android.synthetic.main.fragment_products.tvTotalCompra
import kotlinx.android.synthetic.main.fragment_save_order.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


const val PRODUCTS_LIST = "productsList"
const val ESTABLISHMENT = "establishment"

class ProductsFragment : Fragment(), ProductsViewHolder.ViewHolderListener {

    companion object {
        const val TAG = "ProductsFragment"

        @JvmStatic
        fun newInstance(establishment: SupplierView, productList: ArrayList<ProductsView>) =
            ProductsFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(PRODUCTS_LIST, productList)
                    putParcelable(ESTABLISHMENT, establishment)
                }
            }
    }

    private val searchProductsViewModel: SearchProductsViewModel by viewModel()
    private val sendOrdersViewModel: SaveOrderViewModel by sharedViewModel()
    private lateinit var establishment: SupplierView
    private var orderValueList = ArrayList<SaveOrderView>()
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var adapter: ProductsAdapter
    private var productList = ArrayList<ProductsView>()
    private var variableList = ArrayList<ProductsView>()
    private var isFirst = true
    private var productAddPosition = 0
    private var scrollPosition : Parcelable? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            productList = it.getParcelableArrayList(PRODUCTS_LIST)!!
            establishment = it.getParcelable(ESTABLISHMENT)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_products, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = ProductsAdapter(establishment, this)
        searchProductsViewModel.clearListProducts()
        listener!!.hideLoading()

    }

    override fun onResume() {
        super.onResume()
        observeListOrder()
        observeValueTotal()
    }

    private fun setRecyclerView() {
        val linearLayoutManagerStores = GridLayoutManager(context, 2)
        rvProducts.layoutManager = linearLayoutManagerStores
        rvProducts.adapter = adapter
        adapter.setProductsList(productList)
        if(scrollPosition != null){
            rvProducts.layoutManager!!.onRestoreInstanceState(scrollPosition);
        }
    }

    private fun validateIfPreviousSelected() {
        resetListProducts()
        productList.forEach {
            for (i in  0 until orderValueList.size) {
                if (it.storeProductId == orderValueList[i].storeProductId) {
                    it.quantitySelected = orderValueList[i].quantity
                }
            }
        }
        setRecyclerView()
    }

    private fun resetListProducts() {
        productList.forEach {
            it.quantitySelected = 0.0
        }
        setRecyclerView()
    }

    override fun onClickProducts(position: Int) {
        val product = productList[position]
        if (product.stock.isNotEmpty()) {
            Snackbar.make(
                productView,
                "Producto agotado",
                Snackbar.LENGTH_LONG
            ).show()
        }
        // listener?.onProductClicked(productView.name)
    }

    override fun onClickChangeProduct(
        position: Int,
        quantity: Double
    ) {
        if (quantity == 0.0) {
            getIsInvitedDialog()
        } else {
            productAddPosition = position
            sendOrdersViewModel.addProductsForOrder(productList[position], quantity)
            scrollPosition = rvProducts.layoutManager!!.onSaveInstanceState()

        }
    }

    private fun getIsInvitedDialog() {
        val dialog = Dialog(context!!)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.layout_dialog_is_invited)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val btnCancel = dialog.findViewById(R.id.btnCancel) as Button
        val btnLogin = dialog.findViewById(R.id.btnLogin) as Button

        btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        btnLogin.setOnClickListener {
            val intent = Intent(context, LoginActivity::class.java)
            startActivity(intent)
            activity!!.finish()
        }
        dialog.show()
    }

    private fun observeValueTotal() {
        sendOrdersViewModel.totalValue.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    tvTotalCompra.visibility = View.VISIBLE
                    tvTotalCompra.text = String.format("%s %.2f", "s/", it.data)
                }

                Status.ERROR -> {
                    tvTotalCompra.visibility = View.GONE
                }

                Status.LOADING -> {
                    tvTotalCompra.visibility = View.GONE
                }
            }
        })
        sendOrdersViewModel.getTotalValue()
    }


    private fun observeListOrder() {
        sendOrdersViewModel.listProducts.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (isOpenKeyboard()){
                        hideKeyboard()
                    }
                    listener?.hideLoading()
                    orderValueList = it.data.toCollection(ArrayList())
                    if (orderValueList.isNotEmpty()) {
                        validateIfPreviousSelected()
                    } else {
                        resetListProducts()
                    }
                }

                Status.ERROR -> {
                    if (isOpenKeyboard()){
                        hideKeyboard()
                    }
                    listener?.hideLoading()
                    Snackbar.make(
                        sendOrderView,
                        getString(R.string.producto_no_encontrado),
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                    listener?.showLoading()
                }
            }
        })
        sendOrdersViewModel.getOrdersByUser()
    }

    fun refreshList(list: List<ProductsView>) {
        productList = list.toCollection(ArrayList())
        sendOrdersViewModel.getOrdersByUser()
    }

    interface OnFragmentInteractionListener {
        fun showLoading()
        fun hideLoading()
    }

    fun hideKeyboard() {
        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0);
    }


    fun isOpenKeyboard(): Boolean {
        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return imm.isAcceptingText
    }
}