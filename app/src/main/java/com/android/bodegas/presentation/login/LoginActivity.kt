package com.android.bodegas.presentation.login

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.BaseActivity
import com.android.bodegas.presentation.register_user.RegistroDatosBasicosActivity
import com.android.bodegas.presentation.main.MainActivity
import com.android.bodegas.presentation.utils.animation.LoadingDots
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.layout_loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class LoginActivity : BaseActivity() {

    private val loginViewModel: LoginViewModel by viewModel()
    private lateinit var validationUtil: InputValidationUtil
    private var loadingDots: LoadingDots? = null
    private val GUEST_USER_EMAIL = "rantyk.guest@gmail.com"
    private val GUEST_USER_PASSWORD = "guest123456"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        observeViewModel()
        observeViewModelGetData()
        setButtonClickListener()
        recoveryPassword()
        goToRegisterActivity()
        validationUtil = InputValidationUtil(
            etUser,
            textInputLayoutUser,
            etPass,
            textInputLayoutPassword,
            applicationContext,
            btnLogin,
            imageView
        )

       // val typeface: Typeface? = ResourcesCompat.getFont(this, R.font.averta_regular)
     //   textInputLayoutPassword.typeface = typeface
        layoutProgressBar.visibility = View.GONE
    }

    private fun observeViewModel() {
        loginViewModel.loginCodeToken.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if(it.message!!.isEmpty()){
                        PreferencesManager.getInstance().isInvited(false)
                        Log.e("error", "obteniendo datos del usuario luego de login ")
                        loginViewModel.getDataUser(
                            PreferencesManager.getInstance().getCustomerLoginTokenSelected().idEntity.toInt()
                        )
                    }else{
                        layoutProgressBar.visibility = View.GONE
                        showErrorMessage(
                            it.message
                        )
                    }

                }
                Status.ERROR -> {
                    layoutProgressBar.visibility = View.GONE
                    if (it.message == "500") {
                        showErrorMessage(
                            it.message.let { getString(R.string.login_not_connection_message)})
                    } else {
                        showErrorMessage(
                            it.message.let { getString(R.string.login_error_message)})
                    }
                }
                Status.LOADING -> {
                    layoutProgressBar.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun observeViewModelGetData() {
        loginViewModel.loginCode.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    loginSuccessfully()
                }
                Status.ERROR -> {
                    layoutProgressBar.visibility = View.GONE
                    if (it.message == "500") {
                        showErrorMessage(
                            it.message.let { getString(R.string.login_not_connection_message)})
                    } else {
                        it.message?.let { it1 ->
                            showErrorMessage(
                                it1
                            )
                        }
                    }
                }
                Status.LOADING -> {
                    layoutProgressBar.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun animateLoadingText() {
        if (loadingDots == null) {
            loadingDots =
                LoadingDots(dots)
            loadingDots?.animateLoadingText()
        }
    }

    private fun setButtonClickListener() {
        btnLogin.setOnClickListener {
            layoutProgressBar.visibility = View.VISIBLE
            if (isOpenKeyboard()) {
                hideKeyboard()
                imageView.visibility = View.VISIBLE
            }
            if (validationUtil.getValidate()) {
                layoutProgressBar.visibility = View.VISIBLE
                animateLoadingText()
                loginViewModel.loginUserToken(
                    etUser.text.toString(),
                    etPass.text.toString()
                )
            }else{
                layoutProgressBar.visibility = View.GONE
            }
        }

        btnIngresarInvitado.setOnClickListener {
            PreferencesManager.getInstance().isInvited(true)
            PreferencesManager.getInstance().setUserToken("")
            loginViewModel.loginUserTokenAndGetData(GUEST_USER_EMAIL , GUEST_USER_PASSWORD)
            loginSuccessfully()
        }
    }

    private fun loginSuccessfully() {
        layoutProgressBar.visibility = View.GONE
        val bundle = Bundle()
        nextActivity(bundle, MainActivity::class.java, true)
    }

    private fun showErrorMessage(message: String) {
        Snackbar.make(
            mainView,
            message,
            Snackbar.LENGTH_LONG
        ).show()
    }

    private fun goToRegisterActivity(){
        btnRegistrarse.setOnClickListener{
            if(isOpenKeyboard()){
                hideKeyboard()
            }
            val bundle = Bundle()
            nextActivity(bundle, RegistroDatosBasicosActivity::class.java, true)
        }
    }

    private fun recoveryPassword(){
        btnGetPassword.setOnClickListener{
            if(isOpenKeyboard()){
                hideKeyboard()
            }
            nextActivity(null, RecoveryPasswordActivity::class.java, true)
        }
    }
}
