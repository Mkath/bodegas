package com.android.bodegas.presentation.supplier.mapper

import com.android.bodegas.domain.supplier.Schedule
import com.android.bodegas.domain.supplier.Supplier
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.supplier.model.ScheduleView
import com.android.bodegas.presentation.supplier.model.SupplierView


class ScheduleViewMapper : Mapper<ScheduleView, Schedule> {

    override fun mapToView(type: Schedule): ScheduleView {
        return ScheduleView(
            type.range,
            type.startTime,
            type.endTime
        )
    }

}