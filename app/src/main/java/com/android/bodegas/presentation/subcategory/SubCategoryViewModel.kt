package com.android.bodegas.presentation.subcategory

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegas.domain.sub_sub_category.GetSSubCategory
import com.android.bodegas.domain.subcategory.GetSubcategories
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.subsubcategory.mapper.GroupViewMapper
import com.android.bodegas.presentation.subsubcategory.model.GroupView
import com.android.bodegas.presentation.subcategory.mapper.SubCategoryViewMapper
import com.android.bodegas.presentation.subcategory.model.SubCategoryView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class SubCategoryViewModel constructor(
    private val getSubCategories: GetSubcategories,
    private val subcategoryViewMapper: SubCategoryViewMapper,
    private val getSSubCategory: GetSSubCategory,
    private val groupViewMapper: GroupViewMapper
) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    //de mutable a live data
    private val _listSubCategory = MutableLiveData<Resource<List<SubCategoryView>>>()
    val listSubCategory: LiveData<Resource<List<SubCategoryView>>> = _listSubCategory

    private val _listSSubCategory = MutableLiveData<Resource<GroupView>>()
    val listSSubCategory: LiveData<Resource<GroupView>> = _listSSubCategory

    fun getSubCategories(
        establishmentId: Int,
        categoryId: Int,
        page: String,
        size: String,
        sortBy: String
    ) {
        viewModelScope.launch {
            _listSubCategory.value =
                Resource(Status.LOADING, mutableListOf(), "")

            val subCategoryResult = getSubCategories.getSubCategories(
                establishmentId,
                categoryId,
                page,
                size,
                sortBy
            )

            if (subCategoryResult.status == Status.SUCCESS) {

                val subCategoryResultData = subCategoryResult.data

                if (subCategoryResultData.isNotEmpty()) {
                    val classView =
                        subCategoryResultData.map { subcategoryViewMapper.mapToView(it) }
                    _listSubCategory.value = Resource(Status.SUCCESS, classView, "")
                } else {
                    _listSubCategory.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _listSubCategory.value = Resource(Status.ERROR, mutableListOf(), "")
            }

        }
    }

    fun getSubSubCategories(establishmentId: Int, subCategoryId: Int) {
        viewModelScope.launch {
            _listSSubCategory.value =
                Resource(Status.LOADING, GroupView(0, mutableListOf(), mutableListOf()), "")

            val ssResult = getSSubCategory.getSSubCategory(establishmentId, subCategoryId)

            if (ssResult.status == Status.SUCCESS) {

                val ssResultData = groupViewMapper.mapToView(ssResult.data)
                _listSSubCategory.value = Resource(Status.SUCCESS, ssResultData, "")
            } else {
                _listSSubCategory.value =
                    Resource(Status.SUCCESS, GroupView(0, mutableListOf(), mutableListOf()), "")
            }
        }
    }
}