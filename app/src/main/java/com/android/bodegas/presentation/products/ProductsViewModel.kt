package com.android.bodegas.presentation.products

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegas.domain.products.GetProductsBySubCategory
import com.android.bodegas.domain.products.SearchProductsByEstablishment
import com.android.bodegas.domain.supplier.GetSupplierByType
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.products.mapper.ProductsViewMapper
import com.android.bodegas.presentation.products.model.ProductsView
import com.android.bodegas.presentation.supplier.mapper.SupplierViewMapper
import com.android.bodegas.presentation.supplier.model.SupplierView
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class ProductsViewModel constructor(
    private val getProductsBySubCategory: GetProductsBySubCategory,
    private val productsViewMapper: ProductsViewMapper
) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _listProducts = MutableLiveData<Resource<List<ProductsView>>>()
    val listProducts: LiveData<Resource<List<ProductsView>>> = _listProducts

    fun getProductsBySubSubCategory(establishmentId: Int, supplierDocument: String, subSubCategoryId: Int) {
        viewModelScope.launch {
            _listProducts.value =
                Resource(Status.LOADING, mutableListOf(), "")

            val productsResult =
                getProductsBySubCategory.getProductsBySubCategory(establishmentId,supplierDocument, subSubCategoryId)

            if (productsResult.status == Status.SUCCESS) {

                val productsResultData = productsResult.data

                if (productsResultData.isNotEmpty()) {
                    val suppliersView = productsResultData.map { productsViewMapper.mapToView(it) }
                    _listProducts.value = Resource(Status.SUCCESS, suppliersView, "")

                } else {
                    _listProducts.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _listProducts.value =
                    Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }


}