package com.android.bodegas.presentation.orders.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.bodegas.presentation.orders.model.CustomerOrderView
import kotlinx.android.synthetic.main.row_customer_order.view.*

class CustomerOrderViewHolder (itemView: View, viewHolderListener: ViewHolderListener):
    RecyclerView.ViewHolder(itemView) {

    private val boxDelivery : TextView = itemView.boxDelivery
    private val titleShiping : TextView = itemView.titleShiping
    private val inputShipping : TextView = itemView.inputShipping
    private val inputOrderDate : TextView = itemView.inputOrderDate
    private val inputHorarioAtencion: TextView = itemView.inputHorarioAtencion
    private val titleHorarioAtencion: TextView = itemView.titleHorarioAtencion

    private val inputEstablishmentName : TextView = itemView.inputNombreSupplier
    private val boxStatus : TextView = itemView.boxStatus
    private val inputTotal: TextView = itemView.inputTotal
    private val btnReSend: TextView = itemView.boxReOrder
    private val imageReSend: ImageView = itemView.imageReOrder
    private val btnDownload: ImageView = itemView.imDownload
    val holder: ViewHolderListener = viewHolderListener

    fun bind(customerOrderView: CustomerOrderView) {
        boxDelivery.text = customerOrderView.deliveryType
        inputEstablishmentName.text = customerOrderView.establishment.establishmentName
        boxStatus.text = customerOrderView.status
        inputTotal.text = String.format("%s %s", "s/ ",  customerOrderView.total)
        inputOrderDate.text = customerOrderView.orderDate

        if (customerOrderView.deliveryType == "Delivery"){
            titleShiping.visibility = View.VISIBLE
            inputShipping.visibility = View.VISIBLE
            inputHorarioAtencion.visibility = View.GONE
            titleHorarioAtencion.visibility = View.GONE
            inputShipping.text = customerOrderView.shippingDateFrom
        }else{
            titleHorarioAtencion.visibility = View.VISIBLE
            inputHorarioAtencion.text = customerOrderView.establishment.storeScheduleOperation[0].range.toString()
            inputHorarioAtencion.visibility = View.VISIBLE
            titleShiping.visibility = View.GONE
            inputShipping.visibility = View.GONE
        }

        itemView.setOnClickListener {
            holder.onClick(
                layoutPosition
            )
        }

        if(customerOrderView.status == "Entregado"){
            btnDownload.visibility = View.VISIBLE
            btnReSend.visibility = View.VISIBLE
            imageReSend.visibility = View.VISIBLE
        }else{
            imageReSend.visibility= View.GONE
            btnDownload.visibility = View.GONE
            btnReSend.visibility = View.GONE
        }

        btnReSend.setOnClickListener {
            holder.onClickReSend(layoutPosition)
        }

        btnDownload.setOnClickListener {
            holder.onClickDownLoad(layoutPosition)
        }

    }

    interface ViewHolderListener {
        fun onClick(
            position: Int
        )

        fun onClickReSend(
            position:Int
        )

        fun onClickDownLoad(
            position:Int
        )
    }
}