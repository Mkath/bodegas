package com.android.bodegas.presentation.register_user.mapper

import com.android.bodegas.domain.user.create.model.RegisterUserBody
import com.android.bodegas.presentation.register_user.model.RegisterUserBodyView
import com.android.bodegas.presentation.save_order.mapper.SaveViewMapper

open class RegisterUserBodyViewMapper :
    SaveViewMapper<RegisterUserBodyView, RegisterUserBody> {

    override fun mapToView(type: RegisterUserBody): RegisterUserBodyView {
        return RegisterUserBodyView(
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.country,
            type.creationUser,
            type.department,
            type.district,
            type.dni,
            type.email,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.password,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.urbanization
        )
    }

    override fun mapToUseCase(type: RegisterUserBodyView): RegisterUserBody {
        return RegisterUserBody(
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.country,
            type.creationUser,
            type.department,
            type.district,
            type.dni,
            type.email,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.password,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.urbanization
        )
    }
}