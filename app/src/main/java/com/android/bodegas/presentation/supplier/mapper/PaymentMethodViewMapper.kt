package com.android.bodegas.presentation.supplier.mapper

import com.android.bodegas.domain.supplier.PaymentMethod
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.supplier.model.PaymentMethodView

class PaymentMethodViewMapper : Mapper<PaymentMethodView, PaymentMethod> {

    override fun mapToView(type: PaymentMethod): PaymentMethodView {
        return PaymentMethodView(
            type.paymentMethodId,
            type.requestedAmount,
            type.description,
            type.customerMessage
        )
    }

}