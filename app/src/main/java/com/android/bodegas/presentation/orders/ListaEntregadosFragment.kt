package com.android.bodegas.presentation.orders

import android.app.DownloadManager
import android.content.*
import android.content.Context.DOWNLOAD_SERVICE
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.duplicate_order.DuplicateOrderItem
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.detail_orders.DetailOrderViewModel
import com.android.bodegas.presentation.detail_orders.DuplicateOrderViewModel
import com.android.bodegas.presentation.orders.adapter.CustomerOrderAdapter
import com.android.bodegas.presentation.orders.adapter.CustomerOrderViewHolder
import com.android.bodegas.presentation.orders.model.CustomerOrderView
import com.android.bodegas.presentation.orders.model.EstablishmentOrdersView
import com.android.bodegas.presentation.orders.model.OrdersItemView
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_lista_informados.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File


class ListaEntregadosFragment : Fragment(), CustomerOrderViewHolder.ViewHolderListener {

    private var listener: OnFragmentInteractionListener? = null
    private val customerOrdersViewModel: CustomerOrdersViewModel by sharedViewModel()
    private var customerOrdersViewList: MutableList<CustomerOrderView> = mutableListOf()
    private var customerOrdersViewListFiltered: MutableList<CustomerOrderView> = mutableListOf()
    private var adapter = CustomerOrderAdapter(this)
    private val detailOrdersViewModel: DetailOrderViewModel by viewModel()
    private var orderDetails = listOf<OrdersItemView>()
    private var establishmentId_duplicate: Int = 0
    private var orderId_duplicate: Int = 0

    private val duplicateOrderViewModel: DuplicateOrderViewModel by viewModel()
    private var orderIdx: Int = 0
    private lateinit var customerOrderViewReSend: CustomerOrderView
    private  var customerOrderViewReSendFiltered = listOf<OrdersItemView>()
    private  var flagTodosLosProductosDisponiblesParaReplica  = false

    private var establishmentOrder = EstablishmentOrdersView(
        0, "",
        "", false, mutableListOf(), mutableListOf(), mutableListOf(), false, ""
    )
    private var downloadID: Long = 0
    private lateinit var orderSelected: CustomerOrderView

    private val onDownloadComplete: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            val id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            if (downloadID == id) {
                Toast.makeText(context, "Descarga completa", Toast.LENGTH_SHORT).show()
            }
        }
    }

    companion object {
        const val TAG = "ListaEntregadosFragment"
        private val REQUEST_CODE_STORAGE = 200

        @JvmStatic
        fun newInstance() =
            ListaEntregadosFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity!!.registerReceiver(
            onDownloadComplete,
            IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        );
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lista_informados, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeValueTotal()
        observerOrdersByUser()
        observerFileByOrder()
        observerDetail1()
    }

    override fun onResume() {
        super.onResume()
        customerOrdersListClasses()
    }

    private fun customerOrdersListClasses() {
        customerOrdersViewModel.listCustomerOrders.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener?.hideLoading()
                    customerOrdersViewList = it.data.toMutableList()
                    provisionalFilter()
                }
                Status.ERROR -> {
                    listener?.hideLoading()
                    Snackbar.make(
                        informadosView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                    listener?.showLoading()
                }
            }
        })
        customerOrdersViewModel.getCustomerOrdersByCustomerId(
            PreferencesManager.getInstance().getUser().customerId
        ) //
    }

    private fun provisionalFilter() {
        customerOrdersViewListFiltered.clear()
        customerOrdersViewList.forEach {
            if (it.status == "Entregado") {
                customerOrdersViewListFiltered.add(it)
            }
        }

        if (customerOrdersViewListFiltered.isEmpty()) {
            containerEmptyState.visibility = View.VISIBLE
            rvCustomerOrdersInformados.visibility = View.GONE
        } else {
            setRecyclerView()
            containerEmptyState.visibility = View.GONE
            rvCustomerOrdersInformados.visibility = View.VISIBLE
        }
    }

    private fun setRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        rvCustomerOrdersInformados.layoutManager = linearLayoutManager
        rvCustomerOrdersInformados.adapter = adapter
        adapter.setStoreList(customerOrdersViewListFiltered)
    }

    interface OnFragmentInteractionListener {
        fun replaceByOrdersDetailFragment(customerOrderView: CustomerOrderView)
        fun showLoading()
        fun hideLoading()
    }


    override fun onClick(position: Int) {
        val classView = customerOrdersViewListFiltered[position]
        listener?.replaceByOrdersDetailFragment(classView)
    }

    override fun onClickReSend(position: Int) {
        val customerOrderView = customerOrdersViewListFiltered[position]
        customerOrderViewReSend = customerOrdersViewListFiltered[position]
        orderDetails = customerOrderView.orderDetails
        establishmentOrder = customerOrderView.establishment
        orderIdx = customerOrderView.orderId.toInt()
        duplicateOrderViewModel.filterOrder(establishmentOrder.establishmentId , orderIdx.toInt() )


        //keepWithReOrder()
       /* val dialogBuilder = AlertDialog.Builder(context!!)
        dialogBuilder
            .setMessage("¿Desea volver a realizar este pedido a la tienda ${customerOrderView.establishment.establishmentName}? Los productos disponibles se agregarán al carrito.")
            .setCancelable(false)
            .setPositiveButton("ACEPTAR", DialogInterface.OnClickListener { dialog, id ->
                sendNewOrder(orderIdx.toInt(), establishmentOrder.establishmentId)
            })
            .setNegativeButton("CANCELAR", DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
            })

        val alert = dialogBuilder.create()
        alert.setTitle("PEDIDOS")
        alert.show()*/
    }

    private fun observerDetail1() {
        duplicateOrderViewModel.duplicateOrder.observe(viewLifecycleOwner, Observer {

            flagTodosLosProductosDisponiblesParaReplica = it.data.flagAllAvailableProducts

            //aqui hacemos el filtro
             customerOrderViewReSendFiltered = listOf<OrdersItemView>() //limpiamos
            Log.e("error", "en customerOrderViewReSendFiltered antes de cargar: "+customerOrderViewReSendFiltered)
            val newList = mutableListOf<OrdersItemView>()

            for(a in it.data.orderDetailRequest) {
                Log.e("error", "################################ a: "+a)
                for ( b in customerOrderViewReSend.orderDetails) {
                    if (a.storeProductId.toString() == b.storeProductStoreProductId) {
                        Log.e("error", "################################ b: "+b)
                        b.orderDetailSubTotal = a.subTotal.toString()
                        b.orderDetailPrice = a.price.toString()
                        b.productTemplateName = a.description

                        newList.add(b)
                        Log.e("error", "--------------------------------------------- ")
                    }
                }
            }
            customerOrderViewReSendFiltered = newList
            Log.e("error", "en customerOrderViewReSendFiltered luego de cargar: "+customerOrderViewReSendFiltered)
            for(d in customerOrderViewReSendFiltered) {
                Log.e("error", "$$$$$$$$$$$$$$$$$$$ OBJETO DUPLICADO::::::::::::::::::::::::::::: "+d)
            }
            keepWithReOrder()
        })
    }

    fun keepWithReOrder() {
        val dialogBuilder = AlertDialog.Builder(context!!)
        dialogBuilder
            .setMessage("¿Desea volver a realizar este pedido a la tienda ${customerOrderViewReSend.establishment.establishmentName}? Los productos disponibles se agregarán al carrito.")
            .setCancelable(false)
            .setPositiveButton("ACEPTAR", DialogInterface.OnClickListener { dialog, id ->
                sendNewOrder(orderIdx.toInt(), establishmentOrder.establishmentId)
            })
            .setNegativeButton("CANCELAR", DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
            })

        val alert = dialogBuilder.create()
        alert.setTitle("PEDIDOS")
        alert.show()
    }

    override fun onClickDownLoad(position: Int) {
        val customerOrderView = customerOrdersViewListFiltered[position]
        orderSelected = customerOrderView
        if (validatePermissionsStorage()) {
            customerOrdersViewModel.getFileByOrder(customerOrderView.orderId.toInt())
        } else {
            requestPermissions()
        }

    }

    private fun validatePermissionsStorage(): Boolean {
        val readPermission = ActivityCompat.checkSelfPermission(
            activity!!.applicationContext,
            android.Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED

        val writePermission = ActivityCompat.checkSelfPermission(
            activity!!.applicationContext,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED

        return readPermission && writePermission

    }

    private fun sendNewOrder(establishmentId: Int, orderId: Int) {
        detailOrdersViewModel.getOrdersByUser(orderId, establishmentId )
        //aqui obtenemos los valores establishmentId and order Id y los enviamos al metodo addProductsForOrder, necesario para actualizar los precios al duplicar la compra
        establishmentId_duplicate = establishmentId
        orderId_duplicate = orderId
    }

    private fun observerOrdersByUser() {
        /*detailOrdersViewModel.orderByUser.observe(viewLifecycleOwner, Observer {
            Log.e("error", "------------------->>>>> orderId: ")

        })*/
        detailOrdersViewModel.orderByUser.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    Log.e("error", "################################ customerOrderViewReSendFiltered: "+customerOrderViewReSendFiltered)

                    if(customerOrderViewReSendFiltered.size >0) {
                        customerOrderViewReSendFiltered.forEach {
                            detailOrdersViewModel.addProductsForOrder(it, orderId_duplicate, establishmentId_duplicate )
                        }
                    }

                    else {
                        Snackbar.make(
                            informadosView,
                            "No existen productos disponibles a agregar al carrito de compras.",
                            Snackbar.LENGTH_LONG
                        ).show()
                    }


                }
                Status.ERROR -> {
                    Snackbar.make(
                        informadosView,
                        "Usted cuenta con un pedido en su carrito, debe completarlo para iniciar una nueva compra",
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                }
            }
        })

    }

    private fun observerFileByOrder() {
        customerOrdersViewModel.getFile.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener!!.hideLoading()
                    downloadFile(
                        it.data,
                        orderSelected.establishment.establishmentName,
                        orderSelected.orderId
                    )
                }
                Status.ERROR -> {
                    listener!!.hideLoading()
                    Snackbar.make(
                        informadosView,
                        "No se ha podido descargar el reporte, inténtelo nuevamente",
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                    listener!!.showLoading()
                }
            }
        })

    }

    private fun observeValueTotal() {
        detailOrdersViewModel.totalValue.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    val establishment = SupplierView(
                        establishmentOrder.establishmentId,
                        establishmentOrder.establishmentName, establishmentOrder.establishmentAddress
                        , "",0.00, 0.00,
                        establishmentOrder.storeScheduleShipping,
                        establishmentOrder.storeScheduleOperation,
                        "", "",
                        establishmentOrder.hasDelivery,
                        establishmentOrder.paymentMethod,
                        establishmentOrder.deliveryCharge
                    )
                    PreferencesManager.getInstance().setEstablishmentSelected(establishment)

                    PreferencesManager.getInstance().setChargeForDelivery(establishment.deliveryCharge)

                    var textByDefault = "Se ha generado el pedido con todos los productos disponibles, puede revisarlo en su carrito de compras"
                    if (!flagTodosLosProductosDisponiblesParaReplica) {
                        textByDefault = "Se ha generado el pedido, no obstante no todos los productos se encuentran disponibles."
                    }
                    Snackbar.make(
                        informadosView,
                        textByDefault,
                        Snackbar.LENGTH_SHORT
                    ).show()
                }

                Status.ERROR -> {
                    Snackbar.make(
                        informadosView,
                        "Ha ocurrido un error al generar su nuevo pedido",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }

                Status.LOADING -> {
                }
            }
        })
    }

    private fun requestPermissions() {
        val contextProvider =
            ActivityCompat.shouldShowRequestPermissionRationale(
                activity!!,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            )

        if (contextProvider) {
            Toast.makeText(
                activity!!.applicationContext,
                "Los permisos son requeridos para obtener la imagen",
                Toast.LENGTH_SHORT
            ).show()
        }
        permissionRequest()
    }

    private fun permissionRequest() {
        ActivityCompat.requestPermissions(
            activity!!,
            arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
            REQUEST_CODE_STORAGE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    customerOrdersViewModel.getFileByOrder(orderSelected.orderId.toInt())
                } else {
                    Toast.makeText(
                        activity!!.applicationContext,
                        "No aceptó los permisos",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    fun downloadFile(url: String, businessName: String, orderId: String) {
        val file = File(activity!!.getExternalFilesDir(null), "Dummy")

        val request =
            DownloadManager.Request(Uri.parse(url))
                .setTitle("Reporte de compra") // Title of the Download Notification
                .setDescription("Descargando") // Description of the Download Notification
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED) // Visibility of the download Notification
                .setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_DOWNLOADS,
                    "Pedido N° $orderId $businessName.xlsx"
                )
                .setAllowedOverMetered(true) // Set if download is allowed on Mobile network
                .setAllowedOverRoaming(true) // Set if download is allowed on roaming network

        val downloadManager = activity!!.getSystemService(DOWNLOAD_SERVICE) as DownloadManager?
        downloadID =
            downloadManager!!.enqueue(request) // enqueue puts the download request in the queue.
    }

    fun refreshList(){
        customerOrdersViewModel.getCustomerOrdersByCustomerId(
            PreferencesManager.getInstance().getUser().customerId
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.unregisterReceiver(onDownloadComplete);

    }
}
