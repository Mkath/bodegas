package com.android.bodegas.presentation.register_user

import com.android.bodegas.R
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.presentation.BaseActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_registro_user_identi.*

const val TIPO_DOC  ="TIPO_DOC"
const val NUMERO_DOC  ="NUMERO_DOC"

class RegistroIdentidadActivity : BaseActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro_user_identi)
        continuar_a_direccion()
        boton_registrar_usuario_final()
    }

    fun boton_registrar_usuario_final(){
        boton_set_tipodoc.setOnClickListener{
            showDialogTipoDoc()
        }
    }

    private fun showDialogTipoDoc(){
        lateinit  var dialog: AlertDialog
        val array = arrayOf("DNI", "Pasaporte", "Carné de extranjería")
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Elige un tipo de documento.")
        builder.setSingleChoiceItems(array,-1) { _, which-> val color = array[which]
            inputtipodoc.setText(color)
            dialog.dismiss() }
        dialog = builder.create()
        dialog.show()
    }

    private fun continuar_a_direccion(){
        boton_continuar_a_direccion.setOnClickListener{
        if(validar_alerts(inputtipodoc, boxtipodoc) && validar_extension_numero_doc()){
            registerSuccessfully()
            saveDataInLocalStorage()
            }
        }
    }

    private fun registerSuccessfully() {
        val bundle = Bundle()
        nextActivity(bundle, SearchPlacesActivity::class.java, true)
    }

    fun validar_extension_numero_doc() : Boolean{

        if(inputtipodoc.equals("DNI")){
            if(input_numerodoc.text.toString().length !=8) {
                box_numerodoc.error = null
                box_numerodoc.error = "Digita un número con 8 dígitos"
                return false
            }
            else{
                return true
            }
        }

        else if(inputtipodoc.equals("Pasaporte")){
            if(input_numerodoc.text.toString().length !=9) {
                box_numerodoc.error = null
                box_numerodoc.error = "Digita un número con 9 dígitos"
                return false
            }
            else{
                return true
            }
        }

        else { //"Carné de extranjería"
            if(input_numerodoc.text.toString().length !=8) {
                box_numerodoc.error = null
                box_numerodoc.error = "Digita un número con 8 dígitos"
                return false
            }
            else{
                return true
            }
        }
    }

    fun validar_alerts(campo_input: TextInputEditText, title_input: TextInputLayout) : Boolean{
        val campo = campo_input.text.toString()
        title_input.error = null
        if(campo == "Seleccionar") {
            title_input.error = "Selecciona un item de la lista."
            return false
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        comeBack()
    }

    private fun comeBack() {
        val bundle = Bundle()
        nextActivity(bundle, RegistroDatosBasicosActivity::class.java, true)
    }

    fun saveDataInLocalStorage(){
        PreferencesManager.getInstance().saveData(TIPO_DOC,inputtipodoc.text.toString())
        PreferencesManager.getInstance().saveData(NUMERO_DOC,input_numerodoc.text.toString())
    }

    override fun onResume() {
        super.onResume()
        loadDataFromLocalStorage()
    }

    fun loadDataFromLocalStorage(){
        val tipo_doc_ = PreferencesManager.getInstance().getData(TIPO_DOC)
        if(!tipo_doc_.equals("")) inputtipodoc.setText(tipo_doc_)
        val numero_doc_ = PreferencesManager.getInstance().getData(NUMERO_DOC)
        if(!numero_doc_.equals("")) input_numerodoc.setText(numero_doc_)
    }

}
