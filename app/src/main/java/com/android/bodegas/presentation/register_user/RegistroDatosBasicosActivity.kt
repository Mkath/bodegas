package com.android.bodegas.presentation.register_user

import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import kotlinx.android.synthetic.main.activity_registro_datos_basicos.*
import com.android.bodegas.presentation.BaseActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout


const val NOMBRE  ="NOMBRE"
const val APE_PATERNO  ="APE_PATERNO"
const val APE_MATERNO  ="APE_MATERNO"
const val CELULAR  ="CELULAR"
const val CORREO  ="CORREO"
const val CONTRASENIA  ="CONTRASENIA"

class RegistroDatosBasicosActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro_datos_basicos)
        registrar_datos()
    }

    override fun onResume() {
        super.onResume()
        loadDataFromLocalStorage()
    }

    private fun registrar_datos(){
        boton_registrar_datos.setOnClickListener{
            if( validar_extension_campo(inputname,boxname,2) &&
                validar_extension_campo(input_ape_paterno, box_ape_paterno, 2) &&
                validar_extension_campo(input_ape_materno, box_ape_materno, 2) &&
                validar_extension_campo(inputcelular, boxcelular, 9) &&
                validar_correo_ingresado() && validar_passwords()){
                Toast.makeText(this, "Los datos ingresados son validos", Toast.LENGTH_SHORT).show()
                registerSuccessfully()
                saveDataInLocalStorage()
            }
        }
    }

    private fun registerSuccessfully() {
        val bundle = Bundle()
        nextActivity(bundle, RegistroIdentidadActivity::class.java, true)
    }

    fun validar_extension_campo(campo_input: TextInputEditText, title_input: TextInputLayout, minima_longitud: Int) : Boolean{
        val campo = campo_input.text.toString()
        title_input.error = null
        if((campo.isEmpty()) || campo.length<minima_longitud) {
            title_input.error = "Introduce un dato más extenso."
            return false
        }
        return true
    }

    fun validar_correo_ingresado(): Boolean{
        if (!isEmailValid(inputcorreo.text.toString())){
            boxcorreo.isErrorEnabled = true
            boxcorreo.error = "Por favor, ingrese un correo válido"
            boxcorreo.boxBackgroundColor = ContextCompat.getColor(
                applicationContext,
                R.color.background_error_color
            )
            return false
        }
        else return true
    }

    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun validateInputPass(boxpassword1_ :TextInputLayout, inputpassword1_:TextInputEditText): Boolean {
        return if(inputpassword1_.text.toString().length >= 6 &&  inputpassword1_.text.toString().length <= 12){
            true
        } else{
            boxpassword1_.isErrorEnabled = true
            boxpassword1_.error = applicationContext.getString(R.string.error_pass)
            boxpassword1_.boxBackgroundColor = ContextCompat.getColor(
                applicationContext,
                R.color.background_error_color
            )
            false
        }
    }

    fun validar_passwords(): Boolean{
        if (validateInputPass(boxpassword1, inputpassword1) && validateInputPass(boxpassword2, inputpassword2))  {
            if(inputpassword1.text.toString() == inputpassword2.text.toString())  {
                Toast.makeText(this, "Contraseña valida!!", Toast.LENGTH_SHORT).show()
                return true }
        }
          return false
    }

    fun saveDataInLocalStorage(){
        PreferencesManager.getInstance().saveData(NOMBRE,inputname.text.toString())
        PreferencesManager.getInstance().saveData(APE_PATERNO,input_ape_paterno.text.toString())
        PreferencesManager.getInstance().saveData(APE_MATERNO,input_ape_materno.text.toString())
        PreferencesManager.getInstance().saveData(CELULAR,inputcelular.text.toString())
        PreferencesManager.getInstance().saveData(CORREO,inputcorreo.text.toString())
        PreferencesManager.getInstance().saveData(CONTRASENIA,inputpassword1.text.toString())
    }

    fun loadDataFromLocalStorage(){
        val nombre_ = PreferencesManager.getInstance().getData(NOMBRE)
        if(!nombre_.equals("")) inputname.setText(nombre_)
        val ape_paterno_ = PreferencesManager.getInstance().getData(APE_PATERNO)
        if(!ape_paterno_.equals("")) input_ape_paterno.setText(ape_paterno_)
        val ape_materno_ = PreferencesManager.getInstance().getData(APE_MATERNO)
        if(ape_materno_ != "") input_ape_materno.setText(ape_materno_)
        val celular_ = PreferencesManager.getInstance().getData(CELULAR)
        if(celular_ != "") inputcelular.setText(celular_)
        val correo_ = PreferencesManager.getInstance().getData(CORREO)
        if(correo_ != "") inputcorreo.setText(correo_)
        val password_ = PreferencesManager.getInstance().getData(CONTRASENIA)
        if(password_ != "") {
            inputpassword1.setText(password_)
            inputpassword2.setText(password_)
        }
    }



    //-------------------------
   /* val TIPO_DOC  ="TIPO_DOC"
    val NUMERO_DOC  ="NUMERO_DOC"
    //-------------------------
    val DIRECCION  ="DIRECCION"
    val LATITUD  ="LATITUD"
    val LONGITUD  ="LONGITUD"
    //-------------------------
    val PAIS  ="PAIS"
    val DEPARTAMENTO  ="DEPARTAMENTO"
    val PROVINCIA  ="PROVINCIA"
    val DISTRITO  ="DISTRITO"
    val URBANIZACION  ="URBANIZACION"*/


}
