package com.android.bodegas.presentation.subsubcategory

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegas.domain.subcategory.GetSubcategories
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.subcategory.mapper.SubCategoryViewMapper
import com.android.bodegas.presentation.subcategory.model.SubCategoryView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class SSubcategoryViewModel constructor(
    private val getClassesBySupplierId : GetSubcategories,
    private val classesViewMaper : SubCategoryViewMapper
) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    //de mutable a live data
    private val _listaClases = MutableLiveData<Resource<List<SubCategoryView>>> ()
    val listaClases : LiveData<Resource<List<SubCategoryView>>> = _listaClases

    fun getSubCategories(establishmentId:Int,
                         class_id: Int,
                         page: String,
                         size: String,
                         sortBy: String) {
        viewModelScope.launch {
            _listaClases.value =
                Resource(Status.LOADING, mutableListOf(), "")

            val classes_result = getClassesBySupplierId.getSubCategories(establishmentId, class_id, page, size, sortBy)

            if(classes_result.status == Status.SUCCESS){

                val classes_result_data = classes_result.data
                //validamos que la data no se encuentre vacia
                if(classes_result_data.isNotEmpty()){
                    //inyectamos al fragment si obtenemos data
                    val classView  = classes_result_data.map { classesViewMaper.mapToView(it) }
                    _listaClases.value = Resource(Status.SUCCESS, classView, "")
                }
                else{
                    _listaClases.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            }
            else{
                _listaClases.value = Resource(Status.ERROR, mutableListOf(), "")
            }

        }
    }
}