package com.android.bodegas.presentation.login.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserLoginView(
    val userToken: String,
    val userName:String
) : Parcelable