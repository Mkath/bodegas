package com.android.bodegas.presentation.register_user.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegisterUserBodyView(
    val address: String,
    val businessName: String,
    val cardNumber: String,
    val cardOperator: String,
    val country:String = "PE",
    val creationUser: String,
    val department: String,
    val district: String,
    val dni: String,
    val email: String,
    val lastNameMaternal: String,
    val lastNamePaternal: String,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val password: String,
    val phoneNumber: String,
    val province: String,
    val ruc: String,
    val urbanization: String
) : Parcelable
