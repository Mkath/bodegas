package com.android.bodegas.presentation.save_order.adapter

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.bodegas.presentation.save_order.model.SaveOrderView
import kotlinx.android.synthetic.main.row_products.view.btnAdd
import kotlinx.android.synthetic.main.row_products.view.btnRemove
import kotlinx.android.synthetic.main.row_products.view.ivImageProduct
import kotlinx.android.synthetic.main.row_products.view.tvNameProduct
import kotlinx.android.synthetic.main.row_products.view.tvSubTotalPrice
import kotlinx.android.synthetic.main.row_products.view.tvQuantity
import kotlinx.android.synthetic.main.row_products.view.tvUnit
import kotlinx.android.synthetic.main.row_save_order.view.*

class SaveOrderViewHolder(itemView: View, viewHolderListener: ViewHolderListener) :
    RecyclerView.ViewHolder(itemView) {
    val productName: TextView = itemView.tvNameProduct
    val productPrice: TextView = itemView.tvSubTotalPrice
    val imageProduct: ImageView = itemView.ivImageProduct
    val btnAdd: Button = itemView.btnAdd
    val btnRemove: Button = itemView.btnRemove
    val tvQuantity: TextView = itemView.tvQuantity
    val tvQuantityUnit: TextView = itemView.tvUnit
    val tvUnitPrice: TextView = itemView.tvUnitPrice
    val holder: ViewHolderListener = viewHolderListener

    fun bind(order: SaveOrderView) {
        productName.text = order.name
        tvUnitPrice.text = String.format("%s %.2f", "s/", order.price)
        productPrice.text = String.format("%s %.2f", "s/", order.subtotal)
        tvQuantityUnit.text = order.unitMeasure
        tvQuantity.text = order.quantity.toString()

        if (order.unitMeasure == "Kg" || order.unitMeasure == "KG") {
            buttonWeightBehavior(tvQuantity.text.toString().toDouble())
        }else{
            buttonsBehavior(tvQuantity.text.toString().toDouble())
        }

        btnAdd.setOnClickListener {
            var value = tvQuantity.text.toString().toDouble()
            if (order.unitMeasure == "Kg" || order.unitMeasure == "KG") {
                value += 0.25
                tvQuantity.text = value.toString()
                buttonWeightBehavior(value)
                holder.onClickChangeProduct(layoutPosition, value)
                productPrice.text = String.format("%s %.2f", "s/", order.price * value)
                order.subtotal = order.price * value

            } else {
                value++
                tvQuantity.text = value.toString()
                buttonsBehavior(value)
                holder.onClickChangeProduct(layoutPosition, value)
                productPrice.text = String.format("%s %.2f", "s/", order.price * value)
                order.subtotal = order.price * value
            }

        }

        btnRemove.setOnClickListener {
            var value = tvQuantity.text.toString().toDouble()
            if (order.unitMeasure == "Kg" ||  order.unitMeasure == "KG") {
                value -= 0.25
                tvQuantity.text = value.toString()
                buttonWeightBehavior(value)
                holder.onClickChangeProduct(layoutPosition, value)
                productPrice.text = String.format("%s %.2f", "s/", order.price * value)
                order.subtotal = order.price * value
            } else {
                value--
                tvQuantity.text = value.toString()
                buttonsBehavior(value)
                holder.onClickChangeProduct(layoutPosition, value)
                productPrice.text = String.format("%s %.2f", "s/", order.price * value)
                order.subtotal = order.price * value
            }

        }

        itemView.setOnClickListener {
            holder.onClickProducts(
                layoutPosition
            )
        }
    }


    private fun buttonsBehavior(value: Double) {
        if (value <= 1.0) {
            btnRemove.isEnabled = false
            btnAdd.isEnabled = true
        } else {
            btnRemove.isEnabled = true
            btnAdd.isEnabled = true
        }
    }

    private fun buttonWeightBehavior(value: Double) {
        if (value <= 0.25) {
            btnRemove.isEnabled = false
            btnAdd.isEnabled = true
        } else {
            btnRemove.isEnabled = true
            btnAdd.isEnabled = true
        }
    }

    interface ViewHolderListener {
        fun onClickProducts(
            position: Int
        )

        fun onClickChangeProduct(
            position: Int,
            quantity: Double
        )

    }
}

