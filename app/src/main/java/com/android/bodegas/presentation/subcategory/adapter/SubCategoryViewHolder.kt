package com.android.bodegas.presentation.subcategory.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_subcategories.view.*

class SubCategoryViewHolder  (itemView: View, viewHolderListener: ViewHolderListener):
    RecyclerView.ViewHolder(itemView) {

    //inyectamos la data
    val name_subcategory : TextView = itemView.name_subcategory
    val imageSubCategory : ImageView = itemView.ivImageSubCategory

    init {
        itemView.setOnClickListener {
            viewHolderListener.onClick(
                layoutPosition
            )
        }
    }

    interface ViewHolderListener {
        fun onClick(
            position: Int
        )
    }
}