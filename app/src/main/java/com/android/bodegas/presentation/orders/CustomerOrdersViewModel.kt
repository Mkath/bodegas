package com.android.bodegas.presentation.orders

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegas.di.customerOrders
import com.android.bodegas.domain.customerOrders.GetCustomerOrdersByCustomerId
import com.android.bodegas.domain.customerOrders.OrderDetailBody
import com.android.bodegas.domain.customerOrders.UpdateOrderStatus
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.orders.mapper.CustomerOrderViewMapper
import com.android.bodegas.presentation.orders.model.CustomerOrderView
import com.android.bodegas.presentation.orders.model.OrderDetailBodyView
import com.android.bodegas.presentation.orders.model.OrdersItemView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class CustomerOrdersViewModel constructor(
    private val getCustomerOrders: GetCustomerOrdersByCustomerId,
    private val customerOrdersViewMapper: CustomerOrderViewMapper
) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _listCustomerOrders = MutableLiveData<Resource<List<CustomerOrderView>>>()
    val listCustomerOrders: LiveData<Resource<List<CustomerOrderView>>> = _listCustomerOrders

    private val _getFile = MutableLiveData<Resource<String>>()
    val getFile: LiveData<Resource<String>> = _getFile

    fun getCustomerOrdersByCustomerId(
        customerId: Int
    ) {
        viewModelScope.launch {
            _listCustomerOrders.value =
                Resource(Status.LOADING, mutableListOf(), "")

            val customerOrdersResult =
                getCustomerOrders.getCustomerOrdersByCustomerId(customerId)

            if (customerOrdersResult.status == Status.SUCCESS) {
                val customerOrdersResultData = customerOrdersResult.data
                if (customerOrdersResultData.isNotEmpty()) {
                    customerOrdersResultData.sortedBy { it.orderDate }
                    val customerOrdersView = customerOrdersResultData.map { customerOrdersViewMapper.mapToView(it) }

                    _listCustomerOrders.value = Resource(Status.SUCCESS, customerOrdersView, "")
                } else {
                    _listCustomerOrders.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _listCustomerOrders.value = Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }

    fun getFileByOrder(orderId: Int){
        _getFile.value = Resource(Status.LOADING, "", "")
        viewModelScope.launch {
            val result = getCustomerOrders.getFileByOrder(orderId)
            if(result.status == Status.SUCCESS){
                _getFile.value = Resource(Status.SUCCESS, result.data, "")
            }else{
                _getFile.value = Resource(Status.ERROR, "", "")
            }
        }
    }
}