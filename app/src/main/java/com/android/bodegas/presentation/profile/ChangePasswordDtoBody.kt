package com.android.bodegas.presentation.profile

import com.google.gson.annotations.SerializedName

data class ChangePasswordDtoBody(
    @SerializedName("email")
    var email: String,

    @SerializedName("passwordNew")
    val passwordNew: String,
    // val country: String,

    @SerializedName("passwordOld")
    val passwordOld: String,

    @SerializedName("userType")
    val userType: String
)