package com.android.bodegas.presentation.detail_orders

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegas.domain.duplicate_order.DuplicateOrder
import com.android.bodegas.domain.duplicate_order.GetDuplicateOrder
import com.android.bodegas.domain.save_order.GetOrderByUser
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class DuplicateOrderViewModel(
    private val getDuplicateOrder: GetDuplicateOrder
) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _duplicateOrder = MutableLiveData<Resource<DuplicateOrder>>()
    val duplicateOrder: LiveData<Resource<DuplicateOrder>> = _duplicateOrder


    fun filterOrder(establishmentId: Int, orderId: Int) {
        viewModelScope.launch {
            _duplicateOrder.value = getDuplicateOrder.getDuplicateOrder(establishmentId,orderId)
        }
    }
}