package com.android.bodegas.presentation.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.android.bodegas.R
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.utils.animation.LoadingDots
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_recovery_password.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import androidx.lifecycle.Observer
import com.android.bodegas.presentation.BaseActivity

class RecoveryPasswordActivity : BaseActivity() {

    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recovery_password)
        setOn()
        observeUpdateEmail()
    }

    fun setOn(){
        btnSendEmail.setOnClickListener{
            Log.e("db","inputMail: "+ inputMail.text.toString().toLowerCase())
            val obj = ForgetPassDTO(inputMail.text.toString().toLowerCase())
            loginViewModel.sendEmailToRecoveryAccount(obj)
        }
        btnCancelar.setOnClickListener{
            nextActivity(null, LoginActivity::class.java, true)
        }
    }

    private fun observeUpdateEmail(){
        loginViewModel.sendEmail.observe( this, Observer {
            when(it.status){
                Status.SUCCESS -> {
                    hideKeyboard()
                    layoutProgressBar.visibility = View.GONE
                    Snackbar.make(
                        background,
                        "Se envió el mail correctamente.",
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.ERROR -> {
                    hideKeyboard()
                    layoutProgressBar.visibility = View.GONE
                    showErrorMessage(
                        it.message.let { "Se envió el mail correctamente."})
                }

                Status.LOADING -> {
                    hideKeyboard()
                    layoutProgressBar.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun showErrorMessage(message: String) {
        Snackbar.make(
            background,
            message,
            Snackbar.LENGTH_LONG
        ).show()
    }
}
