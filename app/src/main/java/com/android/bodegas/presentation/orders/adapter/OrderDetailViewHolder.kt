package com.android.bodegas.presentation.orders.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_customer_order_detail.view.*

class OrderDetailViewHolder (itemView: View, viewHolderListener: ViewHolderListener):
    RecyclerView.ViewHolder(itemView) {

    //inyectamos la data
    val inputImagen: ImageView = itemView.inputImagen
    val subtotalPrice : TextView = itemView.tvSubTotalPrice
    val unitPrice : TextView = itemView.tvUnitPrice
    val inputCantidad : TextView = itemView.tvQuantity
    val inputEstatus: TextView = itemView.tvStatus
    val inputName: TextView = itemView.tvNameProduct
    val descripcion: TextView = itemView.tvDescripcion

    init {
        itemView.setOnClickListener {
            viewHolderListener.onClick(
                layoutPosition
            )
        }
    }

    interface ViewHolderListener {
        fun onClick(
            position: Int
        )
    }
}