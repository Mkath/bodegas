package com.android.bodegas.presentation.category

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegas.R
import com.android.bodegas.presentation.category.adapter.CategoryAdapter
import com.android.bodegas.presentation.category.model.CategoryView
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.category.adapter.CategoryViewHolder
import com.android.bodegas.presentation.products.SearchProductsViewModel
import com.android.bodegas.presentation.supplier.model.SupplierView
import kotlinx.android.synthetic.main.fragment_category.*


const val ESTABLISHMENT = "establisment"
const val CLASS_ID = "class_id"


class CategoryFragment : Fragment(), CategoryViewHolder.ViewHolderListener {

    private var listener: OnFragmentInteractionListener? = null
    private val categoryViewModel: CategoryViewModel by viewModel()
    private val searchProductsViewModel: SearchProductsViewModel by viewModel()
    private var categoryViewList: MutableList<CategoryView> = mutableListOf()
    private var adapter = CategoryAdapter(this)
    private lateinit var establishment: SupplierView
    private var classId: Int = 0

    companion object {

        const val TAG = "CategoryFragment"

        @JvmStatic
        fun newInstance(establishment: SupplierView, classId: Int) =
            CategoryFragment().apply {
                arguments = Bundle().apply {
                    putInt(CLASS_ID, classId)
                    putParcelable(ESTABLISHMENT, establishment)

                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        classId = arguments!!.getInt(CLASS_ID)
        establishment = arguments!!.getParcelable(ESTABLISHMENT)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    private fun observeListClasses() {
        categoryViewModel.listCategories.observe(this, Observer {
            when (it.status) {

                Status.SUCCESS -> {
                    Log.e("Debug", " Status.SUCCESS")
                    listener?.hideLoading()
                    categoryViewList = it.data.toMutableList()
                    if(categoryViewList.isEmpty()){
                        containerEmptyState.visibility = View.VISIBLE
                        rvCategories.visibility = View.GONE
                    }else{
                        containerEmptyState.visibility = View.GONE
                        rvCategories.visibility = View.VISIBLE
                        setRecyclerView()
                    }
                }
                Status.ERROR -> {
                    Log.e("Debug", "Status.ERROR")
                    listener?.hideLoading()
                    Snackbar.make(
                        categoryView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                    Log.e("Debug", " Status.LOADING")
                    listener?.showLoading()
                }
            }
        })
        categoryViewModel.getCategories(establishment.storeId, classId, "", "", "")
    }


    override fun onResume() {
        super.onResume()
        observeListClasses()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onClick(position: Int) {
        val categoryView = categoryViewList[position]
        listener?.replaceSubCategoryFragment(
            establishment, categoryView.id, categoryView.name)
    }

    private fun setRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        rvCategories.layoutManager = linearLayoutManager
        rvCategories.adapter = adapter
        adapter.setStoreList(categoryViewList)
    }

    interface OnFragmentInteractionListener {
        fun replaceSubCategoryFragment(establishment: SupplierView, categoryId: Int, categoryName: String)
        fun showLoading()
        fun hideLoading()
    }

}
