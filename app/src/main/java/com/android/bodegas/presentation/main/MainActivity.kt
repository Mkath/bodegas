package com.android.bodegas.presentation.main

import android.Manifest
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.android.bodegas.BuildConfig
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.category.CategoryFragment
import com.android.bodegas.presentation.clases.ClassFragment
import com.android.bodegas.presentation.detail_orders.DetailOrderFragment
import com.android.bodegas.presentation.favorites.FavoritesFragment
import com.android.bodegas.presentation.login.LoginActivity
import com.android.bodegas.presentation.orders.ListaEntregadosFragment
import com.android.bodegas.presentation.orders.ListaInformadosFragment
import com.android.bodegas.presentation.orders.TabLayoutFragment
import com.android.bodegas.presentation.orders.model.CustomerOrderView
import com.android.bodegas.presentation.products.ProductsFragment
import com.android.bodegas.presentation.products.SearchProductsViewModel
import com.android.bodegas.presentation.products.model.ProductsView
import com.android.bodegas.presentation.profile.ProfileFragment
import com.android.bodegas.presentation.purchase_details.PurchaseDetailsFragment
import com.android.bodegas.presentation.save_order.SaveOrderFragment
import com.android.bodegas.presentation.subcategory.SubcategoryFragment
import com.android.bodegas.presentation.subsubcategory.SSubcategoryFragment
import com.android.bodegas.presentation.subsubcategory.model.SSubcategoryView
import com.android.bodegas.presentation.supplier.SupplierFragment
import com.android.bodegas.presentation.supplier.map_stores.MapStoreMarketFragment
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.android.bodegas.presentation.supplier.store.StoreFragment
import com.android.bodegas.presentation.utils.animation.LoadingAnimationImageView
import com.android.bodegas.presentation.utils.animation.LoadingDots
import com.android.bodegas.presentation.utils.location.*
import com.android.bodegas.presentation.utils.location.SharedPreferenceUtil
import com.google.android.gms.location.LocationResult
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList

private const val REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE = 34
private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity(), SupplierFragment.OnFragmentInteractionListener,
    MapStoreMarketFragment.OnFragmentInteractionListener,
    ClassFragment.OnFragmentInteractionListener,
    StoreFragment.OnFragmentInteractionListener,
    CategoryFragment.OnFragmentInteractionListener,
    SubcategoryFragment.OnFragmentInteractionListener,
    SSubcategoryFragment.OnFragmentInteractionListener,
    ProductsFragment.OnFragmentInteractionListener,
    FavoritesFragment.OnFragmentInteractionListener,
    SaveOrderFragment.OnFragmentInteractionListener,
    PurchaseDetailsFragment.OnFragmentInteractionListener,
    ListaInformadosFragment.OnFragmentInteractionListener,
    ListaEntregadosFragment.OnFragmentInteractionListener,
    DetailOrderFragment.OnFragmentInteractionListener,
    ProfileFragment.OnFragmentInteractionListener, RecognitionListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private var loadingAnimationImageView: LoadingAnimationImageView? = null
    private var loadingDots: LoadingDots? = null
    private lateinit var className: String
    private lateinit var categoryName: String
    private lateinit var subCategoryName: String
    private lateinit var subsubCategoryName: String
    private var typeStore = 0
    private val searchProductsViewModel: SearchProductsViewModel by viewModel()
    var location: Location? = null
    lateinit var establishment: SupplierView
    private var speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
    val RecordAudioRequestCode = 1
    private val REQUESTCODE = 100
    private var isFromVoice = false


    //private var foregroundOnlyLocationServiceBound = false

    // Provides location updates for while-in-use feature.
    //private var foregroundOnlyLocationService: ForegroundOnlyLocationService? = null

    // Listens for location broadcasts from ForegroundOnlyLocationService.
    //private lateinit var foregroundOnlyBroadcastReceiver: ForegroundOnlyBroadcastReceiver

    private lateinit var sharedPreferences: SharedPreferences

    //private lateinit var foregroundOnlyLocationButton: Button

    /*private val foregroundOnlyServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as ForegroundOnlyLocationService.LocalBinder
            foregroundOnlyLocationService = binder.service
            foregroundOnlyLocationServiceBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            foregroundOnlyLocationService = null
            foregroundOnlyLocationServiceBound = false
        }
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        llBack.setOnClickListener {
            onBackPressed()
        }
        btnMicrophone.setOnClickListener {
            val permissionCheck = ContextCompat.checkSelfPermission(
                applicationContext,
                Manifest.permission.RECORD_AUDIO
            )

            if (permissionCheck == PERMISSION_GRANTED) {
                startSpeechToText()
                Log.i("SpeechActivity", "Speech Recognizer is listening")
            } else {

                // you don't have permission, try requesting for it
                ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.RECORD_AUDIO),
                    REQUESTCODE
                )
            }
        }

        searchBehavior()
        showLoading()

        try {
            location = Location(this, object : CustomLocationListener {
                override fun locationResponse(locationResult: LocationResult) {
                    if (locationResult.lastLocation.latitude != 0.0) {
                        val addresses: List<Address>
                        val geocoder = Geocoder(applicationContext)
                        addresses = geocoder.getFromLocation(
                            locationResult.lastLocation.latitude,
                            locationResult.lastLocation.longitude,
                            1
                        )
                        if (addresses != null) {
                            tvDirection.text = addresses[0].getAddressLine(0)
                            if (isFragmentVisible(SupplierFragment.TAG)) {
                                val fragment =
                                    supportFragmentManager.findFragmentByTag(SupplierFragment.TAG) as SupplierFragment
                                fragment.refreshListByUbication(
                                    locationResult.lastLocation.latitude,
                                    locationResult.lastLocation.longitude
                                )
                                location?.stopUpdateLocation()
                            }
                        } else {
                            tvDirection.text = "Dirección no retornada"
                        }
                    } else {
                        tvDirection.text = "Ubicar dirección"
                    }
                }
            })
        } catch (e: Exception) {
            Toast.makeText(
                applicationContext,
                "Hubo un problema obteniendo tu ubicación, seleccionalo nuevamente.",
                Toast.LENGTH_LONG
            ).show()
        }

        /*foregroundOnlyBroadcastReceiver = ForegroundOnlyBroadcastReceiver()

        sharedPreferences =
            getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        val enabled = sharedPreferences.getBoolean(
            SharedPreferenceUtil.KEY_FOREGROUND_ENABLED, false
        )

        if (enabled) {
            foregroundOnlyLocationService?.unsubscribeToLocationUpdates()
        } else {

            // TODO: Step 1.0, Review Permissions: Checks and requests if needed.
            if (foregroundPermissionApproved()) {
                foregroundOnlyLocationService?.subscribeToLocationUpdates() ?: Log.d(TAG, "Service Not Bound")
            } else {
                requestForegroundPermissions()
            }
        }*/

        bottom_navigation.selectedItemId = R.id.item_stores
    }


    private fun startSpeechToText() {

        val speechRecognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        speechRecognizerIntent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())

        speechRecognizer.setRecognitionListener(object : RecognitionListener {
            override fun onReadyForSpeech(bundle: Bundle) {}

            override fun onBeginningOfSpeech() {
                search.setIconified(false)
                search.findViewById<AutoCompleteTextView>(R.id.search_src_text)
                    .setText("Escuchando..")
                isFromVoice = true
            }

            override fun onRmsChanged(v: Float) {}

            override fun onBufferReceived(bytes: ByteArray) {}

            override fun onEndOfSpeech() {}

            override fun onError(i: Int) {}

            override fun onResults(bundle: Bundle) {
                val matches =
                    bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)//getting all the matches
                //displaying the first match
                if (matches != null) {
                    isFromVoice = true
                    search.findViewById<AutoCompleteTextView>(R.id.search_src_text)
                        .setText(matches[0])
                }
                //  editText.setText(matches[0])
            }

            override fun onPartialResults(bundle: Bundle) {}

            override fun onEvent(i: Int, bundle: Bundle) {}
        })
        speechRecognizer.startListening(speechRecognizerIntent)

    }

    override fun onDestroy() {
        super.onDestroy()
        speechRecognizer!!.destroy()
    }

    private fun buildAlertMessageNoGps() {
        val dialogBuilder = AlertDialog.Builder(this)
        // set message of alert dialog
        dialogBuilder
            .setMessage("Tu GPS se encuentra desabilitado, por favor habilitado para que puedas ubicar tus tiendas más cercanas")
            // if the dialog is cancelable
            .setCancelable(false)
            // positive button text and action
            .setPositiveButton(
                "ACTIVAR",
                DialogInterface.OnClickListener { dialog, whichButton ->
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                })
            .setNegativeButton("CANCELAR", DialogInterface.OnClickListener { dialog, which ->
                dialog.cancel();

            })
        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle("UBICACIÓN")
        // show alert dialog
        alert.show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        location?.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == RecordAudioRequestCode && grantResults.size > 0) {
            if (grantResults[0] == PERMISSION_GRANTED) Toast.makeText(
                this,
                "Permission Granted",
                Toast.LENGTH_SHORT
            ).show()
        }

        when (requestCode) {
            REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE -> when {
                    // If user interaction was interrupted, the permission request
                    // is cancelled and you receive empty arrays.

                    grantResults[0] == PackageManager.PERMISSION_GRANTED ->{

                    }
                    // Permission was granted.
                    //foregroundOnlyLocationService?.subscribeToLocationUpdates()

                else -> {
                    // Permission denied.
                    Snackbar.make(
                        mainView,
                        R.string.permission_denied_explanation,
                        Snackbar.LENGTH_LONG
                    )
                        .setAction(R.string.settings) {
                            // Build intent that displays the App settings screen.
                            val intent = Intent()
                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts(
                                "package",
                                BuildConfig.APPLICATION_ID,
                                null
                            )
                            intent.data = uri
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        }
                        .show()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps()
        } else {
            location?.inicializeLocation()

            /*sharedPreferences.registerOnSharedPreferenceChangeListener(this)

            val serviceIntent = Intent(this, ForegroundOnlyLocationService::class.java)
            bindService(serviceIntent, foregroundOnlyServiceConnection, Context.BIND_AUTO_CREATE)*/
        }


    }

    override fun onPause() {

        location?.stopUpdateLocation()

       /* LocalBroadcastManager.getInstance(this).unregisterReceiver(
            foregroundOnlyBroadcastReceiver
        )*/
        super.onPause()

    }

    override fun onStop() {
        /*if (foregroundOnlyLocationServiceBound) {
            unbindService(foregroundOnlyServiceConnection)
            foregroundOnlyLocationServiceBound = false
        }*/
        //sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)

        location?.stopUpdateLocation()

        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        when (bottom_navigation.selectedItemId) {
            R.id.item_profile -> {
                val fragment =
                    supportFragmentManager.findFragmentById(R.id.frameLayoutProfile) as ProfileFragment
                fragment.refreshView()
            }
        }

        /*LocalBroadcastManager.getInstance(this).registerReceiver(
            foregroundOnlyBroadcastReceiver,
            IntentFilter(
                ForegroundOnlyLocationService.ACTION_FOREGROUND_ONLY_LOCATION_BROADCAST
            )
        )*/
    }

    private fun searchBehavior() {
        observeProductsList()
        search.queryHint = "Buscar producto"
        search.findViewById<AutoCompleteTextView>(R.id.search_src_text).threshold = 1
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    searchProductsViewModel.searchProductByEstablishment(
                        it,
                        establishment.storeId
                    )
                }
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                val listen = "Escuchando.."
                query?.let {
                    if (isFromVoice) {
                        if (query != listen) {
                            searchProductsViewModel.searchProductByEstablishment(
                                it,
                                establishment.storeId
                            )
                        }
                        isFromVoice = false
                    }

                }
                return true
            }
        })
    }

    private fun observeProductsList() {
        searchProductsViewModel.listProducts.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    search.setIconified(true)
                    if (it.data.isNotEmpty()) {
                        val fragment =
                            supportFragmentManager.findFragmentById(R.id.frameLayoutFragment)
                        if (fragment!!.tag == ProductsFragment.TAG) {
                            val fragmentProducts =
                                supportFragmentManager.findFragmentByTag(ProductsFragment.TAG) as ProductsFragment
                            fragmentProducts.refreshList(it.data)
                        } else {
                            replaceProductsFragment(establishment, it.data, "", true)
                        }
                    } else {
                        hideLoading()
                        Snackbar.make(
                            mainView,
                            getString(R.string.producto_no_encontrado),
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                }

                Status.ERROR -> {
                    hideLoading()
                    Snackbar.make(
                        mainView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                    showLoading()
                }
            }
        })
    }

    private fun setSupplierFragment() {
        supportFragmentManager.beginTransaction()
            .add(
                R.id.frameLayoutFragment,
                SupplierFragment.newInstance(),
                SupplierFragment.TAG
            )
            .addToBackStack(SupplierFragment.TAG).commitAllowingStateLoss()
    }


    override fun replaceStoresFragment(type: Int, latitude: Double, longitude: Double) {
        llBack.visibility = View.VISIBLE
        tvDirection.visibility = View.GONE
        ivChangeLocation.visibility = View.GONE
        tvTitle.visibility = View.VISIBLE
        this.typeStore = type
        if (type == StoreFragment.STORE) {
            tvTitle.text = "Selecciona tu bodega más cercana"
        } else {
            tvTitle.text = "Selecciona tu puesto más cercano"
        }

        supportFragmentManager.beginTransaction()
            .replace(
                R.id.frameLayoutFragment,
                StoreFragment.newInstance(type, latitude, longitude),
                StoreFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            .hide(supportFragmentManager.findFragmentByTag(SupplierFragment.TAG)!!)
            .addToBackStack(StoreFragment.TAG).commitAllowingStateLoss()
    }

    override fun replaceMapStoresFragment(type: Int, latitude: Double, longitude: Double) {
        llBack.visibility = View.VISIBLE
        tvDirection.visibility = View.GONE
        ivChangeLocation.visibility = View.GONE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = "Selecciona"
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.frameLayoutFragment,
                MapStoreMarketFragment.newInstance(type, latitude, longitude),
                MapStoreMarketFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            .hide(supportFragmentManager.findFragmentByTag(StoreFragment.TAG)!!)
            .addToBackStack(MapStoreMarketFragment.TAG).commitAllowingStateLoss()
    }

    override fun replaceClassFragment(establishment: SupplierView) {
        this.establishment = establishment
        llBack.visibility = View.VISIBLE
        tvDirection.visibility = View.GONE
        ivChangeLocation.visibility = View.GONE
        containerSearch.visibility = View.VISIBLE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = "Selecciona una categoría"
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.frameLayoutFragment,
                ClassFragment.newInstance(establishment),
                ClassFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            .hide(supportFragmentManager.findFragmentByTag(SupplierFragment.TAG)!!)
            .addToBackStack(ClassFragment.TAG).commitAllowingStateLoss()
    }

    override fun replaceByCategoryFragment(
        establishment: SupplierView,
        classId: Int,
        className: String
    ) {
        llBack.visibility = View.VISIBLE
        tvDirection.visibility = View.GONE
        ivChangeLocation.visibility = View.GONE
        containerSearch.visibility = View.VISIBLE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = className
        this.className = className
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.frameLayoutFragment,
                CategoryFragment.newInstance(establishment, classId),
                CategoryFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            .hide(supportFragmentManager.findFragmentByTag(ClassFragment.TAG)!!)
            .addToBackStack(CategoryFragment.TAG).commitAllowingStateLoss()
    }

    override fun replaceSubCategoryFragment(
        establishment: SupplierView,
        categoryId: Int,
        categoryName: String
    ) {
        llBack.visibility = View.VISIBLE
        tvDirection.visibility = View.GONE
        ivChangeLocation.visibility = View.GONE
        containerSearch.visibility = View.VISIBLE

        tvTitle.visibility = View.VISIBLE
        tvTitle.text = categoryName
        this.categoryName = categoryName
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.frameLayoutFragment,
                SubcategoryFragment.newInstance(establishment, categoryId),
                SubcategoryFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            .hide(supportFragmentManager.findFragmentByTag(CategoryFragment.TAG)!!)
            .addToBackStack(SubcategoryFragment.TAG).commitAllowingStateLoss()
    }

    override fun replaceProductsFragment(
        establishment: SupplierView,
        productsList: List<ProductsView>,
        subCategoryName: String,
        isFromSubCategory: Boolean
    ) {
        llBack.visibility = View.VISIBLE
        tvDirection.visibility = View.GONE
        ivChangeLocation.visibility = View.GONE
        containerSearch.visibility = View.VISIBLE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = subCategoryName

        if (isFromSubCategory) {
            this.subCategoryName = subCategoryName
        } else {
            this.subsubCategoryName = subCategoryName
        }

        val productsArrayList = productsList.toCollection(ArrayList())
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.frameLayoutFragment,
                ProductsFragment.newInstance(establishment, productsArrayList),
                ProductsFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            //    .hide(supportFragmentManager.findFragmentByTag(SubcategoryFragment.TAG)!!)
            .addToBackStack(ProductsFragment.TAG).commitAllowingStateLoss()
    }

    override fun replaceSSubCategoryFragment(
        ssList: List<SSubcategoryView>,
        subCategoryName: String,
        establishment: SupplierView
    ) {
        llBack.visibility = View.VISIBLE
        tvDirection.visibility = View.GONE
        ivChangeLocation.visibility = View.GONE
        containerSearch.visibility = View.VISIBLE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = subCategoryName
        this.subCategoryName = subCategoryName
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.frameLayoutFragment,
                SSubcategoryFragment.newInstance(ssList.toCollection(ArrayList()), establishment),
                SSubcategoryFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            .hide(supportFragmentManager.findFragmentByTag(SubcategoryFragment.TAG)!!)
            .addToBackStack(SSubcategoryFragment.TAG).commitAllowingStateLoss()
    }

    private fun replaceFavoritesFragments() {
        llBack.visibility = View.GONE
        tvDirection.visibility = View.GONE
        ivChangeLocation.visibility = View.GONE
        containerSearch.visibility = View.GONE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = "Favoritos"

        supportFragmentManager.beginTransaction()
            .add(
                R.id.frameLayoutFavoritesFragment,
                FavoritesFragment.newInstance(),
                FavoritesFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            .commit()
    }

    private fun replaceProfileFragment() {
        llBack.visibility = View.GONE
        tvDirection.visibility = View.GONE
        ivChangeLocation.visibility = View.GONE
        containerSearch.visibility = View.GONE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = "Perfil"

        supportFragmentManager.beginTransaction()
            .add(
                R.id.frameLayoutProfile,
                ProfileFragment.newInstance(),
                ProfileFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            .commit()
    }

    private fun replaceCustomerOrdersFragments() {
        llBack.visibility = View.GONE
        tvDirection.visibility = View.GONE
        ivChangeLocation.visibility = View.GONE
        containerSearch.visibility = View.GONE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = "Historial de órdenes"

        supportFragmentManager.beginTransaction()
            .add(
                R.id.frameLayoutOrderCustomerFragment,
                TabLayoutFragment.newInstance(),
                TabLayoutFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            ).commit()
    }

    private fun replaceSendOrderFragments() {
        llBack.visibility = View.GONE
        tvDirection.visibility = View.GONE
        ivChangeLocation.visibility = View.GONE
        containerSearch.visibility = View.GONE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = "Carrito"

        supportFragmentManager.beginTransaction()
            .add(
                R.id.frameLayoutPurchaseFragment,
                SaveOrderFragment.newInstance(),
                SaveOrderFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            ).commit()
    }


    override fun replacePurchaseDetailsFragment() {
        llBack.visibility = View.VISIBLE
        tvDirection.visibility = View.GONE
        ivChangeLocation.visibility = View.GONE
        containerSearch.visibility = View.GONE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = "Carrito"

        supportFragmentManager.beginTransaction()
            .add(
                R.id.frameLayoutPurchaseFragment,
                PurchaseDetailsFragment.newInstance(),
                PurchaseDetailsFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            .hide(supportFragmentManager.findFragmentByTag(SaveOrderFragment.TAG)!!)
            .addToBackStack(PurchaseDetailsFragment.TAG).commitAllowingStateLoss()
    }

    override fun replaceByOrdersDetailFragment(customerOrderView: CustomerOrderView) {
        llBack.visibility = View.VISIBLE
        tvDirection.visibility = View.GONE
        ivChangeLocation.visibility = View.GONE
        containerSearch.visibility = View.GONE
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = "Detalle del pedido"

        supportFragmentManager.beginTransaction()
            .add(
                R.id.frameLayoutOrderCustomerFragment,
                DetailOrderFragment.newInstance(customerOrderView),
                DetailOrderFragment.TAG
            ).setCustomAnimations(
                R.anim.slide_enter_from_right,
                R.anim.slide_exit_to_right,
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )
            .hide(supportFragmentManager.findFragmentByTag(TabLayoutFragment.TAG)!!)
            .addToBackStack(DetailOrderFragment.TAG).commitAllowingStateLoss()
    }


    override fun onBackPressed() {
        checkForVisibleMenuFragment()
    }

    private fun popBackStack() {
        val count = supportFragmentManager.backStackEntryCount
        if (count < 2) {
            finish()
        } else {
            supportFragmentManager.popBackStackImmediate()
            checkForVisibleStackFragment()
        }
    }

    private fun checkForVisibleStackFragment() {
        when {

            isFragmentVisible(SupplierFragment.TAG) -> {
                llBack.visibility = View.GONE
                tvDirection.visibility = View.VISIBLE
                ivChangeLocation.visibility = View.VISIBLE
                tvTitle.visibility = View.GONE
                containerSearch.visibility = View.GONE
            }

            isFragmentVisible(StoreFragment.TAG) -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                if (typeStore == StoreFragment.STORE) {
                    tvTitle.text = "Selecciona tu bodega más cercana"
                } else {
                    tvTitle.text = "Selecciona tu puesto más cercaoa"
                }
            }

            isFragmentVisible(ClassFragment.TAG) -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                containerSearch.visibility = View.VISIBLE
                tvTitle.text = "Selecciona una categoría"

            }

            isFragmentVisible(CategoryFragment.TAG) -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = className
            }
            isFragmentVisible(SubcategoryFragment.TAG) -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.VISIBLE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = categoryName
            }

            isFragmentVisible(SSubcategoryFragment.TAG) -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.VISIBLE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = subCategoryName
            }

            isFragmentVisible(MapStoreMarketFragment.TAG) -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Selecciona una bodega"
            }

            isFragmentVisible(DetailOrderFragment.TAG) -> {
                llBack.visibility = View.GONE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Historial de pedidos"
            }

            isFragmentVisible(FavoritesFragment.TAG) -> {
                llBack.visibility = View.GONE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Favoritos"
            }

            isFragmentVisible(SaveOrderFragment.TAG) -> {
                llBack.visibility = View.GONE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Carrito"
                val fragment =
                    supportFragmentManager.findFragmentByTag(SaveOrderFragment.TAG) as SaveOrderFragment
                fragment.refreshList()
            }
            isFragmentVisible(TabLayoutFragment.TAG) -> {
                llBack.visibility = View.GONE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Historial de pedidos"
            }

            isFragmentVisible(ProfileFragment.TAG) -> {
                llBack.visibility = View.GONE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Perfil"
            }

            isFragmentVisible(PurchaseDetailsFragment.TAG) -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Carrito"
            }

        }
    }


    private fun checkForVisibleMenuFragment() {
        when (bottom_navigation.selectedItemId) {
            R.id.item_stores -> {
                val fragment = supportFragmentManager.findFragmentById(R.id.frameLayoutFragment)
                if (fragment!!.tag == SupplierFragment.TAG) {
                    finish()
                } else {
                    popBackStack()
                }
            }

            R.id.item_orders -> {
                val fragment =
                    supportFragmentManager.findFragmentById(R.id.frameLayoutOrderCustomerFragment)
                if (fragment!!.tag == TabLayoutFragment.TAG) {
                    finish()
                } else {
                    supportFragmentManager.popBackStackImmediate()
                    val newFragment = supportFragmentManager
                        .findFragmentById(R.id.frameLayoutOrderCustomerFragment);
                    checkForVisibleFragment(newFragment!!.tag!!)
                }
            }

            R.id.item_favorites -> {
                val fragment =
                    supportFragmentManager.findFragmentById(R.id.frameLayoutFavoritesFragment)
                if (fragment!!.tag == FavoritesFragment.TAG) {
                    finish()
                } else {
                    popBackStack()
                }
            }

            R.id.item_shopping -> {
                val fragment =
                    supportFragmentManager.findFragmentById(R.id.frameLayoutPurchaseFragment)
                if (fragment!!.tag == SaveOrderFragment.TAG) {
                    finish()
                } else {
                    supportFragmentManager.popBackStackImmediate()
                    val newFragment = supportFragmentManager
                        .findFragmentById(R.id.frameLayoutPurchaseFragment);
                    checkForVisibleFragment(newFragment!!.tag!!)
                }
            }

            R.id.item_profile -> {
                val fragment =
                    supportFragmentManager.findFragmentById(R.id.frameLayoutProfile)
                if (fragment!!.tag == ProfileFragment.TAG) {
                    finish()
                } else {
                    popBackStack()
                }
            }
        }
    }

    private fun isFragmentVisible(tag: String)
            : Boolean {
        val fragment = supportFragmentManager.findFragmentByTag(tag)
        fragment?.isVisible.let {
            if (it == null) {
                return false
            }
            return it
        }
    }

    /* Design*/

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.item_stores -> {
                    frameLayoutFragment.visibility = View.VISIBLE
                    frameLayoutFavoritesFragment.visibility = View.GONE
                    frameLayoutPurchaseFragment.visibility = View.GONE
                    frameLayoutOrderCustomerFragment.visibility = View.GONE
                    frameLayoutProfile.visibility = View.GONE

                    val fragment = supportFragmentManager
                        .findFragmentById(R.id.frameLayoutFragment);

                    if (fragment == null) {
                        setSupplierFragment()
                    } else {
                        fragment.tag?.let { checkForVisibleFragment(it) }
                    }

                    return@OnNavigationItemSelectedListener true
                }
                R.id.item_favorites -> {
                    frameLayoutFragment.visibility = View.GONE
                    frameLayoutFavoritesFragment.visibility = View.VISIBLE
                    frameLayoutPurchaseFragment.visibility = View.GONE
                    frameLayoutOrderCustomerFragment.visibility = View.GONE
                    frameLayoutProfile.visibility = View.GONE

                    val fragment = supportFragmentManager
                        .findFragmentById(R.id.frameLayoutFavoritesFragment);

                    if (fragment == null) {
                        replaceFavoritesFragments()
                    } else {
                        fragment.tag?.let { checkForVisibleFragment(it) }
                    }

                    return@OnNavigationItemSelectedListener true
                }

                R.id.item_orders -> {

                    if (PreferencesManager.getInstance().getIsInvited()) {
                        getIsInvitedDialog()
                        return@OnNavigationItemSelectedListener false
                    } else {
                        frameLayoutFragment.visibility = View.GONE
                        frameLayoutFavoritesFragment.visibility = View.GONE
                        frameLayoutPurchaseFragment.visibility = View.GONE
                        frameLayoutOrderCustomerFragment.visibility = View.VISIBLE
                        frameLayoutProfile.visibility = View.GONE

                        val fragment = supportFragmentManager
                            .findFragmentById(R.id.frameLayoutOrderCustomerFragment);
                        replaceCustomerOrdersFragments()

                        /*if (fragment == null) {
                        } else {
                            fragment.tag?.let { checkForVisibleFragment(it) }
                        }*/
                        return@OnNavigationItemSelectedListener true
                    }
                }

                R.id.item_shopping -> {

                    if (PreferencesManager.getInstance().getIsInvited()) {
                        getIsInvitedDialog()
                        return@OnNavigationItemSelectedListener false
                    } else {
                        frameLayoutFragment.visibility = View.GONE
                        frameLayoutFavoritesFragment.visibility = View.GONE
                        frameLayoutPurchaseFragment.visibility = View.VISIBLE
                        frameLayoutOrderCustomerFragment.visibility = View.GONE
                        frameLayoutProfile.visibility = View.GONE

                        val fragment = supportFragmentManager
                            .findFragmentById(R.id.frameLayoutPurchaseFragment);

                        if (fragment == null) {
                            replaceSendOrderFragments()
                        } else {
                            fragment.tag?.let { checkForVisibleFragment(it) }
                        }
                        return@OnNavigationItemSelectedListener true
                    }
                }

                R.id.item_profile -> {
                    if (PreferencesManager.getInstance().getIsInvited()) {
                        getIsInvitedDialog()
                        return@OnNavigationItemSelectedListener false
                    } else {
                        frameLayoutFragment.visibility = View.GONE
                        frameLayoutFavoritesFragment.visibility = View.GONE
                        frameLayoutPurchaseFragment.visibility = View.GONE
                        frameLayoutOrderCustomerFragment.visibility = View.GONE
                        frameLayoutProfile.visibility = View.VISIBLE

                        val fragment = supportFragmentManager
                            .findFragmentById(R.id.frameLayoutProfile);

                        if (fragment == null) {
                            replaceProfileFragment()
                        } else {
                            fragment.tag?.let { checkForVisibleFragment(it) }
                        }
                        return@OnNavigationItemSelectedListener true
                    }
                }
            }
            false
        }

    private fun checkForVisibleFragment(tag: String) {
        when (tag) {
            SupplierFragment.TAG -> {
                llBack.visibility = View.GONE
                tvDirection.visibility = View.VISIBLE
                tvTitle.visibility = View.GONE
                containerSearch.visibility = View.GONE

            }

            StoreFragment.TAG -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                if (typeStore == StoreFragment.STORE) {
                    tvTitle.text = "Selecciona tu bodega más cercana"
                } else {
                    tvTitle.text = "Selecciona tu puesto más cercano"
                }
            }

            ClassFragment.TAG -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                containerSearch.visibility = View.VISIBLE
                tvTitle.text = "Selecciona una categoría"

            }

            CategoryFragment.TAG -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = className
            }
            SubcategoryFragment.TAG -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.VISIBLE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = categoryName
            }

            SSubcategoryFragment.TAG -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.VISIBLE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = subCategoryName
            }

            ProductsFragment.TAG -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.VISIBLE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = subCategoryName

            }

            FavoritesFragment.TAG -> {
                llBack.visibility = View.GONE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Favoritos"
            }

            TabLayoutFragment.TAG -> {
                llBack.visibility = View.GONE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Historial de pedidos"

            }

            SaveOrderFragment.TAG -> {
                llBack.visibility = View.GONE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Carrito"
                val fragment = supportFragmentManager.findFragmentByTag(tag) as SaveOrderFragment
                fragment.refreshList()
            }

            DetailOrderFragment.TAG -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Detalle del pedido"
            }

            PurchaseDetailsFragment.TAG -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Carrito"
            }

            ProfileFragment.TAG -> {
                llBack.visibility = View.GONE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Perfil"
            }

            MapStoreMarketFragment.TAG -> {
                llBack.visibility = View.VISIBLE
                tvDirection.visibility = View.GONE
                ivChangeLocation.visibility = View.GONE
                containerSearch.visibility = View.GONE
                tvTitle.visibility = View.VISIBLE
                tvTitle.text = "Selecciona"
            }
        }
    }

    private fun getIsInvitedDialog() {
        val dialog = Dialog(this)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.layout_dialog_is_invited)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val btnCancel = dialog.findViewById(R.id.btnCancel) as Button
        val btnLogin = dialog.findViewById(R.id.btnLogin) as Button

        btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        btnLogin.setOnClickListener {
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
        dialog.show()
    }

    private fun animateLoadingText() {
        if (loadingDots == null) {
            loadingDots =
                LoadingDots(dots)
            loadingDots?.animateLoadingText()
        }
    }

    private fun animateLoadingImage() {
        if (loadingAnimationImageView == null) {
            loadingAnimationImageView =
                LoadingAnimationImageView(
                    ivShrimp
                )
            loadingAnimationImageView?.startAnimation()
        }
    }

    override fun showLoading() {
        layoutLoading.visibility = View.VISIBLE
        animateLoadingText()
        animateLoadingImage()
        bottom_navigation.visibility = View.GONE
    }

    override fun hideLoading() {
        layoutLoading.visibility = View.GONE
        bottom_navigation.visibility = View.VISIBLE
    }

    override fun onReadyForSpeech(p0: Bundle?) {
        TODO("Not yet implemented")
    }

    override fun onRmsChanged(p0: Float) {
        TODO("Not yet implemented")
    }

    override fun onBufferReceived(p0: ByteArray?) {
        TODO("Not yet implemented")
    }

    override fun onPartialResults(p0: Bundle?) {
        TODO("Not yet implemented")
    }

    override fun onEvent(p0: Int, p1: Bundle?) {
        TODO("Not yet implemented")
    }

    override fun onBeginningOfSpeech() {
        TODO("Not yet implemented")
    }

    override fun onEndOfSpeech() {
        TODO("Not yet implemented")
    }

    override fun onError(p0: Int) {
        TODO("Not yet implemented")
    }

    override fun onResults(p0: Bundle?) {
        val matches =
            p0!!.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)//getting all the matches
        //displaying the first match
        if (matches != null)
            Toast.makeText(applicationContext, matches[0], Toast.LENGTH_SHORT).show()
        //  editText.setText(matches[0])
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        // Updates button states if new while in use location is added to SharedPreferences.
        if (key == SharedPreferenceUtil.KEY_FOREGROUND_ENABLED) {
        }
    }

    // TODO: Step 1.0, Review Permissions: Method checks if permissions approved.
    private fun foregroundPermissionApproved(): Boolean {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    // TODO: Step 1.0, Review Permissions: Method requests permissions.
    private fun requestForegroundPermissions() {
        val provideRationale = foregroundPermissionApproved()

        // If the user denied a previous request, but didn't check "Don't ask again", provide
        // additional rationale.
        if (provideRationale) {
            Snackbar.make(
                mainView,
                R.string.permission_rationale,
                Snackbar.LENGTH_LONG
            )
                .setAction(R.string.ok) {
                    // Request permission
                    ActivityCompat.requestPermissions(
                        this@MainActivity,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE
                    )
                }
                .show()
        } else {
            ActivityCompat.requestPermissions(
                this@MainActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE
            )
        }
    }


    private fun logResultsToScreen(output: String) {
        //val outputWithPreviousLogs = "$output\n${outputTextView.text}"
      //  outputTextView.text = outputWithPreviousLogs
        Toast.makeText(this, output , Toast.LENGTH_SHORT).show()
    }

}

