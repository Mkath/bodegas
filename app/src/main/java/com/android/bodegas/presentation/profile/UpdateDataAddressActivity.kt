package com.android.bodegas.presentation.profile

import android.Manifest
import android.content.Context
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.presentation.BaseActivity
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import kotlinx.android.synthetic.main.activity_update_data_address.*
import java.lang.reflect.InvocationTargetException
import java.util.*

class UpdateDataAddressActivity : BaseActivity(), OnMapReadyCallback, PlaceSelectionListener,
    GoogleMap.OnMapClickListener {

    private var registerBody = PreferencesManager.getInstance().getBodyUpdateUser()

    var googleMap: GoogleMap? = null
    lateinit var latLng: LatLng

    lateinit var mLocationRequest: LocationRequest
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    lateinit var mLocationCallback: LocationCallback
    lateinit var mSettingsClient: SettingsClient
    lateinit var mLocationSettingsRequest: LocationSettingsRequest
    private val REQUEST_CHECK_SETTINGS = 0x1
    lateinit var location: Location
    val REQUEST_LOCATION = 1011
    lateinit var mapFragment: SupportMapFragment
    var latitudeSelected: String = ""
    var longitudeSelected: String = ""
    var directionSelected: String = ""
    var autocompleteFragment = AutocompleteSupportFragment()
    lateinit var editText: EditText
    //  var location: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_data_address)
        //   initLocation()

        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, getString(R.string.google_maps_key_ruben))
        }

        autocompleteFragment =
            supportFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment

        autocompleteFragment!!.setPlaceFields(
            listOf(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.LAT_LNG,
                Place.Field.ADDRESS,
                Place.Field.ADDRESS_COMPONENTS
            )
        )
        autocompleteFragment.setCountry("PE")
        autocompleteFragment.setOnPlaceSelectedListener(this)
        editText =
            autocompleteFragment.view!!.findViewById(R.id.places_autocomplete_search_input) as EditText
        editText.textSize = 14.0f

        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        goToReferences()
        initLocation()
    }

    override fun onPlaceSelected(p0: Place) {
        latLng = LatLng(p0.latLng!!.latitude, p0!!.latLng!!.longitude)

        mapFragment.getMapAsync(this)
        assignToMap()
        latitudeSelected = p0.latLng!!.latitude.toString()
        longitudeSelected = p0.latLng!!.longitude.toString()
        getAddress(latLng)
    }

    override fun onError(p0: Status) {
        Toast.makeText(applicationContext, "" + p0.toString(), Toast.LENGTH_LONG).show();
    }

    private fun assignToMap() {
        googleMap?.clear()
        latitudeSelected = latLng.latitude.toString()
        longitudeSelected = latLng.longitude.toString()
        getAddress(latLng)

        val options = MarkerOptions()
            .position(latLng)
            .title("Yo")
        googleMap?.apply {
            addMarker(options)
            moveCamera(CameraUpdateFactory.newLatLng(latLng))
            animateCamera(CameraUpdateFactory.newLatLng(latLng))
        }
    }

    private fun firstAssignToMap() {
        googleMap?.clear()
        latitudeSelected = latLng.latitude.toString()
        longitudeSelected = latLng.longitude.toString()
        getAddress(latLng)

        val options = MarkerOptions()
            .position(latLng)
            .title("Yo")
        googleMap?.apply {
            addMarker(options)
            moveCamera(CameraUpdateFactory.newLatLng(latLng))
            animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
        }
    }

    private fun getLastLocation() {
        try {
            mFusedLocationClient.lastLocation?.addOnCompleteListener { task ->
                if (task.isSuccessful && task.result != null) {
                    location = task.result!!
                    latLng = LatLng(location.latitude, location.longitude)
                    firstAssignToMap()
                } else {
                    Log.w("Location", "Error al obtener su ubicación.")
                }
            }
        } catch (unlikely: SecurityException) {
            Log.e("Location", "Permisos de ubicación no encontrados.$unlikely")
        }
    }

    private fun initLocation() {
        try {
            mFusedLocationClient =
                LocationServices.getFusedLocationProviderClient(this@UpdateDataAddressActivity)
            getLastLocation()
            try {
                mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                    .addOnSuccessListener(
                        this,
                        object : OnSuccessListener<LocationSettingsResponse> {
                            override fun onSuccess(p0: LocationSettingsResponse?) {
                                mFusedLocationClient.requestLocationUpdates(
                                    mLocationRequest,
                                    mLocationCallback, Looper.myLooper()
                                );
                            }
                        }).addOnFailureListener(this, object : OnFailureListener {
                        override fun onFailure(p0: java.lang.Exception) {
                            val statusCode = (p0 as ApiException).getStatusCode();
                            when (statusCode) {
                                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                                    Log.i(
                                        "Location",
                                        "Location settings are not satisfied. Attempting to upgrade " +
                                                "location settings "
                                    );
                                    try {
                                        // Show the dialog by calling startResolutionForResult(), and check the
                                        // result in onActivityResult().
                                        val rae = p0 as ResolvableApiException
                                        rae.startResolutionForResult(
                                            this@UpdateDataAddressActivity,
                                            REQUEST_CHECK_SETTINGS
                                        );
                                    } catch (sie: IntentSender.SendIntentException) {
                                        Log.i(
                                            "Location",
                                            "PendingIntent unable to execute request."
                                        );
                                    }
                                }

                                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE ->
                                    Toast.makeText(
                                        this@UpdateDataAddressActivity,
                                        "Location settings are inadequate, and cannot be \"+\n" +
                                                "                                    \"fixed here. Fix in Settings.",
                                        Toast.LENGTH_LONG
                                    ).show();
                            }
                        }
                    })
            } catch (unlikely: SecurityException) {
                Log.e(
                    "Location",
                    "Lost location permission. Could not request updates. " + unlikely
                )
            }
        } catch (e: SecurityException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onMapReady(p0: GoogleMap?) {
        googleMap = p0
        googleMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
        googleMap?.uiSettings?.apply {
            isCompassEnabled = true
            isMyLocationButtonEnabled = true

        }
        googleMap?.setOnMapClickListener(this)
    }

    override fun hideKeyboard() {
        try {
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initLocation()
            } else {
                Toast.makeText(
                    this@UpdateDataAddressActivity,
                    "Permiso denegado",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }
        }
    }

    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_LOCATION
        )
    }

    override fun onResume() {
        super.onResume()
        //loadDataFromLocalStorage()
        if (checkPermissions()) {
            //      initLocation()
        } else {
            requestPermissions();
        }
    }

    private fun goToReferences() {
        btnNext.setOnClickListener {
            if (validationCampos()) {
                saveDataInLocalStorage()
                comeBackToPrevious()
            }

        }
    }

    private fun comeBackToPrevious() {
        nextActivity(null, UpdateDataUserActivity::class.java, true)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        comeBack()
    }

    private fun comeBack() {
        nextActivity(null, UpdateDataUserActivity::class.java, true)
    }

    private fun validationCampos(): Boolean {
        return (latitudeSelected.isNotEmpty() && longitudeSelected.isNotEmpty() && directionSelected.isNotEmpty())
    }

    private fun saveDataInLocalStorage() {
        Log.e("Location", "Direccion seleccionada: "+ directionSelected)
        Log.e("Location", "Latitud seleccionada: "+ latitudeSelected)
        Log.e("Location", "Longitud seleccionada: "+ longitudeSelected)

          registerBody.latitude = latitudeSelected.toDouble()
          registerBody.longitude = longitudeSelected.toDouble()
        registerBody.address = directionSelected
        PreferencesManager.getInstance().saveBodyUpdateUser(registerBody)
    }

    override fun onMapClick(p0: LatLng?) {
        // Toast.makeText(applicationContext,"EN click map: "+p0!!.latitude+p0!!.longitude,Toast.LENGTH_LONG).show()
        latLng = LatLng(p0!!.latitude, p0!!.longitude)
        mapFragment.getMapAsync(this)
        assignToMap()
        latitudeSelected = latLng.latitude.toString()
        longitudeSelected = latLng.longitude.toString()
        getAddress(latLng)
        //direccionSelected =  addresses[0].getAddressLine(0)
    }

    private fun getAddress(p0: LatLng?) {
        try {
            val geocoder = Geocoder(applicationContext, Locale.getDefault());
            val addresses: List<Address>
            addresses = geocoder.getFromLocation(
                p0!!.latitude,
                p0!!.longitude,
                1
            )
            directionSelected = addresses[0].getAddressLine(0)
            //direccion_seleccionada.text = direccionSelected
            editText.setText(directionSelected)

        } catch (e: InvocationTargetException) {
            Toast.makeText(
                applicationContext,
                "Por favor seleccione una dirección",
                Toast.LENGTH_SHORT
            ).show()
        } catch (e: java.lang.Exception) {
            Toast.makeText(
                applicationContext,
                "Por favor seleccione una dirección",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

}