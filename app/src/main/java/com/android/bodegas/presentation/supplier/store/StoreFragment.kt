package com.android.bodegas.presentation.supplier.store

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.supplier.SupplierViewModel
import com.android.bodegas.presentation.supplier.adapter.StoreAdapter
import com.android.bodegas.presentation.supplier.adapter.SupplierAdapter
import com.android.bodegas.presentation.supplier.adapter.SupplierViewHolder
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_market_list.*
import kotlinx.android.synthetic.main.fragment_suppliers.*
import kotlinx.android.synthetic.main.fragment_suppliers.rvStores
import org.koin.androidx.viewmodel.ext.android.viewModel

const val TYPE_STORE = "typeStore"
const val LATITUDE = "latitude"
const val LONGITUDE = "longitude"

class StoreFragment : Fragment(), SupplierViewHolder.ViewHolderListener {

    companion object {
        const val TAG = "StoreFragment"
        const val STORE = 1
        const val STORE_MARKET = 2

        @JvmStatic
        fun newInstance(type: Int, latitude: Double, longitude: Double) = StoreFragment().apply {
            arguments = Bundle().apply {
                putInt(TYPE_STORE, type)
                putDouble(LATITUDE, latitude)
                putDouble(LONGITUDE, longitude)
            }
        }
    }

    private val storesViewModel: SupplierViewModel by viewModel()
    private var storesViewList: MutableList<SupplierView> = mutableListOf()
    private var listener: OnFragmentInteractionListener? = null
    private var adapter = StoreAdapter(this)
    private var latitude = 0.0
    private var longitude = 0.0
    private var type = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        type = arguments?.getInt(TYPE_STORE)!!
        latitude = arguments?.getDouble(LATITUDE)!!
        longitude = arguments?.getDouble(LONGITUDE)!!
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onResume() {
        super.onResume()
        observeListSuppliers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_market_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setSwipeRefreshLayout()
        ctlLocation.setOnClickListener {
            listener!!.replaceMapStoresFragment(type, latitude, longitude)
        }
    }

    private fun observeListSuppliers() {
        storesViewModel.listSuppliers.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener?.hideLoading()
                    swipe.isRefreshing = false
                    storesViewList = it.data.toMutableList()
                    setRecyclerView()
                }
                Status.ERROR -> {
                    listener?.hideLoading()
                    Snackbar.make(
                        supplierView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                    listener?.showLoading()
                }
            }
        })
        storesViewModel.getSuppliersByType(type, latitude.toString(), longitude.toString(), 8000)
    }

    private fun setRecyclerView() {
        val linearLayoutManagerStores = LinearLayoutManager(context)
        rvStores.layoutManager = linearLayoutManagerStores
        rvStores.adapter = adapter
        adapter.setStoreList(storesViewList)
    }

    override fun onClick(position: Int, establishmentType:String) {
        val storeView = storesViewList[position]
        listener?.replaceClassFragment(storeView)
    }

    private fun setSwipeRefreshLayout() {
        swipe.setOnRefreshListener {
            storesViewModel.getSuppliersByType(
                type,
                latitude.toString(),
                longitude.toString(),
                8000
            )
        }
    }

    interface OnFragmentInteractionListener {
        fun replaceClassFragment(establishment: SupplierView)
        fun replaceMapStoresFragment(type: Int, latitude: Double, longitude: Double)
        fun showLoading()
        fun hideLoading()
    }
}