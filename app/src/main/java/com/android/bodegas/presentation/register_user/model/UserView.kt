package com.android.bodegas.presentation.register_user.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserView(
    val customerId: Int,
    val creationDate: String,
    val creationUser: String,
    val departament: String,
    val district: String,
    val dni: String,
    val email: String,
    val address: String,
    val lastNameMaternal: String,
    val lastNamePaternal: String,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val phoneNumber: String,
    val province: String,
    val ruc: String,
    val status: String,
    val urbanization: String,
    var pathImage:String ? = ""
) : Parcelable