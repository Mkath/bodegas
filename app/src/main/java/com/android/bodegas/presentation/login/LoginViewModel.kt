package com.android.bodegas.presentation.login

import android.util.Log
import androidx.lifecycle.*
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.domain.user.login.LoginTokenUser
import com.android.bodegas.domain.user.login.LoginUser
import com.android.bodegas.domain.user.update.RecoveryPassword
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.login.mapper.UserLoginTokenViewMapper
import com.android.bodegas.presentation.login.model.UserLoginTokenView
import com.android.bodegas.presentation.register_user.mapper.UserViewMapper
import kotlinx.coroutines.launch

class LoginViewModel constructor(
    private val loginUser: LoginUser,
    private val userViewMapper: UserViewMapper,
    private val recoveryPassword: RecoveryPassword,
    private val userLoginTokenViewMapper: UserLoginTokenViewMapper,
    private val loginUserToken: LoginTokenUser
    ) :
    ViewModel() {

    private val _loginCodeToken = MutableLiveData<Resource<UserLoginTokenView>>()
    val loginCodeToken: LiveData<Resource<UserLoginTokenView>> = _loginCodeToken

    private val _loginCode = MutableLiveData<Resource<User>>()
    val loginCode: LiveData<Resource<User>> = _loginCode

    private val _sendEmail = MutableLiveData<Resource<Boolean>>()
    val sendEmail : LiveData<Resource<Boolean>> = _sendEmail

    fun getDataUser(customerId: Int) {

        _loginCode.value = Resource(Status.LOADING, User(0,"", "", "", "","","","","",
        "",0.0,0.0,"","","","","","","") , "")
        viewModelScope.launch {

            val loginResult = loginUser.loginUser(customerId)
            Log.e("error", "valor de loginResult: "+loginResult.message)
            if (loginResult.status == Status.SUCCESS) {

                PreferencesManager.getInstance().setUser(userViewMapper.mapToView(loginResult.data))
                _loginCode.value = Resource(loginResult.status, loginResult.data, loginResult.message)
            }else{
                _loginCode.value = Resource(Status.ERROR, loginResult.data, loginResult.message)
            }
        }
    }

    fun sendEmailToRecoveryAccount(body: ForgetPassDTO) {
        _sendEmail.value = Resource(Status.LOADING, false, "")
        viewModelScope.launch {
            val updateResult = recoveryPassword.recoveryPassword(body)
            if(updateResult.status == Status.SUCCESS){
                if(updateResult.data){
                    _sendEmail.value = Resource(Status.SUCCESS, updateResult.data, updateResult.message)
                }
                else{
                    _sendEmail.value = Resource(Status.ERROR, updateResult.data, "")
                }
            }
            else{
                _sendEmail.value = Resource(Status.ERROR, updateResult.data, "")
            }
        }
    }

    fun loginUserToken(email: String, password: String) {
        _loginCodeToken.value = Resource(Status.LOADING, UserLoginTokenView( "", ""),"")

        viewModelScope.launch {
            val loginResult = loginUserToken.loginUserToken(email, password)
            val userView = userLoginTokenViewMapper.mapToView(loginResult.data)
            if (loginResult.status == Status.SUCCESS) {
                PreferencesManager.getInstance().setCustomerLoginTokenSelected(userView)
                _loginCodeToken.value = Resource(loginResult.status, userView, "")
            } else {
                _loginCodeToken.value = Resource(Status.ERROR, userView, loginResult.message)
            }
        }
    }

    fun loginUserTokenAndGetData(email: String, password: String) {
        viewModelScope.launch {
            val loginResult = loginUserToken.loginUserToken(email, password)
            val userView = userLoginTokenViewMapper.mapToView(loginResult.data)
            if (loginResult.status == Status.SUCCESS) {
                PreferencesManager.getInstance().setCustomerLoginTokenSelected(userView)
                getDataUser(PreferencesManager.getInstance().getCustomerLoginTokenSelected().idEntity.toInt())
            } else {
            }
        }
    }
}