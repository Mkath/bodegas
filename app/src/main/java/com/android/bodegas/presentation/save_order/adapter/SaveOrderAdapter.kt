package com.android.bodegas.presentation.save_order.adapter


import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.android.bodegas.R
import com.android.bodegas.presentation.save_order.model.SaveOrderView
import com.bumptech.glide.Glide


class SaveOrderAdapter(
    private val viewHolderListener: SaveOrderViewHolder.ViewHolderListener
) : RecyclerView.Adapter<SaveOrderViewHolder>() {

    val ordersList = mutableListOf<SaveOrderView>()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SaveOrderViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_save_order, parent, false)
        context = parent.context
        return SaveOrderViewHolder(view, viewHolderListener)
    }

    override fun getItemCount(): Int {
        return ordersList.size
    }

    override fun onBindViewHolder(holder: SaveOrderViewHolder, position: Int) {
        val product = ordersList[position]
        holder.bind(product)
        if (product.imageProduct!!.isNotEmpty()) {

            val circularProgressDrawable = CircularProgressDrawable(context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()

            Glide.with(context)  // this
                .load(product.imageProduct)
                .placeholder(circularProgressDrawable)
                .error(context.getDrawable(R.drawable.ic_image_black_24dp))
                .into(holder.imageProduct)
        }
        /*  if (store.hasDelivery) {
            holder.storeDelivery.visibility = View.VISIBLE
        } else {
            holder.storeDelivery.visibility = View.GONE
        }*/

     /*   if (product.imageProduct != "") {
            Glide.with(context).load(product.imageProduct).into(holder.imageProduct)
        }*/
    }


    fun setProductsList(poolList: List<SaveOrderView>) {
        this.ordersList.clear()
        this.ordersList.addAll(poolList)
        notifyDataSetChanged()
    }


    fun removeAt(position: Int) {
        ordersList.removeAt(position)
        notifyItemRemoved(position)
        notifyDataSetChanged()
    }


}