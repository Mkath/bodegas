package com.android.bodegas.presentation.register_user.mapper

import com.android.bodegas.domain.department.Department
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.register_user.model.DepartmentView

class DepartmentViewMapper: Mapper<DepartmentView, Department> {
    override fun mapToView(type: Department): DepartmentView {
        return DepartmentView(
            type.id,
            type.name
        )
    }
}