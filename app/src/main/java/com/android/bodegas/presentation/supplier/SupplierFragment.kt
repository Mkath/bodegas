package com.android.bodegas.presentation.supplier

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.supplier.adapter.SupplierAdapter
import com.android.bodegas.presentation.supplier.adapter.SupplierViewHolder
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.android.bodegas.presentation.supplier.store.StoreFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_suppliers.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class SupplierFragment : Fragment(), SupplierViewHolder.ViewHolderListener {

    companion object {
        const val TAG = "SupplierFragment"

        @JvmStatic
        fun newInstance() =
            SupplierFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

    private val storesViewModel: SupplierViewModel by viewModel()
    private var storesViewList: MutableList<SupplierView> = mutableListOf()
    private var finalStoresViewList: MutableList<SupplierView> = mutableListOf()
    private var storesMarketViewList: MutableList<SupplierView> = mutableListOf()
    private var finalStoresMarketViewList: MutableList<SupplierView> = mutableListOf()
    private var listener: OnFragmentInteractionListener? = null
    private var adapter = SupplierAdapter(this)
    private var adapterMarket = SupplierAdapter(this)
    private var actualLatitude = PreferencesManager.getInstance().getUser().latitude
    private var actualLongitude = PreferencesManager.getInstance().getUser().longitude

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_suppliers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeListSuppliers()
        observeListSuppliersMarket()
        setOnClickListener()
    }

    private fun observeListSuppliers() {
        storesViewModel.listSuppliers.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener?.hideLoading()
                    containerStores.visibility = View.VISIBLE
                    storesViewList = it.data.toMutableList()
                    if (storesViewList.isNotEmpty()) {
                        if(storesViewList.size > 4){
                            finalStoresViewList = storesViewList.subList(0, 4)
                            setRecyclerView(finalStoresViewList)
                        }else{
                            setRecyclerView(storesViewList)
                        }
                        rvStores.visibility = View.VISIBLE
                        emptyStore.visibility = View.GONE
                        tvSeeAllStores.visibility = View.VISIBLE
                    } else {
                        rvStores.visibility = View.INVISIBLE
                        emptyStore.visibility = View.VISIBLE
                        tvSeeAllStores.visibility = View.INVISIBLE
                    }
                }
                Status.ERROR -> {
                    listener?.hideLoading()
                    containerStores.visibility = View.VISIBLE
                    Snackbar.make(
                        supplierView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                    rvStores.visibility = View.INVISIBLE
                    emptyStore.visibility = View.VISIBLE
                    tvSeeAllStores.visibility = View.INVISIBLE
                }
                Status.LOADING -> {
                    containerStores.visibility = View.GONE
                    listener?.showLoading()
                }
            }
        })
       // storesViewModel.getSuppliersByType(1, actualLatitude.toString(), actualLongitude.toString(), 1000)
    }

    private fun observeListSuppliersMarket() {
        storesViewModel.listSuppliersMarket.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    containerStoresMarket.visibility = View.VISIBLE
                    storesMarketViewList = it.data.toMutableList()
                    if (storesMarketViewList.isNotEmpty()) {
                        if(storesMarketViewList.size > 4){
                            finalStoresMarketViewList = storesMarketViewList.subList(0, 4)
                            setRecyclerViewMarket(finalStoresMarketViewList)
                        }else{
                            setRecyclerViewMarket(storesMarketViewList)
                        }
                        rvStoresMarket.visibility = View.VISIBLE
                        emptyStoreMarket.visibility = View.GONE
                        tvSeeAllStoreMarkets.visibility = View.VISIBLE
                    } else {
                        rvStoresMarket.visibility = View.INVISIBLE
                        emptyStoreMarket.visibility = View.VISIBLE
                        tvSeeAllStoreMarkets.visibility = View.INVISIBLE
                    }
                }
                Status.ERROR -> {
                    listener?.hideLoading()
                    containerStoresMarket.visibility = View.VISIBLE

                    Snackbar.make(
                        supplierView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                    rvStoresMarket.visibility = View.INVISIBLE
                    tvSeeAllStoreMarkets.visibility = View.INVISIBLE
                    emptyStoreMarket.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    containerStoresMarket.visibility = View.GONE
                    listener?.showLoading()
                }
            }
        })
        //storesViewModel.getSuppliersMarket(2, actualLatitude.toString(), actualLongitude.toString(), 1000)
    }

    private fun setRecyclerView(finalList: List<SupplierView>) {
        val linearLayoutManagerStores = LinearLayoutManager(
            context,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        rvStores.layoutManager = linearLayoutManagerStores
        rvStores.adapter = adapter

        adapter.setStoreList(finalList)
    }

    private fun setRecyclerViewMarket(finalMarketList: List<SupplierView>) {

        val linearLayoutManagerStoresMarket = LinearLayoutManager(
            context,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        rvStoresMarket.layoutManager = linearLayoutManagerStoresMarket
        rvStoresMarket.adapter = adapterMarket
        adapterMarket.setStoreList(finalMarketList)
    }


    private fun setOnClickListener() {
        tvSeeAllStores.setOnClickListener {
            listener?.replaceStoresFragment(StoreFragment.STORE, actualLatitude, actualLongitude)
        }

        tvSeeAllStoreMarkets.setOnClickListener {
            listener?.replaceStoresFragment(StoreFragment.STORE_MARKET, actualLatitude, actualLongitude)
        }
    }

    override fun onClick(position: Int, establishmentType:String) {
        if(establishmentType != "Puesto de Mercado"){
            val storeView = storesViewList[position]
            listener?.replaceClassFragment(storeView)
        }else{
            val storeViewMarket = storesMarketViewList[position]
            listener?.replaceClassFragment(storeViewMarket)
        }

    }

    fun refreshListByUbication(latitude: Double, longitude: Double) {
        actualLatitude = latitude
        actualLongitude = longitude
        storesViewModel.getSuppliersByType(1, latitude.toString(), longitude.toString(), 8000)
        storesViewModel.getSuppliersMarket(2, latitude.toString(), longitude.toString(), 8000)
    }

    interface OnFragmentInteractionListener {
        fun replaceClassFragment(establishment: SupplierView)
        fun replaceStoresFragment(type: Int, latitude: Double, longitude: Double)
        fun showLoading()
        fun hideLoading()
    }
}