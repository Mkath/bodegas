package com.android.bodegas.presentation.subsubcategory.mapper

import com.android.bodegas.domain.sub_sub_category.SSubCategory
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.subsubcategory.model.SSubcategoryView

class SSubcategoryViewMapper  : Mapper<SSubcategoryView, SSubCategory> {
    override fun mapToView(type: SSubCategory): SSubcategoryView {
        return SSubcategoryView(
            type.id,
            type.description,
            type.name,
            type.pathImage
        )
    }
}

