package com.android.bodegas.presentation

interface Mapper<out V, in D> {

    fun mapToView(type: D): V

}