package com.android.bodegas.presentation.detail_orders

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager

import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.orders.CustomerOrdersViewModel
import com.android.bodegas.presentation.orders.adapter.OrderDetailAdapter
import com.android.bodegas.presentation.orders.adapter.OrderDetailViewHolder
import com.android.bodegas.presentation.orders.model.CustomerOrderView
import com.android.bodegas.presentation.orders.model.OrdersItemView
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.android.bodegas.presentation.allPaymentMethods.GetAllPaymentMethodsViewModel
import com.android.bodegasadmin.presentation.allPaymentMethods.model.AllPaymentMethodsView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_detalle_pedidos.*
import kotlinx.android.synthetic.main.fragment_save_order.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.math.BigDecimal

private const val CUSTOMER_ORDER = "orderDetails"

class DetailOrderFragment : Fragment(), OrderDetailViewHolder.ViewHolderListener {

    private lateinit var customerOrder: CustomerOrderView
    private var adapter = OrderDetailAdapter(this)
    private val detailOrdersViewModel : DetailOrderViewModel by viewModel()
    private val getAllPaymentMethodsViewModel: GetAllPaymentMethodsViewModel by viewModel()
    private val customerOrdersViewModel: CustomerOrdersViewModel by sharedViewModel()
    private var listener: OnFragmentInteractionListener? = null
    private var DELIVERY="Delivery"
    private var paymentMethodsViewList = mutableListOf<AllPaymentMethodsView>()
    private var paymentMethodsHashMap : HashMap<Int, String>  = HashMap<Int, String>()
    private var PAYMENT_METHOD_CASH = "Efectivo"
    var inputTotalVuelto: String = ""
    private var establishmentId_duplicate: Int = 0
    private var orderId_duplicate: Int = 0

    private val duplicateOrderViewModel: DuplicateOrderViewModel by viewModel()
    private var orderIdx: Int = 0
    private lateinit var customerOrderViewReSend: CustomerOrderView
    private  var customerOrderViewReSendFiltered = listOf<OrdersItemView>()
    private  var flagTodosLosProductosDisponiblesParaReplica  = false

    companion object {
        const val TAG = "DetailOrderFragment"

        @JvmStatic
        fun newInstance(customerOrderView: CustomerOrderView) =
            DetailOrderFragment()
                .apply {
                    arguments = Bundle().apply {
                        putParcelable(CUSTOMER_ORDER, customerOrderView)
                    }
                }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            customerOrder = it.getParcelable(CUSTOMER_ORDER)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detalle_pedidos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setView()
        setRecyclerView()
        setOnClickListener()
        observeValueTotal()
        observerOrdersByUser()
        observerUpdateOrder()
        observerDetail1()
    }

    private fun setView() {
        getMethodsForPayment()
        boxStatus.text = String.format("%s %s", getString(R.string.order_detail_status), customerOrder.status)
        inputOrderDate.text = customerOrder.orderDate
        inputNombreSupplier.text = customerOrder.establishment.establishmentName
        inputTotal.text = String.format("%s %s", "s/ ", customerOrder.total)
        boxDelivery.text = customerOrder.deliveryType
        boxDelivery.isEnabled = customerOrder.deliveryType != "Recojo en tienda"
        var aux:BigDecimal = customerOrder.customerAmount.toBigDecimal() - customerOrder.total.toBigDecimal()
        var aux2:BigDecimal = customerOrder.customerAmount.toBigDecimal() - customerOrder.total.toBigDecimal()

        aux = String.format("%.2f", aux).toBigDecimal()
        inputTotalVuelto =String.format("%s %s", "s/ ", aux.toString())

        if(aux2.compareTo(BigDecimal.ZERO)  <0) {
            inputTotalVuelto = inputTotalVuelto + " El vuelto negativo se origina por el 'Costo por envío'"
        }

        //aux = String.format("%.2f", aux).toBigDecimal()
        //inputTotalVuelto  =inputTotalVuelto1
        if(customerOrder.deliveryType == DELIVERY ) {
            inputChargeForDelivery.visibility = View.VISIBLE
            titleChargeForDelivery.visibility = View.VISIBLE
            if (customerOrder.deliveryCharge != "0.00"){
                inputChargeForDelivery.text = String.format("%s %s", "s/ ", customerOrder.deliveryCharge)
            }

            imDelivery.visibility = View.VISIBLE
            inputShipping.visibility = View.VISIBLE
            titleShiping.visibility = View.VISIBLE
            inputShipping.text = customerOrder.shippingDateFrom
        }else{
            imDelivery.visibility = View.GONE
            inputChargeForDelivery.visibility = View.GONE
            titleChargeForDelivery.visibility = View.GONE
            inputShipping.visibility = View.GONE
            titleShiping.visibility = View.GONE
        }


        boxStatus.text = String.format("%s %s", getString(R.string.order_detail_status), customerOrder.status)
        inputOrderDateUpdate.text = customerOrder.udpateUser


        if(customerOrder.status != "Informado"){
            btnCancel.visibility = View.INVISIBLE
        }
        if(customerOrder.status == "Entregado"){
            btnReSend.visibility = View.VISIBLE
        }else{
            btnReSend.visibility = View.INVISIBLE
        }
    }

    private fun setOnClickListener() {
        btnCancel.setOnClickListener {
            detailOrdersViewModel.updateStateOrder(
                customerOrder.orderId.toInt(), "N", customerOrder.orderDetails)
        }

        btnReSend.setOnClickListener {

            orderIdx = customerOrder.orderId.toInt()
            customerOrderViewReSend = customerOrder
            duplicateOrderViewModel.filterOrder(customerOrder.establishment.establishmentId , orderIdx)
        }
    }

    fun keepWithReOrder() {
        val dialogBuilder = AlertDialog.Builder(context!!)
        dialogBuilder
            .setMessage("¿Desea realizar un nuevo pedido de esta orden? Los productos disponibles se agregarán al carrito.")
            .setCancelable(false)
            .setPositiveButton("ACEPTAR", DialogInterface.OnClickListener {
                    dialog, id -> sendNewOrder()
            })
            .setNegativeButton("CANCELAR", DialogInterface.OnClickListener {
                    dialog, id -> dialog.cancel()
            })

        val alert = dialogBuilder.create()
        alert.setTitle("PEDIDOS")
        alert.show()
    }

    private fun observerDetail1() {
        duplicateOrderViewModel.duplicateOrder.observe(viewLifecycleOwner, Observer {

            flagTodosLosProductosDisponiblesParaReplica = it.data.flagAllAvailableProducts

            //aqui hacemos el filtro
            customerOrderViewReSendFiltered = listOf<OrdersItemView>() //limpiamos
            val newList = mutableListOf<OrdersItemView>()

            for(a in it.data.orderDetailRequest) {
                Log.e("error", "################################ a: "+a)
                for ( b in customerOrderViewReSend.orderDetails) {
                    if (a.storeProductId.toString() == b.storeProductStoreProductId) {
                        Log.e("error", "################################ b: "+b)
                        b.orderDetailSubTotal = a.subTotal.toString()
                        b.orderDetailPrice = a.price.toString()
                        b.productTemplateName = a.description
                        newList.add(b)
                        Log.e("error", "--------------------------------------------- ")
                    }
                }
            }
            customerOrderViewReSendFiltered = newList

            for(d in customerOrderViewReSendFiltered) {
                Log.e("error", "$$$$$$$$$$$$$$$$$$$ "+d)
            }
            keepWithReOrder()
        })
    }

    private fun sendNewOrder() {
        detailOrdersViewModel.getOrdersByUser(customerOrder.establishment.establishmentId, customerOrder.orderId.toInt())
        establishmentId_duplicate = customerOrder.establishment.establishmentId
        orderId_duplicate = customerOrder.orderId.toInt()
    }

    private fun observerOrdersByUser(){
        detailOrdersViewModel.orderByUser.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if(customerOrderViewReSendFiltered.size >0) {
                        customerOrderViewReSendFiltered.forEach {
                            detailOrdersViewModel.addProductsForOrder(it, establishmentId_duplicate, orderId_duplicate)
                        }
                    }
                    else {
                        Snackbar.make(
                            detailOrderView,
                            "No existen productos disponibles a agregar al carrito de compras.",
                            Snackbar.LENGTH_LONG
                        ).show()
                    }

                }
                Status.ERROR -> {
                    Snackbar.make(
                        detailOrderView,
                        "Usted cuenta con un pedido en su carrito, debe completarlo para iniciar una nueva compra",
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                }
            }
        })

    }

    private fun observeValueTotal() {
        detailOrdersViewModel.totalValue.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    val establishment = SupplierView(
                        customerOrder.establishment.establishmentId,
                        customerOrder.establishment.establishmentName,
                        customerOrder.establishment.establishmentAddress,
                        "",
                        0.00,
                        0.00,
                        customerOrder.establishment.storeScheduleShipping,
                        customerOrder.establishment.storeScheduleOperation,
                        "",
                        "",
                        customerOrder.establishment.hasDelivery,
                        customerOrder.establishment.paymentMethod,
                        customerOrder.establishment.deliveryCharge
                    )
                    PreferencesManager.getInstance().setEstablishmentSelected(establishment)
                    Log.e("error", "---establishmentOrder: "+customerOrder)
                    Log.e("error", "------------------------------------------------------------")
                    Log.e("error", "linea 231 getEstablishmentSelected().deliveryCharge: "+ PreferencesManager.getInstance().getEstablishmentSelected().deliveryCharge)
                    Log.e("error", "-customerOrder.deliveryCharge:: "+customerOrder.deliveryCharge)
                    Log.e("error", "-customerOrder.deliveryCharge:: "+customerOrder.establishment.deliveryCharge)


                    var textByDefault = "Se ha generado el pedido con todos los productos disponibles, puede revisarlo en su carrito de compras"
                    if (!flagTodosLosProductosDisponiblesParaReplica) {
                        textByDefault = "Se ha generado el pedido, no obstante no todos los productos se encuentran disponibles."
                    }
                    Snackbar.make(
                        detailOrderView,
                        textByDefault,
                        Snackbar.LENGTH_SHORT
                    ).show()
                }

                Status.ERROR -> {
                    Snackbar.make(
                        detailOrderView,
                        "Ha ocurrido un error al generar su nuevo pedido",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }

                Status.LOADING -> {
                }
            }
        })
    }

    private fun observerUpdateOrder(){
        detailOrdersViewModel.updateOrder.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener!!.hideLoading()
                    Snackbar.make(
                        detailOrderView,
                        "Orden cancelada con éxito",
                        Snackbar.LENGTH_LONG
                    ).show()
                    customerOrdersViewModel
                        .getCustomerOrdersByCustomerId(PreferencesManager.getInstance().getUser().customerId)
                    listener!!.onBackPressed()
                }
                Status.ERROR -> {
                    listener!!.hideLoading()
                    Snackbar.make(
                        detailOrderView,
                        "Ocurrió un error al cancelar su pedido, intente nuevamente por favor",
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                    listener!!.showLoading()
                }
            }
        })

    }

    private fun setRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        rvCustomerOrdersDetail.layoutManager = linearLayoutManager
        rvCustomerOrdersDetail.adapter = adapter
        adapter.setStoreList(customerOrder.orderDetails)
    }

    override fun onClick(position: Int) {
        Toast.makeText(context, "Clic", Toast.LENGTH_LONG)
    }

    interface OnFragmentInteractionListener {
        fun onBackPressed()
        fun showLoading()
        fun hideLoading()
    }

    private fun getMethodsForPayment() {
        getAllPaymentMethodsViewModel.paymentMethod.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    paymentMethodsViewList = it.data.toMutableList()
                    getDescriptionAndIdOfPaymentMethodsList()
                }
                Status.ERROR -> {
                    Toast.makeText(
                        activity!!.applicationContext,
                        "Ocurrió un error al cargar los métodos de pago",
                        Toast.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                }
            }
        })
        getAllPaymentMethodsViewModel.getPaymentMethods()
    }

    private fun getDescriptionAndIdOfPaymentMethodsList() {
        for(i in paymentMethodsViewList) {
            paymentMethodsHashMap[i.paymentMethodId] = i.description
        }

        var currentPaymentMethod = paymentMethodsHashMap[customerOrder.paymentMethodPaymentMethodId.toInt()]
        if(currentPaymentMethod.toString().equals(PAYMENT_METHOD_CASH)) {
           // inputTotalVuelto.visibility = View.VISIBLE
           // titleTotalVuelo.visibility = View.VISIBLE
            inputPaymentMethod.text= currentPaymentMethod
            tvTitlePayment.visibility = View.VISIBLE
            var paga1 = String.format("%s %s %s", "Paga con:"," s/", customerOrder.customerAmount)
            tvTitlePayment.text = paga1 + " Vuelto: " + inputTotalVuelto
        }
        else
        {
            tvTitlePayment.visibility = View.GONE
            inputPaymentMethod.text= currentPaymentMethod
          //  inputTotalVuelto.visibility = View.GONE
          //  titleTotalVuelo.visibility = View.GONE

        }
    }
}

