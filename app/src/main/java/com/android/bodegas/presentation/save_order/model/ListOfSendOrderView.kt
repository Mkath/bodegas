package com.android.bodegas.presentation.save_order.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ListOfSendOrderView(
    var orders: List<SaveOrderView>
) : Parcelable
