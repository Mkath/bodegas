package com.android.bodegas.presentation.supplier.map_stores

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.supplier.SupplierViewModel
import com.android.bodegas.presentation.supplier.adapter.StoreAdapter
import com.android.bodegas.presentation.supplier.adapter.SupplierAdapter
import com.android.bodegas.presentation.supplier.adapter.SupplierViewHolder
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_suppliers.*
import org.koin.androidx.viewmodel.ext.android.viewModel


const val TYPE_STORE = "typeStore"
const val LATITUDE= "latitude"
const val LONGITUDE = "longitude"

class MapStoreMarketFragment : Fragment(), OnMapReadyCallback,
    SupplierViewHolder.ViewHolderListener {

    private lateinit var mMap: GoogleMap
    private val storesViewModel: SupplierViewModel by viewModel()
    private var storesViewList: MutableList<SupplierView> = mutableListOf()
    private var listener: OnFragmentInteractionListener? = null
    private var type = 0
    private var adapter = SupplierAdapter(this)
    private var latitude = 0.0
    private var longitude = 0.0

    companion object {
        var mapFragment: SupportMapFragment? = null
        val TAG: String = com.google.android.gms.maps.MapFragment::class.java.simpleName

        fun newInstance(type: Int,latitude: Double, longitude:Double) = 
            MapStoreMarketFragment().apply {
            arguments = Bundle().apply {
                putInt(TYPE_STORE, type)
                putDouble(LATITUDE, latitude)
                putDouble(LONGITUDE, longitude)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        type = arguments?.getInt(TYPE_STORE)!!
        latitude = arguments?.getDouble(LATITUDE)!!
        longitude = arguments?.getDouble(LONGITUDE)!!
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var rootView = inflater.inflate(R.layout.fragment_map_stores, container, false)

        mapFragment = childFragmentManager.findFragmentById(R.id.fallasMap) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        return rootView
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        observeListSuppliers()

        if (ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mMap.isMyLocationEnabled = true
        // Check if we were successful in obtaining the map.
       /* mMap.setOnMyLocationChangeListener { arg0 ->

            latitude = arg0.latitude
            longitude = arg0.longitude
            mMap.clear()
            mMap.addMarker(
                MarkerOptions().position(
                    LatLng(
                        arg0.latitude,
                        arg0.longitude
                    )
                ).title("It's Me!")
            ).setIcon(bitmapDescriptorFromVector(context!!, R.drawable.ic_marker))


        }*/

        mMap.addMarker(
            MarkerOptions().position(
                LatLng(
                    latitude,
                    longitude
                )
            ).title("It's Me!")
        ).setIcon(bitmapDescriptorFromVector(context!!, R.drawable.ic_marker))

        mMap.apply {
            val latLng = LatLng(latitude, longitude)
            moveCamera(CameraUpdateFactory.newLatLng(latLng))
            animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14f))
        }

    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap =
                Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }

    private fun observeListSuppliers() {
        storesViewModel.listSuppliers.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    storesViewList = it.data.toMutableList()
                    setStoresToMap()
                    setRecyclerView()
                }
                Status.ERROR -> {
                    listener?.hideLoading()
                    Snackbar.make(
                        supplierView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                    listener?.showLoading()
                }
            }
        })
        storesViewModel.getSuppliersByType(type, latitude.toString(), longitude.toString(), 8000)
    }

    private fun setRecyclerView() {
        val linearLayoutManagerStores = LinearLayoutManager(
            context,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        rvStores.layoutManager = linearLayoutManagerStores
        rvStores.adapter = adapter
        adapter.setStoreList(storesViewList)
    }


    private fun setStoresToMap() {
        val listId = mutableListOf<String>()
        storesViewList.forEach {
            val store = LatLng(it.latitude, it.longitude)
            val marker = mMap.addMarker(MarkerOptions().position(store).title(it.storeName))
            marker.setIcon(bitmapDescriptorFromVector(context!!, R.drawable.ic_marker))
            marker.tag = it.storeId
            listId.add(marker.id)
            listener?.hideLoading()
        }

        mMap.let {
            it.setOnMarkerClickListener {

                for (i in 0 until storesViewList.size){
                    if (storesViewList[i].storeId == it.tag){
                        adapter.notifyItemChanged(i)
                        rvStores.scrollToPosition(i)
                    }
                }

                false
            }
        }

    }

    override fun onClick(position: Int, establishmentType:String) {
        val storeView = storesViewList[position]
        listener?.replaceClassFragment(storeView)
      //  PreferencesManager.getInstance().setEstablishmentSelected(storeView)
    }

    interface OnFragmentInteractionListener {
        fun replaceClassFragment(establishment: SupplierView)
        fun showLoading()
        fun hideLoading()
    }
}