package com.android.bodegas.presentation.category

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegas.domain.category.GetCategories
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.category.mapper.CategoryViewMapper
import com.android.bodegas.presentation.category.model.CategoryView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class CategoryViewModel constructor(
    private val getCategories: GetCategories,
    private val categoryViewMapper: CategoryViewMapper
) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    //de mutable a live data
    private val _listCategories = MutableLiveData<Resource<List<CategoryView>>>()
    val listCategories: LiveData<Resource<List<CategoryView>>> = _listCategories

    fun getCategories(
        establishmentId: Int,
        class_id: Int,
        page: String,
        size: String,
        sortBy: String
    ) {
        viewModelScope.launch {
            _listCategories.value =
                Resource(Status.LOADING, mutableListOf(), "")

            val categoriesResult =
                getCategories.getCategories(establishmentId, class_id, page, size, sortBy)

            if (categoriesResult.status == Status.SUCCESS) {
                val categResultData = categoriesResult.data
                if (categResultData.isNotEmpty()) {
                    val categoryView = categResultData.map { categoryViewMapper.mapToView(it) }
                    _listCategories.value = Resource(Status.SUCCESS, categoryView, "")
                } else {
                    _listCategories.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _listCategories.value = Resource(Status.ERROR, mutableListOf(), "")
            }

        }
    }
}