package com.android.bodegas.presentation.orders.mapper

import com.android.bodegas.domain.customerOrders.EstablishmentOrders
import com.android.bodegas.domain.customerOrders.OrderDetails
import com.android.bodegas.domain.supplier.PaymentMethod
import com.android.bodegas.domain.supplier.Schedule
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.orders.model.EstablishmentOrdersView
import com.android.bodegas.presentation.orders.model.OrdersItemView
import com.android.bodegas.presentation.supplier.mapper.PaymentMethodViewMapper
import com.android.bodegas.presentation.supplier.mapper.ScheduleViewMapper
import com.android.bodegas.presentation.supplier.model.PaymentMethodView
import com.android.bodegas.presentation.supplier.model.ScheduleView


class EstablishmentOrdersViewMapper(
    private val scheduleViewMapper: ScheduleViewMapper,
    private val paymentMethodViewMapper: PaymentMethodViewMapper
) : Mapper<EstablishmentOrdersView, EstablishmentOrders> {
    override fun mapToView(type: EstablishmentOrders): EstablishmentOrdersView {
        return EstablishmentOrdersView(
            type.establishmentId,
            type.establishmentEmail,
            type.establishmentName,
            getHasDelivery(type.delivery),
            getListSchedules(type.shippingSchedule),
            getListSchedules(type.operationSchedule),
            getListPaymentMethod(type.paymentMethods),
            type.deliveryCharge,
            type.establishmentAddress
        )
    }

    private fun getHasDelivery(hasDelivery:String):Boolean{
        return hasDelivery == "S"
    }

    private fun getListSchedules(list: List<Schedule>): List<ScheduleView>{
        val listSchedule = mutableListOf<ScheduleView>()
        list.forEach {
            listSchedule.add(scheduleViewMapper.mapToView(it))
        }
        return listSchedule
    }

    private fun getListPaymentMethod(list: List<PaymentMethod>): List<PaymentMethodView>{
        val listPaymentMethod = mutableListOf<PaymentMethodView>()
        list.forEach {
            listPaymentMethod.add(paymentMethodViewMapper.mapToView(it))
        }
        return listPaymentMethod
    }
}
