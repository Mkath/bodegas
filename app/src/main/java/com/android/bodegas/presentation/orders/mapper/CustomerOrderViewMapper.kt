package com.android.bodegas.presentation.orders.mapper

import com.android.bodegas.domain.customerOrders.CustomerOrders
import com.android.bodegas.domain.customerOrders.OrderDetails
import com.android.bodegas.domain.util.DateTimeHelper
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.orders.model.CustomerOrderView
import com.android.bodegas.presentation.orders.model.OrdersItemView

class CustomerOrderViewMapper(
    private val establishmentOrdersViewMapper: EstablishmentOrdersViewMapper,
    private val orderDetailsViewMapper: OrderDetailsViewMapper
) : Mapper<CustomerOrderView, CustomerOrders> {
    override fun mapToView(type: CustomerOrders): CustomerOrderView {
        return CustomerOrderView(
            type.orderId,
            getTypeShipping(type.deliveryType),
            DateTimeHelper.parseDateOrder(type.orderDate),
            DateTimeHelper.parseDateOrder(type.shippingDateFrom),
            getStatusValue( type.status),
            establishmentOrdersViewMapper.mapToView(type.establishment),
            type.total,
            getListOrdersDetail(type.orderDetails),
            DateTimeHelper.parseDateOrder(type.updateDate),
            type.deliveryCharge,
            type.customerAmount,
            type.paymentMethodCustomerMessage,
            type.paymentMethodDescription,
            type.paymentMethodEstablishmentMessage,
            type.paymentMethodPaymentMethodId,
            type.paymentMethodRequestAmount
        )
    }

    private fun getListOrdersDetail(list: List<OrderDetails>): List<OrdersItemView> {
        val listSchedule = mutableListOf<OrdersItemView>()
        list.forEach {
            listSchedule.add(orderDetailsViewMapper.mapToView(it))
        }
        return listSchedule
    }

    private fun getTypeShipping(type: String): String {
        return if (type == "E") {
            "Delivery"
        } else {
            "Recojo en tienda"
        }
    }

    private fun getStatusValue(status: String): String {
        return when (status) {
            "I" -> "Informado"
            "A" -> "Atendido"
            "E" -> "Enviado"
            "T" -> "Entregado"
            "R" -> "Rechazado"
            "N" -> "Anulado"
             else -> ""
        }
    }
}