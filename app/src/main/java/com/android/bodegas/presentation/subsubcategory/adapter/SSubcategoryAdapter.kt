package com.android.bodegas.presentation.subsubcategory.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.bodegas.R
import com.android.bodegas.presentation.subsubcategory.model.SSubcategoryView
import com.bumptech.glide.Glide

class SSubcategoryAdapter(private val viewHolderListener: SSubcategoryViewHolder.ViewHolderListener) :
    RecyclerView.Adapter<SSubcategoryViewHolder>() {

    private val sSubCategoryList = mutableListOf<SSubcategoryView>()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SSubcategoryViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_subcategories, parent, false)
        context = parent.context
        return SSubcategoryViewHolder(view, viewHolderListener)
    }

    override fun getItemCount(): Int {
        return sSubCategoryList.size
    }

    override fun onBindViewHolder(holder: SSubcategoryViewHolder, position: Int) {
        val sSubcategoryView = sSubCategoryList[position]
        holder.nameSubcategory.text = sSubcategoryView.name
        if(sSubcategoryView.pathImage!!.isNotEmpty()){
            Glide.with(context).load(sSubcategoryView.pathImage).into(holder.imageSubSubCategory)
        }else{
            holder.imageSubSubCategory.visibility = View.INVISIBLE
        }
    }

    fun setSubSubCategoryList(list: List<SSubcategoryView>) {
        this.sSubCategoryList.clear()
        this.sSubCategoryList.addAll(list)
    }

}
