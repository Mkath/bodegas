package com.android.bodegas.presentation.clases.adapter

import com.android.bodegas.R
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.bodegas.presentation.clases.model.ClassView
import com.bumptech.glide.Glide

class ClassAdapter (private val viewHolderListener: ClassViewHolder.ViewHolderListener):
    RecyclerView.Adapter<ClassViewHolder>()
    {
        private val classesList = mutableListOf<ClassView>()
        private lateinit var context: Context

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClassViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_classes, parent, false)
            context = parent.context
            return ClassViewHolder(view, viewHolderListener)
        }

        override fun getItemCount(): Int {
            return classesList.size
        }

        override fun onBindViewHolder(holder: ClassViewHolder, position: Int) {
            val class_ = classesList[position]
            holder.name_category_class.text = class_.name
            if(class_.pathImage!!.isNotEmpty()){
                Glide.with(context).load(class_.pathImage).into(holder.imageClass)
            }else{
                holder.imageClass.visibility = View.INVISIBLE
            }
        }

        fun setStoreList(poolList: List<ClassView>) {
            this.classesList.clear()
            this.classesList.addAll(poolList)
            notifyDataSetChanged()
        }

    }