package com.android.bodegas.presentation.subsubcategory

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager

import com.android.bodegas.R
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.products.ProductsViewModel
import com.android.bodegas.presentation.products.model.ProductsView
import com.android.bodegas.presentation.subsubcategory.adapter.SSubcategoryAdapter
import com.android.bodegas.presentation.subsubcategory.adapter.SSubcategoryViewHolder
import com.android.bodegas.presentation.subsubcategory.model.SSubcategoryView
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_subsubcategory.*
import org.koin.androidx.viewmodel.ext.android.viewModel

const val SUB_SUB_CATEGORY_LIST = "ssCategoryList"
const val ESTABLISHMENT = "establishment_"

class SSubcategoryFragment : Fragment(), SSubcategoryViewHolder.ViewHolderListener {

    private var listener: OnFragmentInteractionListener? = null
    private var ssubCategoryViewList = ArrayList<SSubcategoryView>()
    private val productsViewModel: ProductsViewModel by viewModel()
    private var adapter = SSubcategoryAdapter(this)
    private var sSubcategoryName = String()
    private lateinit var establishment :SupplierView

    companion object {
        const val TAG = "SSubcategoryFragment"

        @JvmStatic
        fun newInstance(sSubCategoryList: ArrayList<SSubcategoryView>, establishment: SupplierView) =
            SSubcategoryFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(SUB_SUB_CATEGORY_LIST, sSubCategoryList)
                    putParcelable(ESTABLISHMENT, establishment)
                }
            }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ssubCategoryViewList = arguments!!.getParcelableArrayList(SUB_SUB_CATEGORY_LIST)!!
        establishment = arguments!!.getParcelable(ESTABLISHMENT)!!
        observeProductsList()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_subsubcategory, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (ssubCategoryViewList.isEmpty()) {
            containerEmptyState.visibility = View.VISIBLE
            rvSubSubCategories.visibility = View.GONE
        } else {
            containerEmptyState.visibility = View.GONE
            rvSubSubCategories.visibility = View.VISIBLE
            setRecyclerView()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onClick(position: Int) {
        val sSubCategoryView = ssubCategoryViewList[position]
        sSubcategoryName = sSubCategoryView.name
        productsViewModel.getProductsBySubSubCategory(establishment.storeId, "", sSubCategoryView.id)
    }

    private fun setRecyclerView() {
        var gridLayoutManager = LinearLayoutManager(context)
        rvSubSubCategories.layoutManager = gridLayoutManager
        rvSubSubCategories.adapter = adapter
        adapter.setSubSubCategoryList(ssubCategoryViewList)
        listener!!.hideLoading()
    }

    private fun observeProductsList() {
        productsViewModel.listProducts.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data.isNotEmpty()) {
                        listener!!.replaceProductsFragment(
                            establishment,
                            it.data,
                            sSubcategoryName,
                            false
                        )
                    }
                }

                Status.ERROR -> {
                    listener?.hideLoading()
                    Snackbar.make(
                        subSubCategoryView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                    listener?.showLoading()
                }
            }
        })
    }

    interface OnFragmentInteractionListener {
        fun replaceProductsFragment(
            establishment: SupplierView,
            productsList: List<ProductsView>,
            subCategoryName: String,
            isFromSubCategory: Boolean
        )

        fun showLoading()
        fun hideLoading()
    }


}
