package com.android.bodegas.presentation.orders.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrdersItemView (
    val orderDetailOrderDetailId: String,
    val orderDetailUnitMeasure: String,
    val orderDetailQuantity: String,
    var orderDetailPrice: String,
    val orderDetailStatus: String,
    val orderDetailObservation: String,
    var orderDetailSubTotal: String,
    val storeProductStoreProductId: String,
    val storeProductPrice: String,
    val storeProductStatus: String,
    val productTemplateProductTemplateId: String,
    val productTemplateCode: String,
    var productTemplateName: String,
    val productTemplateDescription: String,
    val productTemplateUnitMeasure: String,
    val productTemplateStatus: String,
    val productTemplatePathImage: String
) : Parcelable

