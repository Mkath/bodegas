package com.android.bodegas.presentation.supplier.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SupplierView(
    val storeId: Int,
    val storeName: String,
    val storeAddress: String,
    val establishmentType:String,
    val latitude: Double,
    val longitude: Double,
    val storeScheduleShipping:List<ScheduleView>,
    val storeScheduleOperation:List<ScheduleView>,
    val documentNumber:String,
    val image:String?="",
    val hasDelivery: Boolean,
    val paymentMethods:List<PaymentMethodView>,
    val deliveryCharge: Boolean
) : Parcelable