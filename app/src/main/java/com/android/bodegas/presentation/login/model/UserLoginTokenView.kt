package com.android.bodegas.presentation.login.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserLoginTokenView(
    val idEntity: String,
    val token: String
) : Parcelable