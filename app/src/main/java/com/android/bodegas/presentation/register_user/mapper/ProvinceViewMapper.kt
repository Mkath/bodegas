package com.android.bodegas.presentation.register_user.mapper

import com.android.bodegas.domain.province.Province
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.register_user.model.ProvinceView

class ProvinceViewMapper: Mapper<ProvinceView, Province> {
    override fun mapToView(type: Province): ProvinceView {
        return ProvinceView(
            type.id,
            type.name,
            type.department_id
        )
    }
}