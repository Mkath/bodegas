package com.android.bodegas.presentation.subcategory.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubCategoryView(
    val id: Int,
    val description: String,
    val name: String,
    val pathImage:String?=""
) : Parcelable