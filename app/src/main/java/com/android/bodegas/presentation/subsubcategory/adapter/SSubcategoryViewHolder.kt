package com.android.bodegas.presentation.subsubcategory.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_subcategories.view.*

class SSubcategoryViewHolder  (itemView: View, viewHolderListener: ViewHolderListener):
    RecyclerView.ViewHolder(itemView) {

    //inyectamos la data
    val nameSubcategory : TextView = itemView.name_subcategory
    val imageSubSubCategory : ImageView = itemView.ivImageSubCategory

    init {
        itemView.setOnClickListener {
            viewHolderListener.onClick(
                layoutPosition
            )
        }
    }

    interface ViewHolderListener {
        fun onClick(
            position: Int
        )
    }
}