package com.android.bodegas.presentation.supplier

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegas.domain.supplier.GetSupplierByType
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.supplier.mapper.SupplierViewMapper
import com.android.bodegas.presentation.supplier.model.SupplierView
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class SupplierViewModel constructor(
    private val getSupplierByType: GetSupplierByType,
    private val supplierViewMapper: SupplierViewMapper
) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _listSuppliers = MutableLiveData<Resource<List<SupplierView>>>()
    val listSuppliers: LiveData<Resource<List<SupplierView>>> = _listSuppliers

    private val _listSuppliersMarket = MutableLiveData<Resource<List<SupplierView>>>()
    val listSuppliersMarket: LiveData<Resource<List<SupplierView>>> = _listSuppliersMarket

    fun getSuppliersByType(type: Int, latitude: String, longitude: String, radius: Int) {
        viewModelScope.launch {
            _listSuppliers.value =
                Resource(Status.LOADING, mutableListOf(), "")

            val suppliersResult =
                getSupplierByType.getSupplierByType(type, latitude, longitude, radius)

            if (suppliersResult.status == Status.SUCCESS) {

                val storesResultData = suppliersResult.data

                if (storesResultData.isNotEmpty()) {
                    val suppliersView = storesResultData.map { supplierViewMapper.mapToView(it) }
                    _listSuppliers.value = Resource(Status.SUCCESS, suppliersView, suppliersResult.message)

                } else {
                    _listSuppliers.value = Resource(Status.SUCCESS, mutableListOf(), suppliersResult.message)
                }
            } else {
                _listSuppliers.value =
                    Resource(Status.ERROR, mutableListOf(), suppliersResult.message)
            }
        }
    }

    fun getSuppliersMarket(type: Int, latitude: String, longitude: String, radius: Int) {
        viewModelScope.launch {
            _listSuppliersMarket.value =
                Resource(Status.LOADING, mutableListOf(), "")

            val suppliersResult =
                getSupplierByType.getSupplierByType(type, latitude, longitude, radius)

            if (suppliersResult.status == Status.SUCCESS) {

                val storesResultData = suppliersResult.data

                if (storesResultData.isNotEmpty()) {
                    val suppliersView = storesResultData.map { supplierViewMapper.mapToView(it) }
                    _listSuppliersMarket.value = Resource(Status.SUCCESS, suppliersView, "")

                } else {
                    _listSuppliersMarket.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _listSuppliersMarket.value =
                    Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }

}