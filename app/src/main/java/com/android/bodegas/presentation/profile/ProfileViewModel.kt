package com.android.bodegas.presentation.profile

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.data.repository.user.model.UserEntity
import com.android.bodegas.domain.save_order.GetOrderByUser
import com.android.bodegas.domain.subcategory.GetSubcategories
import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.domain.user.update.UpdateDataUser
import com.android.bodegas.domain.user.update.UpdatePassword
import com.android.bodegas.domain.user.update.UpdatePhotoUser
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.register_user.mapper.UserViewMapper
import com.android.bodegas.presentation.register_user.model.UserView
import com.android.bodegas.presentation.subcategory.mapper.SubCategoryViewMapper
import com.android.bodegas.presentation.subcategory.model.SubCategoryView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class ProfileViewModel constructor(
    private val updatePhotoUser: UpdatePhotoUser,
    private val getOrderByUser: GetOrderByUser,
    private val updatePasswordService: UpdatePassword,
    private val updateDataUser: UpdateDataUser,
    private val userViewMapper: UserViewMapper
    ) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    //de mutable a live data
    private val _updatePhoto = MutableLiveData<Resource<Boolean>>()
    val updatePhoto : LiveData<Resource<Boolean>> = _updatePhoto

    private val _updatePassword = MutableLiveData<Resource<Boolean>>()
    val updatePassword : LiveData<Resource<Boolean>> = _updatePassword

    private val _updateUserEntity = MutableLiveData<Resource<User>>()
    val updateUserEntity : LiveData<Resource<User>> = _updateUserEntity

    private val _getUserView = MutableLiveData<Resource<UserView>>()
    val getUserView : LiveData<Resource<UserView>> = _getUserView

    fun updatePhotoOfUser(customerId:Int, filePath:String) {
        _updatePhoto.value = Resource(Status.LOADING, false, "")
        viewModelScope.launch {

            val updateResult = updatePhotoUser.updatePhoto(customerId, filePath)
            if(updateResult.status == Status.SUCCESS){
                if(updateResult.data){
                    _updatePhoto.value = Resource(Status.SUCCESS, updateResult.data, updateResult.message)
                }
                else{
                    _updatePhoto.value = Resource(Status.ERROR, updateResult.data, "")
                }
            }
            else{
                _updatePhoto.value = Resource(Status.ERROR, updateResult.data, "")
            }

        }
    }


    fun deleteOrders() {
        viewModelScope.launch {
            getOrderByUser.deleteOrder()
            PreferencesManager.getInstance().cleanEstablishment()
            PreferencesManager.getInstance().closeSession()
            PreferencesManager.getInstance().isInvited(true)
        }
    }

    fun updatePassword(body: ChangePasswordDtoBody) {
        _updatePassword.value = Resource(Status.LOADING, false, "")
        viewModelScope.launch {
            val updateResult = updatePasswordService.updatePassword(body)
            if(updateResult.status == Status.SUCCESS){
                if(updateResult.data){
                    _updatePassword.value = Resource(Status.SUCCESS, updateResult.data, updateResult.message)
                }
                else{
                    _updatePassword.value = Resource(Status.ERROR, updateResult.data, "")
                }
            }
            else{
                _updatePassword.value = Resource(Status.ERROR, updateResult.data, "")
            }

        }
    }

    fun updateDataUser(customerId:Int, body:UpdateCustomerBody) {
        viewModelScope.launch {
            val updateResult = updateDataUser.updateDataUser(customerId, body)
            if(updateResult.status == Status.SUCCESS){
                PreferencesManager.getInstance().setUser(userViewMapper.mapToView(updateResult.data))
                _updateUserEntity.value = Resource(Status.SUCCESS, updateResult.data, updateResult.message)
            }
            else{
                _updateUserEntity.value = Resource(Status.ERROR, updateResult.data, "")
            }
        }
    }

    fun getDataUser() {
        viewModelScope.launch {
            val userView = PreferencesManager.getInstance().getUser()
            _getUserView.value = Resource(Status.SUCCESS, userView, "")
        }
    }

}