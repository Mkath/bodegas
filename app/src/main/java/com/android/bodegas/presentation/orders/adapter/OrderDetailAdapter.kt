package com.android.bodegas.presentation.orders.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.android.bodegas.R
import com.android.bodegas.presentation.orders.model.OrdersItemView
import com.bumptech.glide.Glide

class OrderDetailAdapter (private val viewHolderListener: OrderDetailViewHolder.ViewHolderListener):
    RecyclerView.Adapter<OrderDetailViewHolder>() {

    private val detailedOrdersList = mutableListOf<OrdersItemView>()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderDetailViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_customer_order_detail, parent, false)
        context = parent.context
        return OrderDetailViewHolder(view, viewHolderListener)
    }

    override fun getItemCount(): Int {
        if (detailedOrdersList != null)
            return detailedOrdersList.size
        return 0
    }

    override fun onBindViewHolder(holder: OrderDetailViewHolder, position: Int) {
        val ordersItemView = detailedOrdersList[position]
        holder.inputName.text = ordersItemView.productTemplateName
        holder.subtotalPrice.text = String.format("%s %s", "s/", ordersItemView.orderDetailSubTotal)
        holder.unitPrice.text = String.format("%s %s %s", "Precio unitario","s/", ordersItemView.orderDetailPrice)
        if(ordersItemView.orderDetailStatus == "No atendido"){
            holder.inputEstatus.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_rounded_red_8_background))
        }else{
            holder.inputEstatus.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_rounded_green_8_background))
        }

        holder.inputEstatus.text = ordersItemView.orderDetailStatus

        val quantity = String.format("%s %s", ordersItemView.orderDetailQuantity, ordersItemView.orderDetailUnitMeasure)
        holder.descripcion.text = String.format("%s %s %s", ordersItemView.productTemplateDescription, "x", quantity)
        if(ordersItemView.productTemplatePathImage.isNotEmpty()){

            val circularProgressDrawable = CircularProgressDrawable(context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()

            Glide.with(context)  // this
                .load(ordersItemView.productTemplatePathImage)
                .placeholder(circularProgressDrawable)
                .error(context.getDrawable(R.drawable.ic_image_black_24dp))
                .into(holder.inputImagen)
        }

        else{
            holder.inputImagen.visibility = View.VISIBLE
        }
    }

    fun setStoreList(poolList: List<OrdersItemView>) {
        this.detailedOrdersList.clear()
        this.detailedOrdersList.addAll(poolList)
    }
}