package com.android.bodegas.presentation.save_order.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SaveOrderView(
    var id: String,
    var storeProductId: Int,
    var price: Double,
    var quantity: Double,
    var subtotal: Double,
    var unitMeasure: String,
    var observation:String,
    var name:String,
    var imageProduct:String
) : Parcelable
