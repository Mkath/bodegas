package com.android.bodegas.presentation.orders.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderDetailBodyView (
    val orderDetailId: Int,
    val state: String
) : Parcelable